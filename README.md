Bachelor's Dissertation's project.

This is the project of the work "Integrating similarity and dissimilarity notions in recommenders"
Authors: Christos Zigkolis, Savvas Karagiannidis, Ioannis Koumarelas, Athena Vakali

To create a runnable jar initialize the execution the system from the Main class. Do not forget to firstly include the libraries at the lib/ folder and extract or package them at the runnable jar.

The two parameters that the user has to provide are:
	1. input
		Path: in here the user needs to provide the values (algorithms parameters, dataset's path etc.) that are needed for the execution.
	2. output
		Path: the results will be saved here.

This project's content is released under the MIT License provided at the file License.md and it applies to the whole project, apart from the libraries that we of course use. For other questions please refer to the paper.

From the creator/implementer of the system,
John Koumarelas