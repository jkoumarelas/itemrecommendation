package categoryPipeline;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.IntPairWritable;
import commons.LongArrayWritable;

/**
 * 
 * This class will output MapFiles for the secondary structure:
 * 		-	Set Users for a Pair of Categories
 * 
 * @author John Koumarelas
 *
 */
public class CategoriesCommonUsersDriver {
	private static String pathStrOutput = null;
	private static String pathStrOutputOriginal = null;
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	// Categories Common Users
	private static long keys_MapFile_CategoriesCommonUsers = 0L;
	private static long index_sizeInBytes_MapFile_CategoriesCommonUsers = 0L;
	
	public static void clearFields()
	{
		keys_MapFile_CategoriesCommonUsers = 0L;
		index_sizeInBytes_MapFile_CategoriesCommonUsers = 0L;
	}
	
	// Input: users... At the reducers the average will be computed.
	public static class Map extends Mapper<LongWritable, Text, IntPairWritable, Text> {

		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			//	categoriesCombinationStr[0]: categoryID1
			//	categoriesCombinationStr[1]: categoryID2
			String[] strCategoriesCombination = value.toString().split(",");	//	Categories were separated in file with commas. e.g. 1,2

	    	IntPairWritable ipwCategoriesCombination = new IntPairWritable();
	    	ipwCategoriesCombination.set(Integer.parseInt(strCategoriesCombination[0]), Integer.parseInt(strCategoriesCombination[1]));

	    	context.write(ipwCategoriesCombination, new Text());
		}
	}
	
	public static class Reduce extends Reducer<IntPairWritable, Text, Text, Text> {
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		private String pathStrOutputOriginal = null;
		
		private MapFile.Writer writer = null;
		
		private TaskID thisTaskID = null;
		
		private MapFile.Reader reader = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			thisTaskID = context.getTaskAttemptID().getTaskID();
			
			{
				Path pathInput = new Path(pathStrOutputOriginal + File.separator + "CategoryPipeline" + File.separator + "CategorySetOfUsers_Final");
		    	Path pathInputQualified = fs.makeQualified(pathInput);
		    	reader = new MapFile.Reader(fs, pathInputQualified.toString(),conf);
			}
			
			{
				String strPath = pathStrOutput + File.separator + "SetUsersPairCategories"  + File.separator + thisTaskID;
				Path path = new Path(strPath);
	 	    	Path pathQualified = fs.makeQualified(path);
	 	    	writer = new MapFile.Writer(conf, fs, pathQualified.toString(), IntPairWritable.class, LongArrayWritable.class);
			}
			
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
			writer.close();
			reader.close();
			super.cleanup(context);
		};

		@Override
		public void reduce(IntPairWritable key, Iterable<Text> values, Context context) {
		    
		 // We get the users from both categories.
	    	LongArrayWritable lawCategory1 = new LongArrayWritable();
	    	LongArrayWritable lawCategory2 = new LongArrayWritable();
	    	IntWritable iwKeyCategory1 = new IntWritable(key.getFirst());
	    	IntWritable iwKeyCategory2 = new IntWritable(key.getSecond());
			try {
				reader.get(iwKeyCategory1, lawCategory1);
				reader.get(iwKeyCategory2, lawCategory2);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

	    	//	Get users from both categories.
	    	Writable[] wArrayCategory1 = lawCategory1.get();
	    	Writable[] wArrayCategory2 = lawCategory2.get();
	    	
	    	TreeSet<Long> hsUsersSet = new TreeSet<Long>();
	    	
//	    	// Union
//	    	if(wArrayCategory1 != null && wArrayCategory1.length > 0)
//	    	for(int i = 0 ; i < wArrayCategory1.length ; i++) {
//	    		LongWritable iwUserID = (LongWritable) wArrayCategory1[i];
//	    		hsUsersSet.add(new Long(iwUserID.get()));
//	    	}
//	    	if(wArrayCategory2 != null && wArrayCategory2.length > 0)
//	    	for(int i = 0 ; i < wArrayCategory2.length ; i++) {
//	    		LongWritable iwUserID = (LongWritable) wArrayCategory2[i];
//	    		hsUsersSet.add(new Long(iwUserID.get()));
//	    	}
//	    	//
	    	
	    	// Intersection
	    	if(wArrayCategory1 != null && wArrayCategory2 !=null && wArrayCategory1.length > 0 && wArrayCategory2.length > 0){
	    		for(int i = 0 ; i < wArrayCategory1.length ; i++)
	    			for(int j = 0 ; j < wArrayCategory2.length ; j++)
	    			{
	    				if( ((LongWritable)wArrayCategory1[i]).compareTo((LongWritable)wArrayCategory2[j]) == 0) {
	    					hsUsersSet.add(new Long(((LongWritable)wArrayCategory2[j]).get()));
	    					break;
	    				}
	    			}
	    	}
	    	else return;
	    	//

	    	Long[] lArrayUsersSet = hsUsersSet.toArray(new Long[0]);
	    	
	    	LongWritable[] lwArrayUsersSet = new LongWritable[lArrayUsersSet.length];
	    	
	    	for(int i = 0 ; i < lArrayUsersSet.length ; i++)
	    		lwArrayUsersSet[i] = new LongWritable(lArrayUsersSet[i]);
	    		
	    	LongArrayWritable lawSetOfUsers = new LongArrayWritable();
	    	lawSetOfUsers.set(lwArrayUsersSet);

		    /* Set of users for a pair of categories. U(cl,cm)*/
		    
			try {
				writer.append(key, lawSetOfUsers);
			} catch (IOException e) {
				e.printStackTrace();
			}
		    
	    }
	}
	
	public static void mergeMapFilesGeneral(String pathStrInput){
		try{
			String pathStrOutput = pathStrInput + "_Final";
			Path pathOutput = new Path(pathStrOutput);
			Path pathQualifiedDirNameOutput = fs.makeQualified(pathOutput);
		    MapFile.Writer writer = null;
		    
			FileStatus[] fstatArrayMapFiles = fs.listStatus(new Path(pathStrInput));
		    
			//
		    LocalFileSystem lfs = LocalFileSystem.getLocal(conf);
		    
		    lfs.delete(new Path("localMapFiles"), true);
		    lfs.mkdirs(new Path("localMapFiles"));
		    
		    for(FileStatus fstat : fstatArrayMapFiles)
		    {
		    	Path pathHDFS = fstat.getPath();
		    	Path pathLocal = new Path( "localMapFiles" + File.separator + fstat.getPath().getName());
		    	fs.copyToLocalFile(pathHDFS, pathLocal);
		    }

		    FileStatus[] fstatMapFilesLocal = lfs.listStatus(new Path("localMapFiles"));
			//
			
		    writer = new MapFile.Writer(conf, fs, pathQualifiedDirNameOutput.toString(), IntPairWritable.class, LongArrayWritable.class);
	    	
		    TreeMap<IntPairWritable, MapFile.Reader> tmValues = new TreeMap<IntPairWritable, MapFile.Reader>();
		    
		    long keysCounter = 0L;
		    
		    // Insert first values
		    for(FileStatus fstat : fstatMapFilesLocal)
		    {
		    	Path pathInput = fstat.getPath();
		    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		    	MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		    	
		    	LongArrayWritable law = new LongArrayWritable();
		    	IntPairWritable ipwKey = new IntPairWritable();
		    	ipwKey.set(Integer.MIN_VALUE, Integer.MIN_VALUE);
		    	
		    	ipwKey = (IntPairWritable) reader.getClosest(ipwKey, law);	//	key: userID
		    	
		    	if(ipwKey != null)
		    		tmValues.put(ipwKey, reader);
		    	else
		    		reader.close();
		    	
		    	String[] strTokensArray = pathInput.getName().split("_");
		    	keysCounter += Long.parseLong(strTokensArray[strTokensArray.length - 1]);
		    }
		    
		    keys_MapFile_CategoriesCommonUsers = keysCounter;
		    
		    while(true) {
		    	
		    	if(tmValues.size() == 0)
		    		break;
		    	
		    	keys_MapFile_CategoriesCommonUsers++;
		    	
		    	Entry<IntPairWritable, MapFile.Reader> entry = tmValues.firstEntry();
		    	IntPairWritable ipwTmKey = entry.getKey();
		    	MapFile.Reader reader = entry.getValue();
		    	
		    	LongArrayWritable law = new LongArrayWritable();
		    	
		    	reader.get(ipwTmKey, law);	//	key: userID
		    	IntPairWritable ipwKey = new IntPairWritable();
		    	ipwKey.set(ipwTmKey.getFirst(), ipwTmKey.getSecond());
		    	writer.append(ipwKey, law);
		    	boolean existsNext = reader.next(ipwKey, law);
		    	tmValues.remove(ipwTmKey);
		    	if(!existsNext)
		    		reader.close();
		    	else
		    		tmValues.put(ipwKey, reader);
			    	
		    }
		    
		    writer.close();
		    lfs.delete(new Path("localMapFiles"), true);
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void mergeMapFiles() throws IOException{
		mergeMapFilesGeneral(pathStrOutput + File.separator + "SetUsersPairCategories");
	}
	
	private static void calculateFileSizes()
	{
		try{
			FileStatus fileStatusIndex = fs.getFileStatus(new Path(pathStrOutput + File.separator + "SetUsersPairCategories_Final" + File.separator + "index"));
			index_sizeInBytes_MapFile_CategoriesCommonUsers = fileStatusIndex.getLen();	// size in bytes
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evGeneral.file_MapFile_CategoriesCommonUsers_keys = keys_MapFile_CategoriesCommonUsers;
		evcg.evGeneral.file_MapFile_CategoriesCommonUsers_index_sizeInBytes = index_sizeInBytes_MapFile_CategoriesCommonUsers;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputOriginal", pathStrOutputOriginal);
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Categories Common Users Driver - Start");
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator + "CategoriesCommonUsers";
		pathStrOutputOriginal = evcg.getOutputPathGeneral();
		
		setParameters(conf);

		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput)))
			{
				System.out.println("*Categories Common Users Driver*");
				System.out.println("Folder: " + pathStrOutput);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}
		
		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.evGeneral.inputPath + File.separator + "filesNeeded" + File.separator + "categoriesCombination" + evcg.evGeneral.datasetName;
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "CategoriesCommonUsers");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(CategoriesCommonUsersDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntPairWritable.class);
		job.setMapOutputValueClass(Text.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);
		
		mergeMapFiles();
		calculateFileSizes();
		updateInfo(evcg);
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_CategoriesCommonUsersDriver = stop - start;
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "SetUsersPairCategories"));
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Categories Common Users Driver - Stop");
		evcg.hasChangedGeneral = true;
		
		System.runFinalization();
		System.gc();
	}
}
