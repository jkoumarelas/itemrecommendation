package categoryPipeline;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.LongArrayWritable;

/**
 * 
 * This class outputs MapFiles for the secondary structures:
 * 		-	Category Preferences
 * 		-	Category Set Of Users
 * 		-	Category Average Value
 * 
 * @author John Koumarelas
 *
 */
public class CategoryPipelineDriver {
	private static String pathStrOutput = null;
	private static String pathStrOutputOriginal = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	// File Size Info
	
	// Category Set Of Users
	private static long keys_MapFile_CategorySetOfUsers = 0L;
	private static long index_sizeInBytes_MapFile_CategorySetOfUsers = 0L;
	
	public static void clearFields()
	{
		keys_MapFile_CategorySetOfUsers = 0L;
		index_sizeInBytes_MapFile_CategorySetOfUsers = 0L;
	}

		public static class Map extends Mapper<LongWritable, Text, IntWritable, LongWritable> {

		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
				
				String[] strItemInfo = strUP.split("-");
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strCategoriesIDArray = strItemInfo[1].split("_");
				
				for(String strCategoryID : strCategoriesIDArray){
					context.write(new IntWritable(Integer.parseInt(strCategoryID)), new LongWritable(Long.parseLong(strUserID)));
				}
			}
			
		}
	}
	
	public static class Reduce extends Reducer<IntWritable, LongWritable, Text, Text> {	
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		
		private MapFile.Writer writerCategorySetOfUsers = null;
		
		private TaskID thisTaskID = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<IntWritable,LongWritable,Text,Text>.Context context) throws IOException ,InterruptedException {
			
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			thisTaskID = context.getTaskAttemptID().getTaskID();

			{
				String pathStr = pathStrOutput + File.separator + "CategorySetOfUsers" + File.separator +  thisTaskID;
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
			    writerCategorySetOfUsers = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), IntWritable.class, LongArrayWritable.class);
			}

			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<IntWritable,LongWritable,Text,Text>.Context context) throws IOException ,InterruptedException {
			writerCategorySetOfUsers.close();
			super.cleanup(context);
		};

		@Override
		public void reduce(IntWritable key, Iterable<LongWritable> values, Context context) {
			
			/* Category's Set of Users */
			TreeSet<LongWritable> tsUsers = new TreeSet<LongWritable>();

			for(LongWritable lwUserID : values)
				tsUsers.add(new LongWritable(lwUserID.get()));
			
			try {
				LongArrayWritable lawUsers = new LongArrayWritable();
				LongWritable[] lwArrayUsers = null;
				
				lwArrayUsers = tsUsers.toArray(new LongWritable[0]);
				lawUsers.set(lwArrayUsers);

				writerCategorySetOfUsers.append(new IntWritable(key.get()), lawUsers);
			    
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	}
	
	/* Merging temp files into larger ones, that contain all the records.
	   e.g. For CategoryPreferences we made a MapFile for each category.
	  		
	  			We will open each user's MapFile which will contain only one record
	  			<catID, CategoryPreferenceArray> and save that record in the
	  			new file that will contain all the records together.
	  			
	  			So the final MapFile will contain all the records together:
	  			<catID1, CategoryPreferenceArray_1>
	  			<catID2, CategoryPreferenceArray_2>
	  			...
	  			<catIDN, CategoryPreferenceArray_N>
	 */
	public static void mergeMapFilesGeneral(String inputPath){
		try{
			String pathStr = inputPath + "_Final";
			Path path = new Path(pathStr);
			FileSystem fs = FileSystem.get(conf);
			Path qualifiedDirName = fs.makeQualified(path);
		    MapFile.Writer.setIndexInterval(conf, 1);
		    MapFile.Writer writer = null;
			
			FileStatus[] fstatArrayMapFiles = fs.listStatus(new Path(inputPath));
			
			//
		    LocalFileSystem lfs = LocalFileSystem.getLocal(conf);
		    
		    lfs.delete(new Path("localMapFiles"), true);
		    lfs.mkdirs(new Path("localMapFiles"));
		    
		    for(FileStatus fstat : fstatArrayMapFiles)
		    {
		    	Path pathHDFS = fstat.getPath();
		    	Path pathLocal = new Path( "localMapFiles" + File.separator + fstat.getPath().getName());
		    	fs.copyToLocalFile(pathHDFS, pathLocal);
		    }

		    FileStatus[] fstatMapFilesLocal = lfs.listStatus(new Path("localMapFiles"));
			//
			
	    	writer = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), IntWritable.class, LongArrayWritable.class);

		    TreeMap<IntWritable, MapFile.Reader> tmValues = new TreeMap<IntWritable, MapFile.Reader>();
		    
		    for(FileStatus fstat : fstatMapFilesLocal)
		    {
		    	Path pathInput = fstat.getPath();
		    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		    	MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		    	
	    		LongArrayWritable lawValue = new LongArrayWritable();
		    	IntWritable iwKey = new IntWritable(Integer.MIN_VALUE);
		    	iwKey = (IntWritable) reader.getClosest(iwKey, lawValue);	//	key: categoryID
		    	
		    	if(iwKey != null) // reducers <= number of categories
		    		tmValues.put(iwKey, reader);
		    	else	// reducers > number of categories, some didn't receive any key (key = categoryID).
		    		reader.close();
		    }
		    
		    while(true) {
		    	
		    	if(tmValues.size() == 0)
		    		break;

		    	keys_MapFile_CategorySetOfUsers++;
		    	
		    	Entry<IntWritable, MapFile.Reader> entry = tmValues.firstEntry();
		    	IntWritable iwTmKey = entry.getKey();
		    	MapFile.Reader reader = entry.getValue();
		    	
	    		LongArrayWritable lawValue = new LongArrayWritable();
		    	IntWritable iwKey = new IntWritable(iwTmKey.get());
		    	reader.get(iwKey, lawValue);	//	key: categoryID
		    	
		    	writer.append(iwKey, lawValue);
		    	boolean existsNext = reader.next(iwKey, lawValue);
		    	tmValues.remove(iwTmKey);
		    	if(!existsNext)
		    		reader.close();
		    	else
		    		tmValues.put(iwKey, reader);
		    	
		    }
		    
		    writer.close();
		    lfs.delete(new Path("localMapFiles"), true);
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}

	/* Merging MapFiles files for: 	Category's Set Of Users

		We read the contents of the MapFiles and append them in a new MapFile which will contain all the entries.
		The new MapFile will end with the suffix "_Final".
	*/
	public static void mergeMapFiles() throws IOException{
		mergeMapFilesGeneral(pathStrOutput + File.separator + "CategorySetOfUsers");
	}
	
	private static void calculateFileSizes()
	{
		try{
			FileStatus indexFileStatus = fs.getFileStatus(new Path(pathStrOutput + File.separator + "CategorySetOfUsers_Final" + File.separator + "index"));
			index_sizeInBytes_MapFile_CategorySetOfUsers = indexFileStatus.getLen();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evGeneral.file_MapFile_CategorySetOfUsers_keys = keys_MapFile_CategorySetOfUsers;
		evcg.evGeneral.file_MapFile_CategorySetOfUsers_index_sizeInBytes = index_sizeInBytes_MapFile_CategorySetOfUsers;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputOriginal", pathStrOutputOriginal);
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Category Pipeline Driver - Start");
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator +"CategoryPipeline";
		pathStrOutputOriginal = evcg.getOutputPathGeneral();
		
		setParameters(conf);

		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput)))
			{
				System.out.println("*Category Pipeline Driver*");
				System.out.println("Folder: " + pathStrOutput);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)){
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}
		
		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();

		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
		
		MapFile.Writer.setIndexInterval(conf, 1);
				 
		Job job = new Job(conf, "CategoryPipeline");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(CategoryPipelineDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(LongWritable.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);
		
		mergeMapFiles();
		calculateFileSizes();
		updateInfo(evcg);
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
		{ 
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "CategorySetOfUsers"));
		}
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_CategoryPipelineDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Category Pipeline Driver - Stop");
		evcg.hasChangedGeneral = true;
		
		System.runFinalization();
		System.gc();
	}
}
