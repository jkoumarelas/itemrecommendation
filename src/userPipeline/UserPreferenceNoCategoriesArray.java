package userPipeline;

import org.apache.hadoop.io.ArrayWritable;

/**
 * Objects from this class are saved in the secondary structure of 
 * UserPreferences On A Category.
 * 
 * @author John Koumarelas
 *
 */
public class UserPreferenceNoCategoriesArray extends ArrayWritable { 
	public UserPreferenceNoCategoriesArray() 
	{ 
		super(UserPreferenceNoCategories.class); 
	} 
}