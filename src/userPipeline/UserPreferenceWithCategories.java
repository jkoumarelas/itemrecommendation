package userPipeline;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;

/**
 * Objects from this class (grouped into UserPreferenceWithCategoriesArray)
 * are saved in the secondary structure of UserPreferences.
 * 
 * @author John Koumarelas
 *
 */
public class UserPreferenceWithCategories implements WritableComparable<UserPreferenceWithCategories>{
	public long itemID;
	public int categoriesArrayLength;
	public int[] categoriesArray;
	public double rating;
	
	public UserPreferenceWithCategories() {
		itemID = -1;
		categoriesArrayLength = -1;
		categoriesArray = null;
		rating = -1;
	}
	
	public UserPreferenceWithCategories(long itemID, int[] categoriesArray ,double rating) {
		this.itemID = itemID;
		this.categoriesArrayLength = categoriesArray.length;
		this.categoriesArray = categoriesArray;
		this.rating = rating;
	}
	
	public UserPreferenceWithCategories(UserPreferenceWithCategories up) {
		this.itemID = up.itemID;
		this.categoriesArrayLength = up.categoriesArrayLength;
		this.categoriesArray = up.categoriesArray;
		this.rating = up.rating;
	}
	
	@Override
	public int compareTo(UserPreferenceWithCategories o) {
		if( o == this) return 0;
		if( itemID == o.itemID) return 0;
		else
			return ( itemID < o.itemID ? -1 : 1);
	}
	
	@Override
	public int hashCode() {
		return (int)itemID;
	}
	
	@Override
	public String toString(){
		String strReturn = "itemID: " + String.valueOf(itemID)+ " categoriesArrayLength: " + categoriesArrayLength + " categories: ";
		for(int i = 0 ; i < categoriesArray.length ; i++){
			if((i+1)!=categoriesArray.length)
				strReturn += categoriesArray[i] + ", ";
			else
				strReturn += categoriesArray[i];
		}
		strReturn += " rating:" + String.valueOf(rating);
		return strReturn;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		itemID = in.readLong();
		
		categoriesArrayLength = in.readInt();
		
		categoriesArray = new int[categoriesArrayLength];

		for(int i = 0 ; i < categoriesArrayLength ; i++){
			categoriesArray[i]  = in.readInt();
		}

		rating = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(itemID);

		categoriesArrayLength = categoriesArray.length;
		out.writeInt(categoriesArrayLength);
		
		for(int i = 0 ; i < categoriesArray.length ; i++ ){
			out.writeInt(categoriesArray[i]);
		}

		out.writeDouble(rating);
	}
}
