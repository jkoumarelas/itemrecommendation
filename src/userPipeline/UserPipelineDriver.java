package userPipeline;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

/**
 * 
 * This class outputs MapFiles for the secondary structures:
 * 		-	User Preferences
 * 		-	User Preferences On A Category
 * 		-	User Disposition
 * 		-	User Average Value On A Category
 * 
 * @author John Koumarelas
 *
 */
public class UserPipelineDriver {
	private static String pathStrOutput = null;
	private static String pathStrOutputOriginal = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	// File Size Info
	
	// User Preferences
	private static long keys_MapFile_UserPreferences = 0L;
	private static long index_sizeInBytes_MapFile_UserPreferences = 0L;
	
	// User Preferences On A Category
	private static long keys_MapFile_UserPreferencesOnACategory = 0L;
	private static long index_sizeInBytes_MapFile_UserPreferencesOnACategory = 0L;
	
	// User Disposition
	private static long keys_MapFile_UserDisposition = 0L;
	private static long index_sizeInBytes_MapFile_UserDisposition = 0L;
	
	// User Average Value On A Category
	private static long keys_MapFile_UserAverageValueOnACategory = 0L;
	private static long index_sizeInBytes_MapFile_UserAverageValueOnACategory = 0L;
	
	public static void clearFields()
	{
		keys_MapFile_UserPreferences = 0L;
		index_sizeInBytes_MapFile_UserPreferences = 0L;
		keys_MapFile_UserPreferencesOnACategory = 0L;
		index_sizeInBytes_MapFile_UserPreferencesOnACategory = 0L;
		keys_MapFile_UserDisposition = 0L;
		index_sizeInBytes_MapFile_UserDisposition = 0L;
		keys_MapFile_UserAverageValueOnACategory = 0L;
		index_sizeInBytes_MapFile_UserAverageValueOnACategory = 0L;
	}
	
	public static class Map extends Mapper<LongWritable, Text, LongWritable, UserPreferenceWithCategories> {
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
				
				LongWritable lwUserID = new LongWritable( Long.parseLong(strUserID));
				
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
				long itemID = Long.parseLong(strItemInfo[0]);
				double rating = Double.parseDouble(strItemInfo[2]);
				
				String[] strCategoriesIDArray = strItemInfo[1].split("_");
				
				int[] intArrayCategories = new int[strCategoriesIDArray.length];
				
				for(int j = 0 ; j < strCategoriesIDArray.length ; j++) {
					intArrayCategories[j] = Integer.parseInt(strCategoriesIDArray[j]);
				}
					
				UserPreferenceWithCategories upwc = new UserPreferenceWithCategories(itemID, intArrayCategories, rating);
				
				context.write(lwUserID, upwc);
			}
			
		}
	}
	
	public static class Reduce extends Reducer<LongWritable, UserPreferenceWithCategories, Text, Text>{
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		private String pathStrOutputOriginal = null;
		
		private MapFile.Writer writerUserDisposition = null;
		private MapFile.Writer writerUserPreferences = null;
		private MapFile.Writer writerUserPreferencesOnCategory = null;
		private MapFile.Writer writerUserAverageValueOnCategory = null;
		private double totalAverage = Double.MIN_VALUE;
		
		private TaskID thisTaskID = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<LongWritable,UserPreferenceWithCategories,Text,Text>.Context context) throws IOException ,InterruptedException {
			
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			thisTaskID = context.getTaskAttemptID().getTaskID();
			
			{
				String pathStr = pathStrOutput + File.separator + "UserDisposition" + File.separator + thisTaskID; // reduce task id
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				writerUserDisposition = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), LongWritable.class, DoubleWritable.class);
			}
			
			{
				String pathStr = pathStrOutput + File.separator + "UserPreferences" + File.separator + thisTaskID;
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				writerUserPreferences = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), LongWritable.class, UserPreferenceWithCategoriesArray.class);
			}
			
			{
				String pathStr = pathStrOutput + File.separator + "UserPreferencesOnACategory" + File.separator + thisTaskID;
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				writerUserPreferencesOnCategory = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), LongWritable.class, MapWritable.class);
			}
			
			{
				String pathStr = pathStrOutput + File.separator + "UserAverageValueOnACategory" + File.separator + thisTaskID;
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				writerUserAverageValueOnCategory = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), LongWritable.class, MapWritable.class);
			}
			
			{
				// get total average value
				DataInputStream dis;
				try {
					dis = new DataInputStream(fs.open(new Path(pathStrOutputOriginal + File.separator + "TotalAverage_Final")));
					BufferedReader br = new BufferedReader(new InputStreamReader(dis));
					totalAverage = Double.valueOf(br.readLine());
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<LongWritable,UserPreferenceWithCategories,Text,Text>.Context context) throws IOException ,InterruptedException 
		{
			writerUserDisposition.close();
			writerUserPreferences.close();
			writerUserPreferencesOnCategory.close();
			writerUserAverageValueOnCategory.close();
			super.cleanup(context);
		};
		
		/**
		 * @param key : userID
		 */
		public void reduce(LongWritable key, Iterable<UserPreferenceWithCategories> values, Context context) {
		
			LinkedList<UserPreferenceWithCategories> ll = new LinkedList<UserPreferenceWithCategories>();
			
			for(UserPreferenceWithCategories upwc : values){
				ll.add(new UserPreferenceWithCategories(upwc));
			}
			
			Collections.sort(ll);
				
				int sumPref = 0;
				int counterPref = ll.size();
				
				for(UserPreferenceWithCategories upwc : ll)
					sumPref += upwc.rating;
				
				double userDisposition = (sumPref / (double) counterPref) / totalAverage;
				
				// MAP FILE
				try {
					writerUserDisposition.append(key, new DoubleWritable(userDisposition));
				} catch (IOException e) {
					e.printStackTrace();
				}
		
			/*	User's Preferences */   
				// MAP FILE
				try {
					UserPreferenceWithCategories[] upwcArray = ll.toArray(new UserPreferenceWithCategories[0]);
					UserPreferenceWithCategoriesArray upwca = new UserPreferenceWithCategoriesArray();
					
					for(int i = 0 ; i < upwcArray.length ; i++){
						upwcArray[i].rating = userDisposition * upwcArray[i].rating;
						if(upwcArray[i].rating > 5.0)
							upwcArray[i].rating = 5.0;
					}
					
					upwca.set(upwcArray);
					writerUserPreferences.append(key, upwca);
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			/* User's Preferences on a category */
				HashMap<Integer, HashSet<UserPreferenceNoCategories>> hmUserPreferencesOnACategory = new HashMap<Integer, HashSet<UserPreferenceNoCategories>>();
				
				TreeSet<Integer> tsCategoriesMet = new TreeSet<Integer>();
				
				for(UserPreferenceWithCategories upwc : ll){
					int[] categoriesArray = upwc.categoriesArray;
					int categoriesArrayLength = categoriesArray.length;
					
					for(int i = 0 ; i < categoriesArrayLength; i++){
						tsCategoriesMet.add(categoriesArray[i]);
					}
					
					for(int i = 0 ; i < categoriesArrayLength; i++){
						Integer categoryID = new Integer(categoriesArray[i]);
						
						HashSet<UserPreferenceNoCategories> hsUpnc = hmUserPreferencesOnACategory.get(categoryID);
						if(hsUpnc == null){
							hsUpnc = new HashSet<UserPreferenceNoCategories>();
						}
						
						UserPreferenceNoCategories upnc = new UserPreferenceNoCategories(upwc.itemID, upwc.rating);
						
						hsUpnc.add(upnc);
						hmUserPreferencesOnACategory.put(categoryID, hsUpnc);
					}
					
				}
				
				MapWritable mw_upoac = new MapWritable();
				Iterator<Integer> it = tsCategoriesMet.iterator();
				while(it.hasNext()) { 
					Integer categoryID = it.next();
					HashSet<UserPreferenceNoCategories> hashUpnc = hmUserPreferencesOnACategory.get(categoryID);
					
					UserPreferenceNoCategories[] upncArray = hashUpnc.toArray(new UserPreferenceNoCategories[0]);
					
					UserPreferenceNoCategoriesArray upnca = new UserPreferenceNoCategoriesArray();
					upnca.set(upncArray);
					
					mw_upoac.put(new IntWritable(categoryID.intValue()), upnca);
				}
				
				// MAP FILE
				try {	
	
					writerUserPreferencesOnCategory.append(key, mw_upoac);
	
				} catch (IOException e) {
					e.printStackTrace();
				}	
		
			/* User's average value on a category */
				MapWritable mwUserAvgOnACategory = new MapWritable();
				
				it = tsCategoriesMet.iterator();
				while(it.hasNext()) { 
					Integer categoryID = it.next();
					HashSet<UserPreferenceNoCategories> hsUpnc = hmUserPreferencesOnACategory.get(categoryID);
					
					double sum = 0.0;
					int counter = 0;
					Iterator<UserPreferenceNoCategories> it_upnc =  hsUpnc.iterator();
					while(it_upnc.hasNext()){
						UserPreferenceNoCategories upnc= it_upnc.next();
						
						counter++;
						sum += upnc.rating;
					}
					double avg = sum / counter;
					
					mwUserAvgOnACategory.put(new IntWritable(categoryID.intValue()), new DoubleWritable(avg));
				}
				
				// MAP FILE
				try {
					writerUserAverageValueOnCategory.append(key, mwUserAvgOnACategory);
				} catch (IOException e) {
					e.printStackTrace();
				}
	    }
	}
	
	/* Merging temp files into larger ones, that contain all the records.
	   e.g. For UserPreferences we made a MapFile for each user.
	  		
	  			We will open each user's MapFile which will contain only one record
	  			<userID, UserPreferenceWithCategoriesArray> and save that record in the
	  			new file that will contain all the records together.
	  			
	  			So the final MapFile will contain all the records together:
	  			<userID1, UserPreferenceWithCategoriesArray_1>
	  			<userID2, UserPreferenceWithCategoriesArray_2>
	  			...
	  			<userIDN, UserPreferenceWithCategoriesArray_N>
	 */
	public static void mergeMapFilesGeneral(String pathStrInput, int selectToMerge){
		try{
			String pathStrOutput = pathStrInput + "_Final";
			Path pathOutput = new Path(pathStrOutput);
			Path pathQualifiedDirNameOutput = fs.makeQualified(pathOutput);
		    MapFile.Writer writer = null;
		    
			FileStatus[] fstatArrayMapFiles = fs.listStatus(new Path(pathStrInput));
			
			//
			LocalFileSystem lfs = LocalFileSystem.getLocal(conf);
			
			if(selectToMerge == 1)
			{
				lfs.delete(new Path("localMapFiles"), true);
			    lfs.mkdirs(new Path("localMapFiles"));
			}
   
		    for(FileStatus fstat : fstatArrayMapFiles)
		    {
		    	Path pathHDFS = fstat.getPath();
		    	Path pathLocal = new Path( "localMapFiles" + File.separator + selectToMerge + File.separator + fstat.getPath().getName());
		    	fs.copyToLocalFile(pathHDFS, pathLocal);
		    }

		    FileStatus[] fstatMapFilesLocal = lfs.listStatus(new Path("localMapFiles" + File.separator + selectToMerge));
			//
		    
		    if(selectToMerge == 1) //	User's Preferences
		    	writer = new MapFile.Writer(conf, fs, pathQualifiedDirNameOutput.toString(), LongWritable.class, UserPreferenceWithCategoriesArray.class);
	    	else if(selectToMerge == 2)	//	User's Preferences on a Category
	    		writer = new MapFile.Writer(conf, fs, pathQualifiedDirNameOutput.toString(), LongWritable.class, MapWritable.class);
	    	else if(selectToMerge == 3)	//	User's disposition
	    		writer = new MapFile.Writer(conf, fs, pathQualifiedDirNameOutput.toString(), LongWritable.class, DoubleWritable.class);
	    	else if(selectToMerge == 4)	//	User's average value on a category
	    		writer = new MapFile.Writer(conf, fs, pathQualifiedDirNameOutput.toString(), LongWritable.class, MapWritable.class);
		    
		    TreeMap<LongWritable, MapFile.Reader> tmValues = new TreeMap<LongWritable, MapFile.Reader>();

		    for(FileStatus fstatLocal : fstatMapFilesLocal)
		    {
		    	Path fstatPath = fstatLocal.getPath();
		    	Path qualifiedFstatPath = lfs.makeQualified(fstatPath);
		    	MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedFstatPath.toString(),conf);
		    	
		    	if (selectToMerge == 1) {	//	User's Preferences
		    		UserPreferenceWithCategoriesArray upwca = new UserPreferenceWithCategoriesArray();
			    	LongWritable lwKey = new LongWritable(Long.MIN_VALUE);
			    	lwKey = (LongWritable) reader.getClosest(lwKey, upwca);	//	key: userID
			    	
			    	if(lwKey != null)
			    		tmValues.put(lwKey, reader);
			    	else
			    		reader.close();
		    	} else if (selectToMerge == 2) {	//	User's Preferences on a Category
		    		MapWritable mw = new MapWritable();
		    		LongWritable lwKey = new LongWritable(Long.MIN_VALUE);
		    		lwKey = (LongWritable) reader.getClosest(lwKey, mw);	//	key: userID
			    	
		    		if(lwKey != null)
			    		tmValues.put(lwKey, reader);
			    	else
			    		reader.close();
		    	} else if (selectToMerge == 3) {	//	User's disposition
		    		DoubleWritable dw = new DoubleWritable();
		    		LongWritable lwKey = new LongWritable(Long.MIN_VALUE);
			    	lwKey = (LongWritable) reader.getClosest(lwKey, dw);	//	key: userID
			    	
			    	if(lwKey != null)
			    		tmValues.put(lwKey, reader);
			    	else
			    		reader.close();
		    	} else if (selectToMerge == 4) {	//	User's average value on a category
		    		MapWritable mw = new MapWritable();
		    		LongWritable lwKey = new LongWritable(Long.MIN_VALUE);
			    	lwKey = (LongWritable)reader.getClosest(lwKey, mw);	//	key: userID
			    	
			    	if(lwKey != null)
			    		tmValues.put(lwKey, reader);
			    	else
			    		reader.close();
		    	}
		    }
		    
		    
		    while(true) {
		    	
		    	if(tmValues.size() == 0)
		    		break;
		    	
		    	Entry<LongWritable, MapFile.Reader> entry = tmValues.firstEntry();
		    	LongWritable lwTmKey = entry.getKey();
		    	MapFile.Reader reader = entry.getValue();
		    	
		    	if (selectToMerge == 1) {	//	User's Preferences
		    		keys_MapFile_UserPreferences++;
		    		UserPreferenceWithCategoriesArray upwca = new UserPreferenceWithCategoriesArray();
			    	reader.get(lwTmKey, upwca);	//	key: userID
			    	LongWritable lwKey = new LongWritable(lwTmKey.get());
			    	writer.append(lwKey, upwca);
			    	boolean existsNext = reader.next(lwKey, upwca);
			    	tmValues.remove(lwTmKey);
			    	if(!existsNext)
			    		reader.close();
			    	else
			    		tmValues.put(lwKey, reader);
			    	
		    	} else if (selectToMerge == 2) {	//	User's Preferences on a Category
		    		keys_MapFile_UserPreferencesOnACategory++;
		    		MapWritable mw = new MapWritable();
		    		reader.get(lwTmKey, mw);	//	key: userID
			    	LongWritable lwKey = new LongWritable(lwTmKey.get());
			    	writer.append(lwKey, mw);
			    	boolean existsNext = reader.next(lwKey, mw);
			    	tmValues.remove(lwTmKey);
			    	if(!existsNext)
			    		reader.close();
			    	else
			    		tmValues.put(lwKey, reader);
		    		
		    	} else if (selectToMerge == 3) {	//	User's disposition
		    		keys_MapFile_UserDisposition++;
		    		DoubleWritable dw = new DoubleWritable();
		    		reader.get(lwTmKey, dw);	//	key: userID
			    	LongWritable lwKey = new LongWritable(lwTmKey.get());
			    	writer.append(lwKey, dw);
			    	boolean existsNext = reader.next(lwKey, dw);
			    	tmValues.remove(lwTmKey);
			    	if(!existsNext)
			    		reader.close();
			    	else
			    		tmValues.put(lwKey, reader);
		    		
		    	} else if (selectToMerge == 4) {	//	User's average value on a category
		    		keys_MapFile_UserAverageValueOnACategory++;
		    		MapWritable mw = new MapWritable();
		    		reader.get(lwTmKey, mw);	//	key: userID
			    	LongWritable lwKey = new LongWritable(lwTmKey.get());
			    	writer.append(lwKey, mw);
			    	boolean existsNext = reader.next(lwKey, mw);
			    	tmValues.remove(lwTmKey);
			    	if(!existsNext)
			    		reader.close();
			    	else
			    		tmValues.put(lwKey, reader);
		    	}
		    }
		    
		    writer.close();
		    if(selectToMerge == 4)
		    	lfs.delete(new Path("localMapFiles"), true);
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private static void calculateFileSize(String inputPath, int selectToCalculate) throws IOException
	{
		FileStatus indexFileStatus = fs.getFileStatus(new Path(inputPath + File.separator + "index"));

		if(selectToCalculate == 1)
			index_sizeInBytes_MapFile_UserPreferences = indexFileStatus.getLen();
		else if(selectToCalculate == 2)
			index_sizeInBytes_MapFile_UserPreferencesOnACategory = indexFileStatus.getLen();
		else if(selectToCalculate == 3)
			index_sizeInBytes_MapFile_UserDisposition = indexFileStatus.getLen();
		else if(selectToCalculate == 4)
			index_sizeInBytes_MapFile_UserAverageValueOnACategory = indexFileStatus.getLen();
	}
	
	private static void calculateFileSizes()
	{
		try{
			calculateFileSize(pathStrOutput + File.separator + "UserPreferences" + "_Final", 1);
			calculateFileSize(pathStrOutput + File.separator + "UserPreferencesOnACategory" + "_Final", 2);
			calculateFileSize(pathStrOutput + File.separator + "UserDisposition" + "_Final", 3);
			calculateFileSize(pathStrOutput + File.separator + "UserAverageValueOnACategory" + "_Final", 4);
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	/* Merging files for: 	User Preferences 
							User Preferences on a Category
							User Disposition
							User Average value on a Category 
		
		We read the temp outputs and append them in a new file which ends with the String "_Final".
	 */
	public static void mergeMapFiles() throws IOException{
		mergeMapFilesGeneral(pathStrOutput + File.separator + "UserPreferences", 1);
		mergeMapFilesGeneral(pathStrOutput + File.separator + "UserPreferencesOnACategory", 2);
		mergeMapFilesGeneral(pathStrOutput + File.separator + "UserDisposition", 3);
		mergeMapFilesGeneral(pathStrOutput + File.separator + "UserAverageValueOnACategory", 4);
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evGeneral.file_MapFile_UserPreferences_keys = keys_MapFile_UserPreferences;
		evcg.evGeneral.file_MapFile_UserPreferences_index_sizeInBytes = index_sizeInBytes_MapFile_UserPreferences;
		evcg.evGeneral.file_MapFile_UserPreferencesOnACategory_keys = keys_MapFile_UserPreferencesOnACategory;
		evcg.evGeneral.file_MapFile_UserPreferencesOnACategory_index_sizeInBytes = index_sizeInBytes_MapFile_UserPreferencesOnACategory;
		evcg.evGeneral.file_MapFile_UserDisposition_keys = keys_MapFile_UserDisposition;
		evcg.evGeneral.file_MapFile_UserDisposition_index_sizeInBytes = index_sizeInBytes_MapFile_UserDisposition;
		evcg.evGeneral.file_MapFile_UserAverageValueOnACategory_keys = keys_MapFile_UserAverageValueOnACategory;
		evcg.evGeneral.file_MapFile_UserAverageValueOnACategory_index_sizeInBytes = index_sizeInBytes_MapFile_UserAverageValueOnACategory;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputOriginal", pathStrOutputOriginal);
	}
	
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("User Pipeline Driver - Start");
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
	
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator +"UserPipeline";
		pathStrOutputOriginal = evcg.getOutputPathGeneral();
		
		setParameters(conf);
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput)))
			{
				System.out.println("*User Pipeline Driver*");
				System.out.println("Folder: " + pathStrOutput);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}
		
		long start = System.nanoTime();

		fs.delete(new Path(pathStrOutput), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "UserPipeline");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(UserPipelineDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		  
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(UserPreferenceWithCategories.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);
		
		mergeMapFiles();
		calculateFileSizes();
		updateInfo(evcg);
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
		{
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "UserPreferences"));
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "UserPreferencesOnACategory"));
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "UserDisposition"));
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "UserAverageValueOnACategory"));
		}
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_UserPipelineDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("User Pipeline Driver - Stop");
		evcg.hasChangedGeneral = true;
		
		System.runFinalization();
		System.gc();
	}
}
