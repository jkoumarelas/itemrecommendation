package userPipeline;

import org.apache.hadoop.io.ArrayWritable;

/**
 * Objects from this class are saved in the secondary structure of 
 * UserPreferences.
 * 
 * @author John Koumarelas
 *
 */
public class UserPreferenceWithCategoriesArray extends ArrayWritable { 
	public UserPreferenceWithCategoriesArray() 
	{ 
		super(UserPreferenceWithCategories.class); 
	} 
}