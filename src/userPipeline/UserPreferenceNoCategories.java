package userPipeline;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * Objects from this class (grouped in UserPreferenceNoCategoriesArray) are 
 * saved in the secondary structure of UserPreferences On A Category.
 * 
 * @author John Koumarelas
 *
 */
public class UserPreferenceNoCategories implements WritableComparable<UserPreferenceNoCategories>{
	public long itemID;
	public double rating;
	
	public UserPreferenceNoCategories() {
		itemID = -1;
		rating = -1;
	}
	
	public UserPreferenceNoCategories(long itemID,double rating) {
		this.itemID = itemID;
		this.rating = rating;
	}
	
	public UserPreferenceNoCategories(UserPreferenceNoCategories up) {
		this.itemID = up.itemID;
		this.rating = up.rating;
	}
	
	@Override
	public int compareTo(UserPreferenceNoCategories o) {
		if( o == this) return 0;
		if( itemID == o.itemID) return 0;
		else
			return ( itemID < o.itemID ? -1 : 1);
	}
	
	@Override
	public int hashCode() {
		return (int)itemID;
	}
	
	@Override
	public String toString(){
		return "itemID: " + String.valueOf(itemID) +" rating:" + String.valueOf(rating);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		itemID = in.readLong();	
		rating = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(itemID);
		out.writeDouble(rating);
	}
}
