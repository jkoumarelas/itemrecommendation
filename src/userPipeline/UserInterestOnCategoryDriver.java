package userPipeline;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

/**
 * 
 * This class will output MapFiles for the secondary structure:
 * 		-	User Interest On a Category
 * 
 * @author John Koumarelas
 *
 */
public class UserInterestOnCategoryDriver {

	private static String pathStrOutput = null;
	
	private static String pathStrUserPreferencesOnACategory = null;
	private static String pathStrUserAvgValOnACategory = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	private static double b_Fmeasure = 0.0;

	private static long keys_MapFile_UserInterestOnCategory = 0L;
	private static long index_sizeInBytes_MapFile_UserInterestOnCategory = 0L;
	
	public static void clearFields()
	{
		keys_MapFile_UserInterestOnCategory = 0L;
		index_sizeInBytes_MapFile_UserInterestOnCategory = 0L;
	}

	public static class Map extends Mapper<LongWritable, Text, LongWritable, IntWritable> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			String strValue = value.toString();
			String[] strArrayUserPreferences = strValue.split("\\t");
		
			String strUserID = strArrayUserPreferences[0];
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strArrayUserPreferences.length ; i++){
				LongWritable lwUserID = new LongWritable( Long.parseLong(strUserID));
				context.write(lwUserID, new IntWritable(0));
			}
			
		}
	}
	
	public static class Reduce extends Reducer<LongWritable, IntWritable, Text, Text> {
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		private String pathStrUserPreferencesOnACategory = null;
		private String pathStrUserAvgValOnACategory = null;
		private MapFile.Writer writer = null;
		private MapFile.Reader reader_upoac = null;
		private MapFile.Reader reader_uavoac = null;
		
		private double b_Fmeasure = 0.0;
		
		private TaskID thisTaskID = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<LongWritable,IntWritable,Text,Text>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrUserPreferencesOnACategory = conf.get("pathStrUserPreferencesOnACategory");
			pathStrUserAvgValOnACategory = conf.get("pathStrUserAvgValOnACategory");
			
			b_Fmeasure = Double.parseDouble(conf.get("b_Fmeasure"));
			
			thisTaskID = context.getTaskAttemptID().getTaskID();
			
			{
				String pathStr = pathStrOutput + File.separator + "UserInterestOnCategory" + File.separator + thisTaskID;
				Path path = new Path(pathStr);
				Path pathQualified = fs.makeQualified(path);
			    writer = new MapFile.Writer(conf, fs, pathQualified.toString(), LongWritable.class, MapWritable.class);
			}
			{
				// Read User Preferences on a Category
				Path path= new Path(pathStrUserPreferencesOnACategory);
		    	Path pathQualified = fs.makeQualified(path);
				reader_upoac = new MapFile.Reader(fs, pathQualified.toString(),conf);
			}
			{
		    	// Read User Average value on a Category
		    	Path path= new Path(pathStrUserAvgValOnACategory);
		    	Path pathQualified = fs.makeQualified(path);
				reader_uavoac = new MapFile.Reader(fs, pathQualified.toString(),conf);
			}
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<LongWritable,IntWritable,Text,Text>.Context context) throws IOException ,InterruptedException {
			writer.close();
			reader_upoac.close();
			reader_uavoac.close();
			super.cleanup(context);
		};
		
		//	cL: categoryLikeness, cP: categoryPopularity
		private static double Fmeasure(double cL, double cP, double b_Fmeasure){
			return ((1+Math.pow(b_Fmeasure,2.0)) * cL * cP ) / (Math.pow(b_Fmeasure,2.0)*cL+cP);
		}
		
		@Override
		public void reduce(LongWritable key, Iterable<IntWritable> values, Context context) {
			
			long numOfUserPreferences = 0L;
			for(IntWritable lw : values){
				numOfUserPreferences++;
			}

	    	MapWritable mw_upoac = new MapWritable();	//	upoac : User Preferences On A Category
	    	try {
				reader_upoac.get(key, mw_upoac);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

	    	MapWritable mw_uavoac = new MapWritable();	//	uavoac : User's Average Value On A Category
	    	try {
				reader_uavoac.get(key, mw_uavoac);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	    	
	    	//
	    	
	    	TreeSet<Integer> tsCategoriesMet = new TreeSet<Integer>();
	    
	    	Iterator<Entry<Writable,Writable>> itEntry = mw_uavoac.entrySet().iterator();
	    	
	    	double sumOfAllAvg = 0.0;
	    	
			while(itEntry.hasNext()){
				Entry<Writable,Writable> entry = itEntry.next();
				IntWritable iwCategoryID = (IntWritable) entry.getKey();
				DoubleWritable dwAvgValue = (DoubleWritable) entry.getValue();
				
				sumOfAllAvg += dwAvgValue.get();
				
				tsCategoriesMet.add(iwCategoryID.get());
			}
			
			MapWritable mw_uioac = new MapWritable();	//	uioac : User Interest On A Category
			HashMap<Integer, Double> hmTempCategoriesFmeasures = new HashMap<Integer, Double>();
			
			double sumOfFmeasures = 0.0;
			
			Iterator<Integer> it = tsCategoriesMet.iterator();
			while(it.hasNext()) { 
				Integer categoryID = it.next();
				
				UserPreferenceNoCategoriesArray upnca = null; // new UserPreferenceNoCategoriesArray();
				upnca = (UserPreferenceNoCategoriesArray) mw_upoac.get(new IntWritable(categoryID.intValue()));
				
				double categoryLikeness = 0.0;
				DoubleWritable dwUserAvgValOnCategory = (DoubleWritable) mw_uavoac.get(new IntWritable(categoryID.intValue()));
				categoryLikeness = dwUserAvgValOnCategory.get() / sumOfAllAvg;
				
				double categoryPopularity = upnca.get().length / (double) numOfUserPreferences;
	
				double valFmeasure = Fmeasure(categoryLikeness, categoryPopularity, b_Fmeasure);
				
				sumOfFmeasures += valFmeasure;
			
				hmTempCategoriesFmeasures.put(categoryID, new Double(valFmeasure));
			}
			
			it = tsCategoriesMet.iterator();
			while(it.hasNext()) { 
				Integer categoryID = it.next();
				Double valFmeasure = hmTempCategoriesFmeasures.get(categoryID.intValue());
				
				mw_uioac.put(new IntWritable(categoryID.intValue()), new DoubleWritable(valFmeasure/sumOfFmeasures));
			}
			
			try {
				writer.append(key, mw_uioac);
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	}
	
	/* Merging temp files into larger ones, that contain all the records.
	 */
	public static void mergeMapFilesGeneral(String inputPath){
		try{
			String pathStr = inputPath + "_Final";
			Path pathOutput = new Path(pathStr);
			Path pathQualifiedOutput = fs.makeQualified(pathOutput);
		    MapFile.Writer.setIndexInterval(conf, 1);
		    MapFile.Writer writer = null;
		    
			FileStatus[] fstatArrayMapFiles = fs.listStatus(new Path(inputPath));
			
			//
		    LocalFileSystem lfs = LocalFileSystem.getLocal(conf);
		    
		    lfs.delete(new Path("localMapFiles"), true);
		    lfs.mkdirs(new Path("localMapFiles"));
		    
		    for(FileStatus fstat : fstatArrayMapFiles)
		    {
		    	Path pathHDFS = fstat.getPath();
		    	Path pathLocal = new Path( "localMapFiles" + File.separator + fstat.getPath().getName());
		    	fs.copyToLocalFile(pathHDFS, pathLocal);
		    }

		    FileStatus[] fstatMapFilesLocal = lfs.listStatus(new Path("localMapFiles"));
			//
			
			//	User's interest on a category
	    	writer = new MapFile.Writer(conf, fs, pathQualifiedOutput.toString(), LongWritable.class, MapWritable.class);
		    
	    	TreeMap<LongWritable, MapFile.Reader> tmValues = new TreeMap<LongWritable, MapFile.Reader>();
	    	
	    	for(FileStatus fstat : fstatMapFilesLocal)
	    	{
	    		Path pathInput = fstat.getPath();
		    	Path pathQualifiedInput = lfs.makeQualified(pathInput);
		    	MapFile.Reader reader = new MapFile.Reader(lfs, pathQualifiedInput.toString(),conf);
		    	
		    	MapWritable mw = new MapWritable();
	    		LongWritable lwKey = new LongWritable(Long.MIN_VALUE);
		    	lwKey = (LongWritable) reader.getClosest(lwKey, mw);	//	key: userID
		    	
		    	if(lwKey != null)
		    		tmValues.put(lwKey, reader);
		    	else
		    		reader.close();
	    	}
	    	
		    while(true) {
		    	if(tmValues.size() == 0)
		    		break;

		    	keys_MapFile_UserInterestOnCategory++;
		    	
		    	Entry<LongWritable, MapFile.Reader> entry = tmValues.firstEntry();
		    	LongWritable lwTmKey = entry.getKey();
		    	MapFile.Reader reader = entry.getValue();
		    	
	    		MapWritable mw = new MapWritable();
	    		LongWritable lwKey = new LongWritable(lwTmKey.get());
	    		
		    	reader.get(lwKey, mw);	//	key: userID
		    	writer.append(lwKey, mw);
		    	boolean existsNext = reader.next(lwKey, mw);
		    	tmValues.remove(lwTmKey);
		    	if(!existsNext)
		    		reader.close();
		    	else
		    		tmValues.put(lwKey, reader);
		    }
		    
		    writer.close();
		    lfs.delete(new Path("localMapFiles"), true);
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}

	/* 
		We read the temp outputs and append them in a new file which ends with the String "_Final".
	 */
	public static void mergeMapFiles() throws IOException{
		mergeMapFilesGeneral(pathStrOutput + File.separator + "UserInterestOnCategory");
	}
	
	private static void calculateFileSizes()
	{
		try
		{
			FileStatus indexFileStatus = fs.getFileStatus(new Path(pathStrOutput + File.separator + "UserInterestOnCategory_Final" + File.separator + "index"));
			index_sizeInBytes_MapFile_UserInterestOnCategory = indexFileStatus.getLen();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evGeneral.keys_MapFile_UserInterestOnCategory = keys_MapFile_UserInterestOnCategory;
		evcg.evGeneral.index_sizeInBytes_MapFile_UserInterestOnCategory = index_sizeInBytes_MapFile_UserInterestOnCategory;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrUserPreferencesOnACategory", pathStrUserPreferencesOnACategory);
		conf.set("pathStrUserAvgValOnACategory", pathStrUserAvgValOnACategory);
		conf.set("b_Fmeasure", String.valueOf(b_Fmeasure));
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("User Interest On Category Driver - Start");

		conf = new Configuration();
		fs = FileSystem.get(conf);
	
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator +"UserPipeline";
		pathStrUserPreferencesOnACategory = pathStrOutput + File.separator + "UserPreferencesOnACategory_Final";
		pathStrUserAvgValOnACategory = pathStrOutput + File.separator + "UserAverageValueOnACategory_Final";
		
		UserInterestOnCategoryDriver.b_Fmeasure = evcg.evGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
		
		setParameters(conf);
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "UserInterestOnCategory_Final")))
			{
				System.out.println("*User Interest On Category Driver*");
				System.out.println("Folder: " + pathStrOutput + File.separator + "UserInterestOnCategory_Final");
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "UserInterestOnCategory_Final")))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}
		
		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput + File.separator + "UserInterestOnCategory"), true); // true: recursively
		fs.delete(new Path(pathStrOutput + File.separator + "UserInterestOnCategory_Final"), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "UserInterestOnCategory");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(UserInterestOnCategoryDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(IntWritable.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);
		
		mergeMapFiles();
		calculateFileSizes();
		updateInfo(evcg);
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "UserInterestOnCategory"));
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_UserInterestOnCategoryDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("User Interest On Category Driver - Stop");
		evcg.hasChangedGeneral = true;
		
		System.runFinalization();
		System.gc();
	}
	
}
