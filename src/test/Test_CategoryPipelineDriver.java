package test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.LongArrayWritable;

/**
 * 
 * This test class helps for debugging purposes by printing values
 * from the CategoryPipelineDriver.
 * 
 * @author John Koumarelas
 *
 */
public class Test_CategoryPipelineDriver {
	public static String outputPath = null;
	public static Configuration conf = null;
	private static FileSystem fs = null;

	public static class Map extends Mapper<LongWritable, Text, IntWritable, LongWritable> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
			
			String strUserID = strUserPreferences[0];
		
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
				
				String[] strItemInfo = strUP.split("-");
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strCategoriesIDArray = strItemInfo[1].split("_");
				
				for(String strCategoryID : strCategoriesIDArray){
					context.write(new IntWritable(Integer.parseInt(strCategoryID)), new LongWritable(Long.parseLong(strUserID)));
				}
			}
			
		}
	}
	
	public static class Reduce extends Reducer<IntWritable, LongWritable, Text, Text> {
		
		public void showMapFiles(String inputPath, IntWritable key, Context context) {
			Configuration conf = context.getConfiguration();
			
			try {
				String pathStr = inputPath + "_Final";
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				MapFile.Reader reader = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
				
				System.out.println("categoryID: " + key.toString());
				
				LongArrayWritable law = new LongArrayWritable();
		    	reader.get(key, law);
		    	
		    	Writable[] writableArray = law.get();
		    	LongWritable[] lwArrayUserIDs = new LongWritable[writableArray.length];
		    	
		    	for(int i = 0 ; i < writableArray.length ; i++)
		    		lwArrayUserIDs[i] = (LongWritable) writableArray[i];
		    	
		    	//	Print
		    	for(LongWritable iw : lwArrayUserIDs)
		    		System.out.println(iw.toString());
			    		
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		@Override
		public void reduce(IntWritable key, Iterable<LongWritable> values, Context context) {
			System.out.println("Category Set of Users");
			showMapFiles(outputPath + File.separator + "CategorySetOfUsers",key,context);
	    }
	}

	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test Category Pipeline Driver - Start");
	
		long start = System.nanoTime();
	
		outputPath = evcg.getOutputPathGeneral() + File.separator + "CategoryPipeline";
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		Job job = new Job(conf, "Test_CategoryPipeline");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);
	
		job.setJarByClass(Test_CategoryPipelineDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(LongWritable.class); 
		
		//	Reducer
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		job.waitForCompletion(true);
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_Test_CategoryPipelineDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test Category Pipeline Driver - Stop");
	}
}