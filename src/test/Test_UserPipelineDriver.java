package test;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import userPipeline.UserPreferenceNoCategories;
import userPipeline.UserPreferenceNoCategoriesArray;
import userPipeline.UserPreferenceWithCategories;
import userPipeline.UserPreferenceWithCategoriesArray;

/**
 * 
 * This test class helps for debugging purposes by printing values
 * from the UserPipelineDriver.
 * 
 * @author John Koumarelas
 *
 */
public class Test_UserPipelineDriver {
	public static String pathStrOutput = null;
	public static Configuration conf = null;
	private static FileSystem fs = null;
	
	public static class Map extends Mapper<LongWritable, Text, LongWritable, Text> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
			String strUserID = strUserPreferences[0];
			
			context.write(new LongWritable(Long.parseLong(strUserID)), new Text());
		}
	}
	
	public static class Reduce extends Reducer<LongWritable, Text, Text, Text> {
		public static String pathStrOutput = null;
		public static Configuration conf = null;
		private static FileSystem fs = null;
		
		private MapFile.Reader readerUserDisposition = null;
		private MapFile.Reader readerUserPreferences = null;
		private MapFile.Reader readerUserPreferencesOnCategory = null;
		private MapFile.Reader readerAverageValeOnCategory = null;
	
		protected void setup(org.apache.hadoop.mapreduce.Reducer<LongWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
			
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			
			{
				String pathStr = pathStrOutput + File.separator + "UserDisposition" + "_Final";
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				readerUserDisposition = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
			}
			{
				String pathStr = pathStrOutput + File.separator + "UserPreferences" + "_Final";
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				readerUserPreferences = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
			}
			{
				String pathStr = pathStrOutput + File.separator + "UserPreferencesOnACategory" + "_Final";
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				readerUserPreferencesOnCategory = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
			}
			{
				String pathStr = pathStrOutput + File.separator + "UserAverageValueOnACategory" + "_Final";
				Path path = new Path(pathStr);
				Path qualifiedDirName = fs.makeQualified(path);
				readerAverageValeOnCategory = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
			}
			
			super.setup(context);
		};
	
			public void showMapFiles(String inputPath, int selectToShow, LongWritable key, Context context) {
				try {
					
					if( selectToShow == 1){	//	User's Preferences
						UserPreferenceWithCategoriesArray cpa = new UserPreferenceWithCategoriesArray();
						readerUserPreferences.get(key, cpa);
						Writable[] writableArray = cpa.get();
						UserPreferenceWithCategories[] upwcArray = new UserPreferenceWithCategories[writableArray.length];
						
						for(int i = 0 ; i < writableArray.length ; i++)
							upwcArray[i] = (UserPreferenceWithCategories) writableArray[i];
						
						//	Print
						for(UserPreferenceWithCategories upwc : upwcArray)
							System.out.println(key.get() + "," + upwc.itemID + "," + upwc.rating);
						
					} else if (selectToShow == 2) {	//	User's Preferences on a category
						MapWritable mw = new MapWritable();
						readerUserPreferencesOnCategory.get(key, mw);
				    	Set<Entry<Writable,Writable>> upMapSet = mw.entrySet(); 
				    	Iterator<Entry<Writable,Writable>> it = upMapSet.iterator();
				    	
				    	while(it.hasNext()){
				    		Entry<Writable,Writable> entry = it.next();
				    		UserPreferenceNoCategoriesArray entryValue = (UserPreferenceNoCategoriesArray)entry.getValue();
				    		
				    		Writable[] wArray = entryValue.get();
	
				    		System.out.print("\t");
				    		for(int i = 0 ; i < wArray.length ; i++)
				    		{
				    			if((i+1)!= wArray.length)
				    				System.out.print(((UserPreferenceNoCategories) wArray[i]).toString() + ", ");
				    			else
				    				System.out.print(((UserPreferenceNoCategories) wArray[i]).toString());
				    		}
				    		System.out.println("");
				    	}
				    		
					} else if (selectToShow == 3) {	//	User's disposition
						DoubleWritable dw = new DoubleWritable();
						readerUserDisposition.get(key, dw);
				    	
				    	System.out.println("disposition: " + dw.toString());
					} else if (selectToShow == 4) {	//	User's average value on a category
						MapWritable mw = new MapWritable();
						readerAverageValeOnCategory.get(key, mw);
				    	Set<Entry<Writable,Writable>> upMapSet = mw.entrySet(); 
				    	Iterator<Entry<Writable,Writable>> it = upMapSet.iterator();
				    	
				    	while(it.hasNext()){
				    		Entry<Writable,Writable> entry = it.next();
				    		IntWritable entryKey = (IntWritable)entry.getKey();
				    		DoubleWritable entryValue = (DoubleWritable)entry.getValue();
				    		
//				    		System.out.println("categoryID: " + entryKey.toString() + " average_value: " + entryValue.toString());
				    		System.out.println(key.get() + "," + entryKey.get() + "," + entryValue.toString());
				    	}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			@Override
			public void reduce(LongWritable key, Iterable<Text> values, Context context) {				
				showMapFiles(pathStrOutput + File.separator + "UserPreferences",1,key,context);
		    }
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test User Pipeline Driver - Start");
	
		long start = System.nanoTime();
	
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator +"UserPipeline";
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		conf.set("pathStrOutput", pathStrOutput);
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		Job job = new Job(conf, "Test_UserPipeline");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);
	
		job.setJarByClass(Test_UserPipelineDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class); 
		
		//	Reducer
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	
		job.waitForCompletion(true);
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_Test_UserPipelineDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test User Pipeline Driver - Stop");
	}
}
