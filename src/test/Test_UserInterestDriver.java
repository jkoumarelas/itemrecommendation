package test;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

/**
 * 
 * This test class helps for debugging purposes by printing values
 * from the UserInterestDriver.
 * 
 * @author John Koumarelas
 *
 */
public class Test_UserInterestDriver {
	public static String outputPath = null;
	public static Configuration conf = null;
	private static FileSystem fs = null;
	
	public static class Map extends Mapper<LongWritable, Text, LongWritable, Text> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
			String strUserID = strUserPreferences[0];
			
			context.write(new LongWritable(Long.parseLong(strUserID)), new Text());
		}
	}
	
public static class Reduce extends Reducer<LongWritable, Text, Text, Text> {
	
	private MapFile.Reader reader = null;
	
	protected void setup(org.apache.hadoop.mapreduce.Reducer<LongWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
		{
			String pathStr = outputPath + File.separator + "UserInterestOnCategory" + "_Final";
			Path path = new Path(pathStr);
			Path qualifiedDirName = fs.makeQualified(path);
			reader = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
		}
		super.setup(context);
	};
	
	protected void cleanup(org.apache.hadoop.mapreduce.Reducer<LongWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
		reader.close();
		super.cleanup(context);
	};
	

		
		public void showMapFiles(String inputPath, LongWritable key, Context context) {
			
			try {
				MapWritable mw = new MapWritable();
		    	reader.get(key, mw);
		    	Set<Entry<Writable,Writable>> upMapSet = mw.entrySet(); 
		    	Iterator<Entry<Writable,Writable>> it = upMapSet.iterator();
		    	
		    	while(it.hasNext()){
		    		Entry<Writable,Writable> entry = it.next();
		    		IntWritable entryKey = (IntWritable)entry.getKey();
		    		DoubleWritable entryValue = (DoubleWritable)entry.getValue();
		    		
		    		System.out.println("categoryID: " + entryKey.toString() + " interest: " + entryValue.toString());
		    	}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		@Override
		public void reduce(LongWritable key, Iterable<Text> values, Context context) {
			System.out.println("userID: " + key.get());
			
			System.out.println("User Interest on a Category");
			showMapFiles(outputPath + File.separator + "UserInterestOnCategory",key,context);
	    }
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test User Interest On Category Driver - Start");
	
		long start = System.nanoTime();
	
		outputPath = evcg.getOutputPathGeneral() + File.separator +"UserPipeline";
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);

		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		Job job = new Job(conf, "Test_UserInterest");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);
	
		job.setJarByClass(Test_UserInterestDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class); 
		
		//	Reducer
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
	
		job.waitForCompletion(true);
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_Test_UserInterestDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test User Interest On Category Driver - Stop");
	}
}
