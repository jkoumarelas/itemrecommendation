package test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.IntPairWritable;

/**
 * 
 * This test class helps for debugging purposes by printing values
 * from the DMatrixDriver.
 * 
 * @author John Koumarelas
 *
 */
public class Test_DMatrixDriver {
	private static String pathStrOutput = null;
	private static String pathStrOutputOriginal = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	// Input: users... At the reducers the average will be computed.
	public static class Map extends Mapper<LongWritable, Text, IntPairWritable, Text> {
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			//	categoriesCombinationStr[0]: categoryID1
			//	categoriesCombinationStr[1]: categoryID2
			String[] categoriesCombinationStr = value.toString().split(",");	//	Categories where separated in file with commas. e.g. 1,2
			
			// We get the common users for these categories.
			System.out.println("value: " + value.toString());
	    	
	    	IntWritable keyCategory1 = new IntWritable(Integer.parseInt(categoriesCombinationStr[0]));
	    	IntWritable keyCategory2 = new IntWritable(Integer.parseInt(categoriesCombinationStr[1]));
	    	
	    	IntPairWritable ipwCategoriesCombination = new IntPairWritable();
	    	ipwCategoriesCombination.set(keyCategory1.get(), keyCategory2.get());

	    	context.write(ipwCategoriesCombination, new Text());
		}
	}
	
	public static class Reduce extends Reducer<IntPairWritable, Text, Text, Text> {
		private static Configuration conf = null;
		private static FileSystem fs = null;
		
		private String pathStrOutput = null;
		
		private MapFile.Reader reader = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");			
			{
				String pathStr = pathStrOutput;
				Path path = new Path(pathStr);
	 	    	Path qualifiedDirName = fs.makeQualified(path);
	 	    	reader = new MapFile.Reader(fs, qualifiedDirName.toString(),conf);
			}
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
			reader.close();
			super.cleanup(context);
		};

		@Override
		public void reduce(IntPairWritable key, Iterable<Text> values, Context context) {
			
	    	int ci = key.getFirst();
			int cj = key.getSecond();
			
			try {
				/* D(ci,cj) */
	 	    	IntPairWritable ci_cj = new IntPairWritable();
		    	ci_cj.set(ci, cj);

			    DoubleWritable dpw_ci_cj = new DoubleWritable();
			    reader.get(ci_cj, dpw_ci_cj);
			    
			    double dmatrixValue_ci_cj = dpw_ci_cj.get();
			    
			    System.out.println("ci: " + ci + " cj: " + cj + " DMatrix_Value: " + dmatrixValue_ci_cj);
			    
			    /* D(cj,ci) */
	 	    	IntPairWritable cj_ci = new IntPairWritable();
		    	cj_ci.set(cj, ci);
	 	    	
			    DoubleWritable dpw_cj_ci = new DoubleWritable();
			    reader.get(cj_ci, dpw_cj_ci);
			    
			    double dmatrixValue_cj_ci = dpw_cj_ci.get();
				
			    System.out.println("ci: " + cj + " cj: " + ci + " DMatrix_Value: " + dmatrixValue_cj_ci);
			    System.out.println("");
	 	    	
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test D Matrix Driver - Start");
		
		long start = System.nanoTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator + "DMatrix_Final";
		pathStrOutputOriginal = evcg.getOutputPathGeneral();
		
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputOriginal", pathStrOutputOriginal);
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.evGeneral.inputPath + File.separator + "filesNeeded" + File.separator + "categoriesCombination" + evcg.evGeneral.datasetName;
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + + dt.getTime();
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		Job job = new Job(conf, "Test_DMatrix");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(Test_DMatrixDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntPairWritable.class);
		job.setMapOutputValueClass(Text.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		job.waitForCompletion(true);
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_Test_DMatrixDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test D Matrix Driver - Stop");
	}
}
