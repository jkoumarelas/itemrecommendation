package test;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.IntArrayWritable;
import commons.IntPairWritable;
import commons.LongArrayWritable;

/**
 * 
 * This test class helps for debugging purposes by printing values
 * from the CategoriesCommonUsersDriver.
 * 
 * @author John Koumarelas
 *
 */
public class Test_CategoriesCommonUsersDriver {
	private static String outputPath = null;
	private static Configuration conf = null;
	private static FileSystem fs = null;

	public static class Map extends Mapper<LongWritable, Text, IntPairWritable, IntArrayWritable> {
		
		private MapFile.Reader reader = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,IntPairWritable,IntArrayWritable>.Context context) throws IOException ,InterruptedException {
			{
				Path pathInput = new Path(outputPath + File.separator + "SetUsersPairCategories" + "_Final");
		    	Path qualifiedDirNameInput = fs.makeQualified(pathInput);
		    	reader = new MapFile.Reader(fs, qualifiedDirNameInput.toString(),conf);
			}
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,IntPairWritable,IntArrayWritable>.Context context) throws IOException ,InterruptedException {
			reader.close();
			super.cleanup(context);
		};
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			
			//	categoriesCombinationStr[0]: categoryID1
			//	categoriesCombinationStr[1]: categoryID2
			String[] categoriesCombinationStr = value.toString().split(",");
			
			IntPairWritable ipw = new IntPairWritable();
	    	ipw.set(Integer.parseInt(categoriesCombinationStr[0]), Integer.parseInt(categoriesCombinationStr[1]));
	    	
	    	LongArrayWritable setOfUsers = new LongArrayWritable();

	    	reader.get(ipw, setOfUsers);
	    	
	    	Writable[] wSetOfUsers = setOfUsers.get();
	    	
	    	if(!(wSetOfUsers != null && wSetOfUsers.length > 0)) return;
	    	
	    	System.out.println("(MAP) Categories: " + ipw.getFirst() + "," + ipw.getSecond());
	    	
	    	for(int i = 0 ; i < wSetOfUsers.length ; i++){
	    		if((i+1)!= wSetOfUsers.length) System.out.print(wSetOfUsers[i] + ",");
	    		else System.out.print(wSetOfUsers[i]);
	    	}
	    	System.out.println("");
	    	
	    	IntArrayWritable iaw = new IntArrayWritable();
	    	iaw.set(new IntWritable[0]);
	    	context.write(ipw, iaw);
	    }
	}
	
	public static class Reduce extends Reducer<IntPairWritable, IntArrayWritable, Text, Text> {
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,IntArrayWritable,Text,Text>.Context context) throws IOException ,InterruptedException {

			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,IntArrayWritable,Text,Text>.Context context) throws IOException ,InterruptedException {
			super.cleanup(context);
		};
		
		@Override
		public void reduce(IntPairWritable key, Iterable<IntArrayWritable> values, Context context) {
	    	
	    }
	}

	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test Categories Common Users Driver - Start");
	
		long start = System.nanoTime();
	
		outputPath = evcg.getOutputPathGeneral() + File.separator + "CategoriesCommonUsers";
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.evGeneral.inputPath + File.separator + "filesNeeded" + File.separator + "categoriesCombination" + evcg.evGeneral.datasetName;
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + dt.getTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		Job job = new Job(conf, "Test_CategoriesCommonUsers");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);
	
		job.setJarByClass(Test_CategoriesCommonUsersDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntPairWritable.class);
		job.setMapOutputValueClass(IntArrayWritable.class); 
		
		//	Reducer
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		job.waitForCompletion(true);
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_Test_CategoriesCommonUsersDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Test Categories Common Users Driver - Stop");
	}
}
