import java.io.IOException;
import java.util.LinkedList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import categoryPipeline.CategoryPipelineDriver;
import userPipeline.UserPipelineDriver;
import userPipeline.UserInterestOnCategoryDriver;
import categoryPipeline.CategoriesCommonUsersDriver;
import dmatrix.DMatrixDriver;
import other.CalculateTotalAverageDriver;
import other.SplitDataset;
import other.executionEnvironment.ExecutionVariablesCurrentGroup;
import other.executionEnvironment.ExecutionVariablesGeneral;
import other.executionEnvironment.ExecutionVariablesOtherRecommendationSystem;
import other.executionEnvironment.ExecutionVariablesSimilarityEstimation;
import other.executionEnvironment.ExecutionVariablesTesting;
import other.executionEnvironment.ExecutionVariablesTraining;
import other.executionEnvironment.ReadParameters;
import other.executionEnvironment.RefreshStatistics;


/**
 * 
 * This class is the main class from which the execution of our program begins.
 * It reads the "ExecutionParameters.txt" and instantiates the appropriate objects
 * from the following classes:
 * 		-	ExecutionVariablesGeneral
 * 		-	ExecutionVariablesOtherRecommendationSystem
 * 		-	ExecutionVariablesTraining
 * 		-	ExecutionVariablesTesting
 * 		-	ExecutionVariablesSimilarityEstimation
 * 
 * These objects will help for the execution of our program, providing a structured way
 * for accessing the appropriate parameters when it's necessary, as well as keeping other
 * valuable information such as execution time etc.
 * 
 * Our testing helps more as visual verification for the values rather than the usual concept of Java 
 * Unit Tests. You can enable tests for the following drivers by removing the comments from the
 * lines in this class's run() method.
 * 		-	UserPipelineDriver
 * 		-	CategoryPipelineDriver
 * 		-	CategoriesCommonUsersDriver
 * 		-	UserInterestOnCategoryDriver
 * 		-	DMatrixDriver
 * 
 * Moreover some more testing results are exported to TXTs, mostly for a visual verification of the results
 * in the Training and Testing drivers.
 * 
 * @author John Koumarelas
 * @param args : args[0]: input path, args[1]: output path 		|	(both in the FS)
 *
 */
public class Main {

	/**
	 * From this method, the execution of the whole program is started.
	 * 
	 * @param args : args[0]: input path, args[1]: output path 		|	(both in the FS)
	 */
	public static void main(String[] args) {
		try 
		{
			LinkedList<ExecutionVariablesGeneral> llEvg = new LinkedList<ExecutionVariablesGeneral>();
			
			ReadParameters.run(args,llEvg);
			
			if(llEvg.size() > 0)
				run(llEvg);
			
			RefreshStatistics rs = new RefreshStatistics();
			rs.run(args[1]);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * As always we provide a run() method, which basically does all the job of the execution.
	 * This run() is the main method from which the whole execution is structured and executed.
	 * 
	 * The execution is structured with loops in such a way so that work that has been executed
	 * will not be executed again. Moreover this structure helps for other things like measuring
	 * the total execution time etc. See the appropriate classes for more details.
	 * 
	 * @param llEvg		:	Objects the assist the execution of this program. Provided by the
	 * ReadParameter's run() method, that reads the "ExecutionParameters.txt".
	 * 
	 * @throws Exception
	 */
	public static void run(LinkedList<ExecutionVariablesGeneral> llEvg) throws Exception{
		
		
		ExecutionVariablesCurrentGroup evcg = new ExecutionVariablesCurrentGroup();
		
		for(ExecutionVariablesGeneral evg : llEvg)
		{
			long start = System.nanoTime();
			
			evcg.evGeneral = evg;
			evcg.hasChangedGeneral = false;
			
			try{evcg.fillMissingValues_General();}catch(Exception e){}
			try{evcg.saveStatistics_General();}catch(Exception e){e.printStackTrace();}
			
			clearWholeOutputFolderBeforeStart(evcg);
			
			SplitDataset.run(evcg);

			CalculateTotalAverageDriver.run(evcg);
			
			UserPipelineDriver.run(evcg);
//			test.Test_UserPipelineDriver.run(evcg);
			
			UserInterestOnCategoryDriver.run(evcg);
//			test.Test_UserInterestDriver.run(evcg);
			
			CategoryPipelineDriver.run(evcg);
//			test.Test_CategoryPipelineDriver.run(evcg);
			
			CategoriesCommonUsersDriver.run(evcg);
//			test.Test_CategoriesCommonUsersDriver.run(evcg);
			
			DMatrixDriver.run(evcg);
//			test.Test_DMatrixDriver.run(evcg);
			
			evcg.evGeneral.medianOfDMatrix = other.FindDMatrixValue.run_median(evcg); // threshold for disGenres
			evcg.evGeneral.maxOfDMatrix = other.FindDMatrixValue.run_max(evcg);
			evcg.evGeneral.stdOfDMatrix = other.FindDMatrixValue.run_std(evcg);
			evcg.evGeneral.categoriesList = other.GetCategoriesTreeSet.run(evcg);

			for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : evcg.evGeneral.llOtherRecSys)
			{
				evcg.evOtherRecSys = evOtherRecSys;
				evcg.hasChangedOtherRecommendationSystem = false;
				
				try{evcg.fillMissingValues_OtherRecommendationSystem();}catch(Exception e){}
				try{evcg.saveStatistics_OtherRecommendationSystem();}catch(Exception e){e.printStackTrace();}
				
				other.NormalizeRatings.run(evcg);

				other.OtherRecommendationSystem.run(evcg);
				
				other.MergeOtherRecSysWithOur.run(evcg);
				
				for(ExecutionVariablesTraining evTraining : evOtherRecSys.llTraining)
				{
					evcg.evTraining = evTraining;
					evcg.hasChangedTraining = false;
					
					try{evcg.fillMissingValues_Training();}catch(Exception e){}
					try{evcg.saveStatistics_Training();}catch(Exception e){e.printStackTrace();}
					
					rate.TrainingModelDriver.run(evcg);
					
					for(ExecutionVariablesTesting evTesting : evTraining.llTesting)
					{
						evcg.evTesting = evTesting;
						evcg.hasChangedTesting = false;
						
						try{evcg.fillMissingValues_Testing();}catch(Exception e){}
						try{evcg.saveStatistics_Testing();}catch(Exception e){e.printStackTrace();}
						
						rate.TestingModelPhase1Driver.run(evcg);
						rate.TestingModelPhase2Driver.run(evcg);
						
//						other.ExportTestingModel.run(evcg);
			
						for(ExecutionVariablesSimilarityEstimation evSimEstim : evTesting.llSimEstim)
						{
							evcg.evSimEstim = evSimEstim;
							evcg.hasChangedSimilarityEstimation = false;
							
							rate.SimilarityEstimation.run(evcg);
							
							if(evcg.hasChangedSimilarityEstimation)
							{
								try{evcg.saveStatistics_SimilarityEstimation();}catch(Exception e){e.printStackTrace();}
							}
							else
								try{evcg.fillMissingValues_SimilarityEstimation();}catch(Exception e){}
						}
						
						if(evcg.hasChangedTesting)
						{
							try{evcg.fillMissingValues_Testing();}catch(Exception e){e.printStackTrace();}
							try{evcg.saveStatistics_Testing();}catch(Exception e){e.printStackTrace();}
						}
					}
					
					if(evcg.hasChangedTraining)
					{
						try{evcg.fillMissingValues_Training();}catch(Exception e){e.printStackTrace();}
						try{evcg.saveStatistics_Training();}catch(Exception e){e.printStackTrace();}
					}
					
				}
				
				if(evcg.hasChangedOtherRecommendationSystem)
				{
					try{evcg.fillMissingValues_OtherRecommendationSystem();}catch(Exception e){e.printStackTrace();}
					try{evcg.saveStatistics_OtherRecommendationSystem();}catch(Exception e){e.printStackTrace();}
				}
			}
			
			long stop = System.nanoTime();
			evcg.evGeneral.time_TotalExecutionTime = stop - start;
			
			if(evcg.hasChangedGeneral)
			{
				try{evcg.fillMissingValues_General();}catch(Exception e){e.printStackTrace();}
				try{evcg.saveStatistics_General();}catch(Exception e){e.printStackTrace();}
			}
		}
	}
	
	/**
	 * 
	 * If it is defined as true in the parameters, the WHOLE output folder will be deleted.
	 * Use with caution as this is mostly useful when running a single block of parameters
	 * (specific percentage of Training, Testing and percentage of users). For more info
	 * reference to "Readme_ExecutionParameters.txt".
	 * 
	 * @param evcg : Current execution's ExecutionVariablesCurrentGroup's object.
	 */
	public static void clearWholeOutputFolderBeforeStart(ExecutionVariablesCurrentGroup evcg){
		if(evcg.evGeneral.clearWholeOutputFolderBeforeStart)
		{
			try {
				Configuration conf = new Configuration();
				FileSystem fs = FileSystem.get(conf);
				fs.delete(new Path(evcg.evGeneral.outputPath), true); // true: recursively
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
