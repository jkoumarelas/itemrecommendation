package rate;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import net.robowiki.knn.implementations.Rednaxela3rdGenIteratedTreeKNNSearch;
import net.robowiki.knn.util.KNNEntry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;
import userPipeline.UserPreferenceWithCategories;

import commons.IntPairWritable;

/**
 * 
 * TrainingModelDriver.java
 * 
 * This class trains our system using the training dataset, as long as
 * secondary structures and the D Matrix, in order to be able to provide
 * estimations in the next phases of Testing.
 * 
 * We produce the KdTrees for each category that will be used in the next 
 * Testing phases to compute the K-Nearest Neighbors in an efficient way.
 * In order to be sure that the KdTrees will be able to fit on the RAM
 * we make sure that each KdTree has a maximum allowed entries inside it.
 * This if course may lead to multiple KdTrees per category, but this
 * is not a problem, because our system takes care of this.
 * To be able to fulfill this need, we broke the TestingModelPhase
 * into two phases.
 * 
 * @author John Koumarelas
 *
 */
public class TrainingModelDriver {
	private static String pathStrOutput = null;
	private static String pathStrOutputOriginal = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	private static double disGenresThreshold = -1.0;
	
	private static TreeSet<Integer> categoriesList = null;
	
	private static TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TrainingModel_entries = new TreeMap<Integer,TreeMap<Integer,Long>>();
	private static TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TrainingModel_sizeInBytes = new TreeMap<Integer,TreeMap<Integer,Long>>();
	
	public static void clearFields()
	{
		file_MapFile_TrainingModel_entries = new TreeMap<Integer,TreeMap<Integer,Long>>();
		file_MapFile_TrainingModel_sizeInBytes = new TreeMap<Integer,TreeMap<Integer,Long>>();
	}
	
	public static class Map extends Mapper<LongWritable, Text, IntWritable, TrainingObject> {
		private MapFile.Reader readerUserInterestOnCategory = null;
		private MapFile.Reader readerUserDisposition = null;
		
		private String pathStrOutput = null;
		private String pathStrOutputOriginal = null;
		
		private double disCategoryThreshold = -1.0;
		
		private TreeSet<Integer> categoriesList = null;
		
		private HashMap<Integer,Double> hmSumCategory = null;
		private HashMap<IntPairWritable,Double> hmDMatrix = null;
		
		private Configuration conf = null;
		private FileSystem fs = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,IntWritable,TrainingObject>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			disCategoryThreshold = Double.parseDouble(conf.get("disGenresThreshold"));
			
			String[] categoriesListStrArray = conf.get("categoriesList").split(",");
			categoriesList = new TreeSet<Integer>();
			for(int i = 0 ; i < categoriesListStrArray.length ; i++)
				categoriesList.add(Integer.parseInt(categoriesListStrArray[i]));
			
			{
				// User's Interest on a Category 
				Path pathInput = new Path(pathStrOutputOriginal + File.separator + "UserPipeline" + File.separator + "UserInterestOnCategory_Final");
		    	Path qualifiedDirNameInput = fs.makeQualified(pathInput);
		    	readerUserInterestOnCategory = new MapFile.Reader(fs, qualifiedDirNameInput.toString(),conf);
			}
			
			MapFile.Reader readerDMatrix = null;
			{
		    	Path pathInputDMatrix = new Path(pathStrOutputOriginal + File.separator + "DMatrix" + "_Final");
		    	Path qualifiedDirNameInputDMatrix = fs.makeQualified(pathInputDMatrix);
				readerDMatrix = new MapFile.Reader(fs, qualifiedDirNameInputDMatrix.toString(),conf);
			}
			
			{
		    	Path pathInputUserDisposition = new Path(pathStrOutputOriginal + File.separator + "UserPipeline" + File.separator + "UserDisposition" + "_Final");
		    	Path qualifiedDirNameUserDisposition = fs.makeQualified(pathInputUserDisposition);
				readerUserDisposition = new MapFile.Reader(fs, qualifiedDirNameUserDisposition.toString(),conf);
			}
			
			{
				hmSumCategory = new HashMap<Integer,Double>();
				
				IntPairWritable keyDMatrix = new IntPairWritable();
				DoubleWritable dpwValue = new DoubleWritable();
				
				Iterator<Integer> itCat1 = categoriesList.iterator();
				while(itCat1.hasNext())
				{
					Integer categoryID1 = itCat1.next();
					
					double sum = 0.0;
					
					Iterator<Integer> itCat2 = categoriesList.iterator();
					while(itCat2.hasNext())
					{
						Integer categoryID2 = itCat2.next();
						
						if(categoryID1 == categoryID2) continue;
						
						keyDMatrix.set(categoryID1, categoryID2);
						dpwValue.set(0.0);
						try {
							readerDMatrix.get(keyDMatrix, dpwValue);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}						
						sum += dpwValue.get();
					}
					hmSumCategory.put(categoryID1.intValue(), sum);
				}
			}
			
			{
				hmDMatrix = new HashMap<IntPairWritable,Double>();
				
				Iterator<Integer> itCat1 = categoriesList.iterator();
				while(itCat1.hasNext())
				{
					Integer categoryID1 = itCat1.next();
					
					Iterator<Integer> itCat2 = categoriesList.iterator();
					while(itCat2.hasNext())
					{
						Integer categoryID2 = itCat2.next();
						
						if(categoryID1 == categoryID2) continue;
						
						IntPairWritable keyDMatrix = new IntPairWritable();
						DoubleWritable dpwValue = new DoubleWritable();
						
						keyDMatrix.set(categoryID1, categoryID2);
						try {
							readerDMatrix.get(keyDMatrix, dpwValue);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						
						hmDMatrix.put(keyDMatrix, dpwValue.get());
					}
				}
			}
			readerDMatrix.close();
			
			super.setup(context);
		};
		
		
		protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,IntWritable,TrainingObject>.Context context) throws IOException ,InterruptedException {
			readerUserInterestOnCategory.close();
			readerUserDisposition.close();
			super.cleanup(context);
		};
		
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
		
			String strUserID = strUserPreferences[0];
			LongWritable lwUserID = new LongWritable( Long.parseLong(strUserID));
			
			LinkedList<UserPreferenceWithCategories> ll = new LinkedList<UserPreferenceWithCategories>();
			
			// We begin from 1, because in 0 index userID is stored.
			if(strUserPreferences.length > 1)
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];

				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
				long itemID = Long.parseLong(strItemInfo[0]);
				double rating = 0.0;
				
				if(strItemInfo.length == 3)
					rating = Double.parseDouble(strItemInfo[2]);
				else if(strItemInfo.length == 4)
					rating = Double.parseDouble(strItemInfo[2] + "-" + strItemInfo[3]);
				
				String[] strCategoriesIDArray = strItemInfo[1].split("_");
				
				int[] intArrayCategories = new int[strCategoriesIDArray.length];

				for(int j = 0 ; j < strCategoriesIDArray.length ; j++) {
					intArrayCategories[j] = Integer.parseInt(strCategoriesIDArray[j]);
				}
					
				UserPreferenceWithCategories upwc = new UserPreferenceWithCategories(itemID, intArrayCategories, rating);
				
				ll.add(upwc);
			}

	    	MapWritable mw = new MapWritable();
	    	
	    	try {
				readerUserInterestOnCategory.get(lwUserID, mw);
			} catch (IOException e) {
				e.printStackTrace();
			}	//	key: userID
    		
	    	double maxUserInterestOnCategory = -1.0;
    		int maxCategoryID = -1;
	    	
    		Set<Entry<Writable,Writable>> semw = mw.entrySet();
    		Iterator<Entry<Writable,Writable>> itEn = semw.iterator();
    		while(itEn.hasNext()){
    			Entry<Writable,Writable> en = itEn.next();
    			int categoryID = ((IntWritable)en.getKey()).get();
    			double categoryInterest = ((DoubleWritable)en.getValue()).get();
    			
    			if(categoryInterest > maxUserInterestOnCategory){
    				maxUserInterestOnCategory = categoryInterest;
    				maxCategoryID = categoryID;
    			}
    		}
    		
    		// D Matrix
    		IntPairWritable keyDMatrix = new IntPairWritable();
	 	    	
	    	Double valueDMatrix;
	    
	    	// User Disposition
    		LongWritable keyUserDisposition = new LongWritable(lwUserID.get());
    		DoubleWritable valueUserDisposition = new DoubleWritable();
	    	
			try {
				readerUserDisposition.get(keyUserDisposition, valueUserDisposition);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	
			double sumDMatrixMaxCategoryID = 0.0;

			sumDMatrixMaxCategoryID = hmSumCategory.get(new Integer(maxCategoryID));
	    	
	    	// Process...
			
			for(UserPreferenceWithCategories upwc : ll){
				double sum_dDimension = 0.0;
				double sum_CIDimension = 0.0;
				@SuppressWarnings("unused")
				int counter_dDimension = 0;
				int counter_CIDimension = 0;
				
				double dDimension = 0.0;
				double CIDimension = 0.0;
				
				int[] categoriesArray = upwc.categoriesArray;
								
				DoubleWritable dwMaxCategory = (DoubleWritable)mw.get(new IntWritable(maxCategoryID));
				
				for(int i = 0 ; i < categoriesArray.length ; i++){
					
					if( maxCategoryID != categoriesArray[i])
					{
						keyDMatrix.set(maxCategoryID, categoriesArray[i]);
						valueDMatrix = hmDMatrix.get(keyDMatrix);
					}
					else
						valueDMatrix = 0.0;
					
					sum_dDimension += valueDMatrix;
					counter_dDimension++;
					
					DoubleWritable dwTemp = (DoubleWritable)mw.get(new IntWritable(categoriesArray[i]));
					if(dwTemp != null)
					{
						sum_CIDimension += dwTemp.get();
						counter_CIDimension++;
					}
					
				}
								
				dDimension = sum_dDimension / sumDMatrixMaxCategoryID;
				
				if(counter_CIDimension == 0) counter_CIDimension = 1;
					CIDimension = sum_CIDimension / (counter_CIDimension * dwMaxCategory.get());
				//
				
				for(int i = 0 ; i < categoriesArray.length ; i++){
					
					if(maxCategoryID != categoriesArray[i])
					{
						keyDMatrix.set(maxCategoryID, categoriesArray[i]);
						valueDMatrix = hmDMatrix.get(keyDMatrix);
					}
					else
						valueDMatrix = 0.0;
					
					if(valueDMatrix < disCategoryThreshold)
						continue;
					
					TrainingObject to = new TrainingObject();
					
					to.userID = lwUserID.get();
					to.itemID = upwc.itemID;
					
					to.dDimension = dDimension;
					to.CIDimension = CIDimension;

					to.normalizedRating = upwc.rating;
					
					context.write(new IntWritable(categoriesArray[i]), to);
				}
			}
		}
	}

	public static class Reduce extends Reducer<IntWritable, TrainingObject, Text, Text> {
		private String pathStrOutput = null;
		
		private TreeSet<Integer> categoriesList = null;
		
		private final long NUMBER_OF_RECORDS_PER_FILE = 200000000L;
		private final long NUMBER_OF_RECORDS_PER_LINKED_LIST = 20000000L;

		private HashMap<Integer, Rednaxela3rdGenIteratedTreeKNNSearch> hmKDTrees = null;
		private HashMap<Integer,Long> hmOutCounter = null;
		private HashMap<Integer,Integer> hmOutIndex = null;

		private TaskID thisTaskID = null;
		
		private Configuration conf = null;
		private FileSystem fs = null;
				
		protected void setup(org.apache.hadoop.mapreduce.Reducer<IntWritable,TrainingObject,Text,Text>.Context context) throws IOException ,InterruptedException {			
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			String[] categoriesListStrArray = conf.get("categoriesList").split(",");
			categoriesList = new TreeSet<Integer>();
			for(int i = 0 ; i < categoriesListStrArray.length ; i++)
				categoriesList.add(Integer.parseInt(categoriesListStrArray[i]));
			
			hmKDTrees = new HashMap<Integer, Rednaxela3rdGenIteratedTreeKNNSearch>();
			hmOutCounter = new HashMap<Integer,Long>();
			hmOutIndex = new HashMap<Integer,Integer>();
			
			Iterator<Integer> it = categoriesList.iterator();
			while(it.hasNext())
			{
				Integer categoryID = it.next();
	 	    	
	 	    	hmKDTrees.put(categoryID, new Rednaxela3rdGenIteratedTreeKNNSearch(2));
	 	    	hmOutCounter.put(categoryID, 0L);
	 	    	hmOutIndex.put(categoryID, 0);
			}
			
			thisTaskID = context.getTaskAttemptID().getTaskID();
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<IntWritable,TrainingObject,Text,Text>.Context context) throws IOException ,InterruptedException {
			Iterator<Integer> it = categoriesList.iterator();
			while(it.hasNext())
			{
				Integer categoryID = it.next();
				
				Long numOfEntries = hmOutCounter.get(categoryID);
				
				if(numOfEntries > 0)
				{
					saveDataAndIncreaseIndex(categoryID, true);
				}
			}
			super.cleanup(context);
		};

		private void saveDataAndIncreaseIndex(int categoryID, boolean lastTime)
		{
			int catFileIndex = hmOutIndex.get(categoryID);

			Path path = new Path(pathStrOutput + File.separator + "TrainingModel" + File.separator + categoryID + "_" + catFileIndex + "_" + hmOutCounter.get(categoryID) + "_" + thisTaskID);
 	    	Path qualifiedDirName = fs.makeQualified(path);
 	    	
 	    	catFileIndex++;
    		hmOutIndex.put(categoryID, catFileIndex);
    		
 	    	ObjectOutputStream oos = null;
 	    	try {
				oos = new ObjectOutputStream(new BufferedOutputStream(fs.create(qualifiedDirName, true)));
			} catch (IOException e) {
				e.printStackTrace();
			}
     	    	
 	    	Rednaxela3rdGenIteratedTreeKNNSearch kdTree = hmKDTrees.get(categoryID);
 	    	
    		hmOutCounter.put(categoryID, 0L);
    		try {
				oos.writeObject(kdTree);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
    		
    		try {
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    		

    		if(lastTime == false)
    			hmKDTrees.put(categoryID, new Rednaxela3rdGenIteratedTreeKNNSearch(2));
		}
		
		private void reduceExecute(IntWritable key, LinkedList<TrainingObject> categoriesTrainingObjects, boolean lastTime)
		{
			Integer categoryID = key.get();
	    	
	    	Long categoryCounter = hmOutCounter.get(categoryID);
	    	
	    	Rednaxela3rdGenIteratedTreeKNNSearch kdTree = hmKDTrees.get(categoryID);
	    	
	    	Long tempCounterKeys = 0L;
	    	
	    	for(TrainingObject to : categoriesTrainingObjects)
	    	{
	    		tempCounterKeys++;
	    		
	    		double[] loc = new double[2];
	    		loc[0] = to.CIDimension;
	    		loc[1] = to.dDimension;
	    		KNNEntry e = new KNNEntry(String.valueOf(to.normalizedRating),loc);
	    		
	    		try
	    		{
	    			kdTree.addDataPoint(e);
	    		}
	    		catch(Exception exc)
	    		{
//	    			exc.printStackTrace();
	    		}
	    		categoryCounter++;
	    		
	    		if(categoryCounter == NUMBER_OF_RECORDS_PER_FILE)
		    	{
	    			saveDataAndIncreaseIndex(categoryID, lastTime);
	    			categoryCounter = 0L;
		    	}
	    	}
	    	hmOutCounter.put(categoryID, categoryCounter);
		}
		
		public void reduce(IntWritable key, Iterable<TrainingObject> values, Context context) {			
			long valuesCounter = 0L;
			LinkedList<TrainingObject> ll = new LinkedList<TrainingObject>();
			LinkedList<TrainingObject> ll_temp = new LinkedList<TrainingObject>();
			
			//
			for(TrainingObject to : values)
				ll_temp.add(to.clone());
			
			Collections.sort(ll_temp);
			//
			
			for(TrainingObject to : ll_temp)
			{
				ll.add(to.clone());
				valuesCounter++;
				if(valuesCounter == NUMBER_OF_RECORDS_PER_LINKED_LIST)
				{
					valuesCounter = 0L;
					reduceExecute(key, ll, false);
					ll.clear();
				}
			}
			if(ll.size() > 0)
				reduceExecute(key, ll, true);
			
		} 
	}

	/////////////////////////////////
	/////////////////////////////////

	public static class PathFilterTrainingKDTreesCategoryFilePart implements PathFilter
	{
		public int categoryID = -1;
		
		PathFilterTrainingKDTreesCategoryFilePart(int categoryID)
		{
			this.categoryID = categoryID;
		}
		
		public boolean accept(Path path) {
			if(path.getName().toString().startsWith(String.valueOf(categoryID) + "_"))
				return true;
			else
				return false;
		}
	}
	
	public static void renameKdTreeFileParts(Configuration conf, FileSystem fs) throws IOException
	{
		Iterator<Integer> itCat = categoriesList.iterator();
		while(itCat.hasNext())
		{
			Integer categoryID = itCat.next();
			PathFilterTrainingKDTreesCategoryFilePart pathFilter = new PathFilterTrainingKDTreesCategoryFilePart(categoryID);
			
			FileStatus[] fStatTotalParts = fs.listStatus(new Path(pathStrOutput + File.separator + "TrainingModel"), pathFilter);

			for(int i = 0 ; i < fStatTotalParts.length ; i++)
			{
				Path oldPath = fStatTotalParts[i].getPath();
				Path oldQualifiedPath = fs.makeQualified(oldPath);
				
				String oldFlName = oldQualifiedPath.getName();
				String[] flNameTokens = oldFlName.split("_");
				
				String newFlName = "";
				newFlName += flNameTokens[0] + "_" + i;
				
				getFileKeysAndSize(oldQualifiedPath, oldFlName, newFlName);
				
				Path newPath = new Path(pathStrOutput + File.separator + "TrainingModel" + File.separator + newFlName);
				Path newQualifiedPath = fs.makeQualified(newPath);
				
				fs.rename(oldQualifiedPath, newQualifiedPath);
			}
		}
	}
	
	public static void renameKdTreeFiles(Configuration conf, FileSystem fs)
	{
		try {
			renameKdTreeFileParts(conf,fs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void getFileKeysAndSize(Path oldQualifiedPath, String oldFileName, String newFileName) throws IOException
	{
		String[] oldFileNameTokens = oldFileName.split("_");
		String[] newFileNameTokens = newFileName.split("_");
		
		Integer categoryID = Integer.parseInt(oldFileNameTokens[0]);
		Integer newFilePart = Integer.parseInt(newFileNameTokens[1]);
		
		long keys = Long.parseLong(oldFileNameTokens[1]);
		
		FileStatus fileStatus = fs.getFileStatus(oldQualifiedPath);
		Long size = fileStatus.getLen();
		
		//
		TreeMap<Integer,Long> tmPart_entries = file_MapFile_TrainingModel_entries.get(categoryID);
		
		if(tmPart_entries == null)
			tmPart_entries = new TreeMap<Integer,Long>();
		
		tmPart_entries.put(newFilePart, keys);
		
		file_MapFile_TrainingModel_entries.put(categoryID, tmPart_entries);
		//
		
		//
		TreeMap<Integer,Long> tmPart_size = file_MapFile_TrainingModel_sizeInBytes.get(categoryID);
		
		if(tmPart_size == null)
			tmPart_size = new TreeMap<Integer,Long>();
		
		tmPart_size.put(newFilePart, size);
		
		file_MapFile_TrainingModel_sizeInBytes.put(categoryID,tmPart_size);
		//
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evTraining.file_MapFile_TrainingModel_entries = file_MapFile_TrainingModel_entries;
		evcg.evTraining.file_MapFile_TrainingModel_sizeInBytes = file_MapFile_TrainingModel_sizeInBytes;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputOriginal", pathStrOutputOriginal);
		conf.set("disGenresThreshold", String.valueOf(disGenresThreshold));
		
		String categoriesListStr = "";
		Iterator<Integer> itCat = categoriesList.iterator();
		while(itCat.hasNext())
		{
			Integer categoryID = itCat.next();
			categoriesListStr += categoryID;
			if(itCat.hasNext())
				categoriesListStr += ",";
		}
		
		conf.set("categoriesList", categoriesListStr);
	}
	
	public static boolean run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Training Model Driver - Start");
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathTraining() + File.separator +"RatePrediction";
		pathStrOutputOriginal = evcg.getOutputPathGeneral();
		
		TrainingModelDriver.disGenresThreshold = evcg.evTraining.disGenresThreshold.getValue(evcg.evGeneral);
		TrainingModelDriver.categoriesList = evcg.evGeneral.categoriesList;
		
		setParameters(conf);
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TrainingModel")))
			{
				System.out.println("*Training Model Driver*");
				System.out.println("Folder: " + pathStrOutput + File.separator + "TrainingModel");
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TrainingModel")))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return false;
			}
		}
		
		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput + File.separator + "TrainingModel"), true); // true: recursively
		fs.delete(new Path(pathStrOutput + File.separator + "TrainingModel_Text"), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getOutputPathGeneral() + File.separator + "newUserPreferences" + File.separator + "userPreferences_training_OurSystemInput" + File.separator + "data";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathTraining() + File.separator + "DummyDirectory" + File.separator + dt.getTime();

		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
		
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "CategoryPipeline");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(TrainingModelDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(TrainingObject.class); 
		
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);
		
		renameKdTreeFiles(conf,fs);
		updateInfo(evcg);
		
		long stop = System.nanoTime();
		evcg.evTraining.time_TrainingModelDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Training Model Driver - Stop");
		evcg.hasChangedTraining = true;
		
		conf.clear();
		fs.close();
		
		System.runFinalization();
		System.gc();
		
		return true;
	}
	
}
