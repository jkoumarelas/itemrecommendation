package rate;

import org.apache.hadoop.io.ArrayWritable;

/**
 * 
 * This class is used in TestingModelPhase1Driver.
 * 
 * @author John Koumarelas
 *
 */
public class UserPreferenceWithCategoriesAndEstimationsArray extends ArrayWritable{
	public UserPreferenceWithCategoriesAndEstimationsArray(){
		super(UserPreferenceWithCategoriesAndEstimations.class);
	}	
}