package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * 
 * Objects from this class are exported from the TestingModelPhase1Driver.
 * 
 * @author John Koumarelas
 *
 */
public class TestingObjectPhase1Output implements Writable{
	public double normalizedRating;
	public double otherRecSysEstimationRating;
	
	public double CIDimension;
	public double dDimension;

	public int kNNPointsLength;
	public double[] kNNPointsDistance;
	public double[] kNNPointsValueNormalizedRating;
	
	public TestingObjectPhase1Output() {
		
		normalizedRating = -1.0;
		otherRecSysEstimationRating = -1.0;
		
		CIDimension = -1.0;
		dDimension = -1.0;
		
		kNNPointsLength = -1;
		kNNPointsDistance = null;
		kNNPointsValueNormalizedRating = null;
	}
	
	public TestingObjectPhase1Output(double normalizedRating, double otherRecSysEstimationRating, double CIDimension, double dDimension, int kNNPointsLength, double[] kNNPointsDistance, double[] kNNPointsValueNormalizedRating) {

		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		
		this.CIDimension = CIDimension;
		this.dDimension = dDimension;
		
		this.kNNPointsLength = kNNPointsLength;
		this.kNNPointsDistance = kNNPointsDistance;
		this.kNNPointsValueNormalizedRating = kNNPointsValueNormalizedRating;
	}
	
	public TestingObjectPhase1Output(TestingObjectPhase1Output tObjPh1Obj) {
		this.normalizedRating = tObjPh1Obj.normalizedRating;
		this.otherRecSysEstimationRating = tObjPh1Obj.otherRecSysEstimationRating;
		
		this.CIDimension = tObjPh1Obj.CIDimension;
		this.dDimension = tObjPh1Obj.dDimension;
		
		this.kNNPointsLength = tObjPh1Obj.kNNPointsLength;
		this.kNNPointsDistance = tObjPh1Obj.kNNPointsDistance;
		this.kNNPointsValueNormalizedRating = tObjPh1Obj.kNNPointsValueNormalizedRating;
	}
	
	public void setParameters(double normalizedRating, double otherRecSysEstimationRating, double CIDimension, double dDimension, int kNNPointsLength, double[] kNNPointsDistance, double[] kNNPointsValueNormalizedRating)
	{
		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		
		this.CIDimension = CIDimension;
		this.dDimension = dDimension;
		
		this.kNNPointsLength = kNNPointsLength;
		this.kNNPointsDistance = kNNPointsDistance;
		this.kNNPointsValueNormalizedRating = kNNPointsValueNormalizedRating;
	}
	
	public void setParameters(double normalizedRating, double otherRecSysEstimationRating)
	{
		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
	}
	
	public void setParameters(TestingObjectPhase1Output tObjPh1Obj)
	{
		this.normalizedRating = tObjPh1Obj.normalizedRating;
		this.otherRecSysEstimationRating = tObjPh1Obj.otherRecSysEstimationRating;
		
		this.CIDimension = tObjPh1Obj.CIDimension;
		this.dDimension = tObjPh1Obj.dDimension;
		
		this.kNNPointsLength = tObjPh1Obj.kNNPointsLength;
		this.kNNPointsDistance = tObjPh1Obj.kNNPointsDistance;
		this.kNNPointsValueNormalizedRating = tObjPh1Obj.kNNPointsValueNormalizedRating;
	}
	
	@Override
	public String toString(){
		String strReturn = "";
		
		strReturn += " normalizedRating: " + String.valueOf(normalizedRating);
		strReturn += " otherRecSysEstimationRating: " + String.valueOf(otherRecSysEstimationRating);
		
		strReturn += " CIDimension: " + String.valueOf(CIDimension);
		strReturn += " dDimension: " + String.valueOf(dDimension);
		
		strReturn += " kNNPointsLength: " + String.valueOf(kNNPointsLength);
		
		if(kNNPointsLength > 0)
		for(int i = 0 ; i < kNNPointsLength ; i++)
		{
			strReturn += "\r\n\t kNNPointsDistance: " + String.valueOf(kNNPointsDistance[i]) + " kNNPointsValueNormalizedRating: " + String.valueOf(kNNPointsValueNormalizedRating[i]);
		}
		
		return strReturn;
	}

	@Override
	public void readFields(DataInput in) throws IOException {

		normalizedRating = in.readDouble();
		otherRecSysEstimationRating = in.readDouble();
		
		CIDimension = in.readDouble();
		dDimension = in.readDouble();
		
		kNNPointsLength = in.readInt();
		
		if(kNNPointsLength > 0)
		{
			kNNPointsDistance = new double[kNNPointsLength];
			kNNPointsValueNormalizedRating = new double[kNNPointsLength];
			
			for(int i = 0 ; i < kNNPointsLength ; i++)
			{
				kNNPointsDistance[i] = in.readDouble();
				kNNPointsValueNormalizedRating[i] = in.readDouble();
			}
		}
		else
		{
			kNNPointsDistance = null;
			kNNPointsValueNormalizedRating = null;
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {

		out.writeDouble(normalizedRating);
		out.writeDouble(otherRecSysEstimationRating);
		
		out.writeDouble(CIDimension);
		out.writeDouble(dDimension);
		
		out.writeInt(kNNPointsLength);
		
		if(kNNPointsLength > 0)
		{
			for(int i = 0 ; i < kNNPointsLength ; i++)
			{
				out.writeDouble(kNNPointsDistance[i]);
				out.writeDouble(kNNPointsValueNormalizedRating[i]);
			}
		}
	}
}
