package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * 
 * Objects from this class are exported from the TestingModelPhase2Driver.
 * 
 * @author John Koumarelas
 *
 */
public class TestingObjectPhase2Output implements Writable{
	public int categoriesArrayLength;
	public int[] categoriesArray;
	
	public double normalizedRating;
	public double otherRecSysEstimationRating;
	public double ourEstimationRating;
	
	public TestingObjectPhase2Output() {
		categoriesArrayLength = -1;
		categoriesArray = null;
		
		normalizedRating = -1;
		otherRecSysEstimationRating = -1;
		ourEstimationRating = -1;
	}
	
	public TestingObjectPhase2Output(int[] categoriesArray, double normalizedRating, double otherRecSysEstimationRating, double ourEstimationRating) {
		this.categoriesArrayLength = categoriesArray.length;
		this.categoriesArray = categoriesArray;
		
		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		this.ourEstimationRating = ourEstimationRating;
	}
	
	public TestingObjectPhase2Output(TestingObjectPhase2Output to) {
		this.categoriesArrayLength = to.categoriesArrayLength;
		this.categoriesArray = to.categoriesArray.clone();
		
		this.normalizedRating = to.normalizedRating;
		this.otherRecSysEstimationRating = to.otherRecSysEstimationRating;
		this.ourEstimationRating = to.ourEstimationRating;
	}
	
	public void setParameters(int[] categoriesArray, double normalizedRating, double otherRecSysEstimationRating, double ourEstimationRating)
	{
		this.categoriesArrayLength = categoriesArray.length;
		this.categoriesArray = categoriesArray;
		
		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		this.ourEstimationRating = ourEstimationRating;
	}
	
	public void setParameters(TestingObjectPhase2Output to)
	{
		this.categoriesArrayLength = to.categoriesArrayLength;
		this.categoriesArray = to.categoriesArray.clone();
		
		this.normalizedRating = to.normalizedRating;
		this.otherRecSysEstimationRating = to.otherRecSysEstimationRating;
		this.ourEstimationRating = to.ourEstimationRating;
	}
	
	@Override
	public String toString(){
		String strReturn = "";
		
		strReturn += "categories: ";
		for(int i = 0 ; i < categoriesArrayLength ; i++)
		{
			strReturn += String.valueOf(categoriesArray[i]);
			if((i+1) != categoriesArrayLength)
				strReturn += ",";
		}

		strReturn += " normalizedRating:" + String.valueOf(normalizedRating);
		strReturn += " otherRecSysEstimationRating: " + String.valueOf(otherRecSysEstimationRating);
		strReturn += " ourEstimationRating: " + String.valueOf(ourEstimationRating);
		
		return strReturn;
	}

	@Override
	public void readFields(DataInput in) throws IOException {

		categoriesArrayLength = in.readInt();
		categoriesArray = new int[categoriesArrayLength];
		for(int i = 0 ; i < categoriesArrayLength ; i++)
			categoriesArray[i] = in.readInt();
		
		normalizedRating = in.readDouble();
		otherRecSysEstimationRating = in.readDouble();
		ourEstimationRating = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {

		out.writeInt(categoriesArrayLength);
		for(int i = 0 ; i < categoriesArrayLength ; i++)
			out.writeInt(categoriesArray[i]);
		
		out.writeDouble(normalizedRating);
		out.writeDouble(otherRecSysEstimationRating);
		out.writeDouble(ourEstimationRating);
	}
}
