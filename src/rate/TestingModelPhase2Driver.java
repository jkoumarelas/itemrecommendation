package rate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.LongPairWritable;

/**
 * 
 * TestingModelPhase2Driver.java
 * 
 * This class is the second of the two Testing classes.
 * 
 * In this phase we calculate the K most Nearest Neighbors for each category
 * which will then give us an average value which we will combine with the other
 * average values in order to get the final estimation of our system.
 * From the previous Phase, a MapFile for each category file part (each category
 * file part maps to a KdTree) will provide us with the K-Nearest Neighbors of that
 * KdTree.
 * 
 * @author John Koumarelas
 *
 */
public class TestingModelPhase2Driver {
	
	private static Configuration conf = null;
	private static FileSystem fs = null;

	private static String pathStrOutput = null;
	
	private static TreeSet<Integer> categoriesList = null;
	
	private static long file_MapFile_TestingModel2_keys = 0L;
	private static long file_MapFile_TestingModel2_index_sizeInBytes = 0L;
	
	public static class Map extends Mapper<LongWritable, Text, LongWritable, TestingObjectPhase2> {
		
		private HashMap<Integer,MapFile.Reader[]> hmCatReader = null;
		
		private HashMap<Integer, BufferedWriter> hmBufWriter = null;
		
		private long itemsOurRecSystemEstimatedValue = 0L;
		private long itemsOtherRecSystemEstimatedValue = 0L;
		
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		
		private TreeSet<Integer> categoriesList = null;
		
		private TaskID thisTaskID = null;
		
		public class PathFilterCategory implements PathFilter
		{
			public int categoryID = -1;
			
			PathFilterCategory(int categoryID)
			{
				this.categoryID = categoryID;
			}
			
			public boolean accept(Path path) {
				if(path.getName().toString().startsWith(String.valueOf(categoryID) + "_"))
					return true;
				else
					return false;
			}
		}
		
		protected void setup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,LongWritable,TestingObjectPhase2>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			
			String[] categoriesListStrArray = conf.get("categoriesList").split(",");
			categoriesList = new TreeSet<Integer>();
			for(int i = 0 ; i < categoriesListStrArray.length ; i++)
				categoriesList.add(Integer.parseInt(categoriesListStrArray[i]));
			
			hmBufWriter = new HashMap<Integer, BufferedWriter>();
			
			hmCatReader = new HashMap<Integer,MapFile.Reader[]>();
			
			Path testingPathPhase1 = new Path(pathStrOutput + File.separator + "TestingModelPhase1");
			
			Iterator<Integer> itCat = categoriesList.iterator();
			while(itCat.hasNext())
			{
				Integer categoryID = itCat.next();

				FileStatus[] categoryMapFiles = null;
				try {
					categoryMapFiles = fs.listStatus(testingPathPhase1, new PathFilterCategory(categoryID));
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if(categoryMapFiles != null && categoryMapFiles.length > 0)
				{
					MapFile.Reader[] readers = new MapFile.Reader[categoryMapFiles.length];
					
					for(int i = 0 ; i < readers.length ; i++)
					{
				    	Path qualifiedDirNameInput = fs.makeQualified(categoryMapFiles[i].getPath());
				    	readers[i] = new MapFile.Reader(fs, qualifiedDirNameInput.toString(),conf);
					} 
					
					hmCatReader.put(categoryID, readers);
				}
				
			}
			thisTaskID = context.getTaskAttemptID().getTaskID();
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,LongWritable,TestingObjectPhase2>.Context context) throws IOException ,InterruptedException {
			Iterator<Integer> itCat = categoriesList.iterator();
			while(itCat.hasNext())
			{
				Integer categoryID = itCat.next();
				MapFile.Reader[] readers = hmCatReader.get(categoryID);
				
				if(readers != null && readers.length > 0)
				for(int i = 0; i< readers.length ; i++)
					readers[i].close();
			}
			
			super.cleanup(context);
		};
		
		// http://www.algolist.net/Algorithms/Sorting/Quicksort
		public static int quickSort_partition(Double[] arr, Double[] arr_2, int left, int right)
		{
		      int i = left, j = right;
		      Double tmp,tmp_2;
		      Double pivot = arr[(left + right) / 2];
		     
		      while (i <= j) {
		            while (arr[i] < pivot)
		                  i++;
		            while (arr[j] > pivot)
		                  j--;
		            if (i <= j) {
		            	  //
		                  tmp = arr[i];
		                  arr[i] = arr[j];
		                  arr[j] = tmp;
		                  //
		                  tmp_2 = arr_2[i];
		                  arr_2[i] = arr_2[j];
		                  arr_2[j] = tmp_2;
		                  //
		                  
		                  i++;
		                  j--;
		            }
		      };
		     
		      return i;
		}
		 
		public static void quickSortTwoArrays(Double arr[], Double arr_2[], int left, int right) {
		      int index = quickSort_partition(arr,arr_2, left, right);
		      if (left < index - 1)
		    	  quickSortTwoArrays(arr, arr_2, left, index - 1);
		      if (index < right)
		    	  quickSortTwoArrays(arr, arr_2, index, right);
		}
		
		public double calculateKNNAvg(long userID, long itemID, int[] categories)
		{
			LongPairWritable lpw = new LongPairWritable();
			lpw.set(userID, itemID);
			
			TestingObjectPhase1Output tObjPh1Out = new TestingObjectPhase1Output();
			
			double sum = 0;
			int counter = 0;
			
			for(int i = 0 ; i < categories.length ; i++)
			{
				MapFile.Reader[] readers = hmCatReader.get(categories[i]);
				
				double tempSumValues = 0.0;
				double tempSumDistances = 0.0;
				
				LinkedList<Double> llDistances = new LinkedList<Double>();
				LinkedList<Double> llValueNormalizedRating = new LinkedList<Double>();
				
				if(readers != null)
				for(int j = 0 ; j < readers.length ; j++)
				{
					try {
						if(readers[j].get(lpw, tObjPh1Out) == null)
							continue;
						
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					int kNNPointsLength = tObjPh1Out.kNNPointsLength;
					
					if(kNNPointsLength > 0)
					{
						for(int k = 0 ; k < kNNPointsLength; k++)
						{
							llDistances.add(tObjPh1Out.kNNPointsDistance[k]);
							llValueNormalizedRating.add(tObjPh1Out.kNNPointsValueNormalizedRating[k]);
						}
						
					}
				}
				
				if(llDistances.size() > 0)
				{
					Double[] distancesArray = llDistances.toArray(new Double[0]);
					Double[] valueNormalizedRatingArray = llValueNormalizedRating.toArray(new Double[0]);
					
					quickSortTwoArrays(distancesArray,valueNormalizedRatingArray,0,distancesArray.length - 1);
					
					for(int j = 0 ; j < llDistances.size() ; j++)
					{
						tempSumValues += valueNormalizedRatingArray[j];
						tempSumDistances += distancesArray[j];
					}
					
					sum += (tempSumValues / llDistances.size());
					counter++;
					
					//
					
					double meanValues = (tempSumValues / llDistances.size());
					double meanDistances = (tempSumDistances / llDistances.size());
					
					double sumStdValues = 0;
					double sumStdDistances = 0;
					
					for(int j = 0 ; j < llDistances.size() ; j++)
					{
						sumStdValues = Math.pow(valueNormalizedRatingArray[j] - meanValues, 2.0);
						sumStdDistances = Math.pow(distancesArray[j] - meanDistances, 2.0);
					}
					
					double stdValues = Math.sqrt(sumStdValues / llDistances.size());
					double stdDistances = Math.sqrt(sumStdDistances / llDistances.size());
					
					BufferedWriter out = hmBufWriter.get(categories[i]);
					
					try {
						out.write(userID + "\t" + itemID + "\t" + tObjPh1Out.dDimension + "\t" + tObjPh1Out.CIDimension + "\t" + stdDistances + "\t" + stdValues + "\t" + meanValues + "\r\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				
			}
			
			if(counter == 0)
			{
				return Double.NEGATIVE_INFINITY;
			}
			
			return sum/counter;
		}

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			LongWritable lwUserID = new LongWritable( Long.parseLong(strUserID));
			
			// We begin from 1, because in 0 index userID is stored.
			if(strUserPreferences.length > 1)
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];

				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		normalizedRating
				//	strItemInfo[3]:		estimationRating from other Recommendation System (e.g. SlopeOne)
				String[] strItemInfo = strUP.split("-");
				
				long itemID = Long.parseLong(strItemInfo[0]);
				
				double normalizedRating = -1; 
				
				double estimationRating = -1;
				
				if(strItemInfo.length == 4)
				{
					normalizedRating = Double.parseDouble(strItemInfo[2]);
					estimationRating = Double.parseDouble(strItemInfo[3]);
				}
				else if (strItemInfo.length > 4)	//exponential forms
				{
					if(strItemInfo.length == 5) // one exponential form
					{
						if(strItemInfo[2].endsWith("E")) // the first
						{
							normalizedRating = Double.parseDouble(strItemInfo[2] + "-" + strItemInfo[3]);
							estimationRating = Double.parseDouble(strItemInfo[4]);
						}
						else
						{
							normalizedRating = Double.parseDouble(strItemInfo[2]);
							estimationRating = Double.parseDouble(strItemInfo[3] + "-" + strItemInfo[4]);
						}
					}
					else if(strItemInfo.length == 6) // two exponential forms
					{
						normalizedRating = Double.parseDouble(strItemInfo[2] + "-" + strItemInfo[3]);
						estimationRating = Double.parseDouble(strItemInfo[4] + "-" + strItemInfo[5]);
					}
				}
				
				String[] strCategoriesIDArray = strItemInfo[1].split("_");
				
				int[] intArrayCategories = new int[strCategoriesIDArray.length];
				
				for(int j = 0 ; j < strCategoriesIDArray.length ; j++) {
					intArrayCategories[j] = Integer.parseInt(strCategoriesIDArray[j]);
				}

				double ourEstimationRating = calculateKNNAvg(lwUserID.get(),itemID,intArrayCategories);
				
				if(ourEstimationRating == Double.NEGATIVE_INFINITY)
				{
					ourEstimationRating = estimationRating;
					itemsOtherRecSystemEstimatedValue++;
				}
				else
					itemsOurRecSystemEstimatedValue++;
				
				TestingObjectPhase2 tObjPh2 = new TestingObjectPhase2();
				
				tObjPh2.setParameters(lwUserID.get(), itemID, intArrayCategories, normalizedRating, estimationRating, ourEstimationRating);
				
				context.write(lwUserID, tObjPh2);
			}
		
		}
	}
	
	public static class Reduce extends Reducer<LongWritable, TestingObjectPhase2, LongPairWritable, TestingObjectPhase1Output> {
		
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		
		private MapFile.Writer writer = null;
		
		private TaskID thisTaskID = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<LongWritable,TestingObjectPhase2,LongPairWritable,TestingObjectPhase1Output>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			
			thisTaskID = context.getTaskAttemptID().getTaskID();
			
			{
				Path path = new Path(pathStrOutput + File.separator + "TestingModelPhase2" + File.separator + thisTaskID);
				Path qualifiedDirName = fs.makeQualified(path);
				
				try {
					writer = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), LongPairWritable.class, TestingObjectPhase2Output.class);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<LongWritable,TestingObjectPhase2,LongPairWritable,TestingObjectPhase1Output>.Context context) throws IOException ,InterruptedException {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			super.cleanup(context);
		};
		
		public void reduce(LongWritable key, Iterable<TestingObjectPhase2> values, Context context) {
			
			TreeSet<TestingObjectPhase2> tsTestObjPh2 = new TreeSet<TestingObjectPhase2>();
			for(TestingObjectPhase2 testObjPh2 : values)
				tsTestObjPh2.add(testObjPh2.clone());
			
			Iterator<TestingObjectPhase2> itTestObjPh2 = tsTestObjPh2.iterator();
			while(itTestObjPh2.hasNext())
			{
				TestingObjectPhase2 tObjPh2 = itTestObjPh2.next();
				
				TestingObjectPhase2Output tObjPh2Out = new TestingObjectPhase2Output(tObjPh2.categoriesArray, tObjPh2.normalizedRating,tObjPh2.otherRecSysEstimationRating,tObjPh2.ourEstimationRating);
				LongPairWritable lpw = new LongPairWritable();
				lpw.set(key.get(), tObjPh2.itemID);
				try {
					writer.append(lpw, tObjPh2Out);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		} 
	}
	
	//
	//
	
	public static void mergeMapFilesGeneral(String pathStrInput){
		try{
			String pathStrOutput = pathStrInput + "_Final";
			Path pathOutput = new Path(pathStrOutput);
			Path pathQualifiedDirNameOutput = fs.makeQualified(pathOutput);
		    MapFile.Writer writer = null;
		    
			FileStatus[] fstatArrayMapFiles = fs.listStatus(new Path(pathStrInput));
		    
			//
		    LocalFileSystem lfs = LocalFileSystem.getLocal(conf);
		    
		    lfs.delete(new Path("localMapFiles"), true);
		    lfs.mkdirs(new Path("localMapFiles"));
		    
		    for(FileStatus fstat : fstatArrayMapFiles)
		    {
		    	Path pathHDFS = fstat.getPath();
		    	Path pathLocal = new Path( "localMapFiles" + File.separator + fstat.getPath().getName());
		    	fs.copyToLocalFile(pathHDFS, pathLocal);
		    }

		    FileStatus[] fstatMapFilesLocal = lfs.listStatus(new Path("localMapFiles"));
			//
			
		    writer = new MapFile.Writer(conf, fs, pathQualifiedDirNameOutput.toString(), LongPairWritable.class, TestingObjectPhase2Output.class);
		    
		    TreeMap<LongPairWritable, MapFile.Reader> tmValues = new TreeMap<LongPairWritable, MapFile.Reader>();
		    
		    // Insert first values
		    for(FileStatus fl : fstatMapFilesLocal)
		    {
		    	Path pathInput = fl.getPath();
		    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		    	MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		    	
		    	TestingObjectPhase2Output tObjPh2Out = new TestingObjectPhase2Output();
	    		LongPairWritable lpwKey = new LongPairWritable();
	    		lpwKey.set(Long.MIN_VALUE, Long.MIN_VALUE);
	    		lpwKey = (LongPairWritable) reader.getClosest(lpwKey, tObjPh2Out);	//	key: userID
		    	
		    	tmValues.put(lpwKey, reader);
		    }
		    
		    
		    while(true) {
		    	
		    	if(tmValues.size() == 0)
		    		break;
		    	
		    	Entry<LongPairWritable, MapFile.Reader> entry = tmValues.firstEntry();
		    	LongPairWritable lpwTmKey = entry.getKey();
		    	MapFile.Reader reader = entry.getValue();
		    	
		    	file_MapFile_TestingModel2_keys++;
		    	TestingObjectPhase2Output tObjPh2Out = new TestingObjectPhase2Output();
		    	reader.get(lpwTmKey, tObjPh2Out);	//	key: userID
		    	LongPairWritable lpwKey = new LongPairWritable();
		    	lpwKey.set(lpwTmKey.getFirst(), lpwTmKey.getSecond());
		    	writer.append(lpwKey, tObjPh2Out);
		    	boolean existsNext = reader.next(lpwKey, tObjPh2Out);
		    	tmValues.remove(lpwTmKey);
		    	if(!existsNext)
		    		reader.close();
		    	else
		    		tmValues.put(lpwKey, reader);
		    }
		    
		    writer.close();
		    
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void mergeMapFiles() throws IOException{
		mergeMapFilesGeneral(pathStrOutput + File.separator + "TestingModelPhase2");
	}
	
	private static void calculateFileSizes()
	{
		try{
			FileStatus indexFileStatus = fs.getFileStatus(new Path(pathStrOutput + File.separator + "TestingModelPhase2" + "_Final" + File.separator + "index"));
			file_MapFile_TestingModel2_index_sizeInBytes = indexFileStatus.getLen();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evTesting.file_MapFile_TestingModel2_keys = file_MapFile_TestingModel2_keys;
		evcg.evTesting.file_MapFile_TestingModel2_index_sizeInBytes = file_MapFile_TestingModel2_index_sizeInBytes;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		
		String categoriesListStr = "";
		Iterator<Integer> itCat = categoriesList.iterator();
		while(itCat.hasNext())
		{
			Integer categoryID = itCat.next();
			categoriesListStr += categoryID;
			if(itCat.hasNext())
				categoriesListStr += ",";
		}
		
		conf.set("categoriesList", categoriesListStr);
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Testing Model Phase2 Driver - Start");

		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathTesting() + File.separator +"RatePrediction";

		TestingModelPhase2Driver.categoriesList = evcg.evGeneral.categoriesList;
		
		setParameters(conf);
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TestingModelPhase2")))
			{
				System.out.println("*Testing Model Driver*");
				System.out.println("Folder: " + pathStrOutput + File.separator + "TestingModelPhase2");
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TestingModelPhase2_Final")))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}

		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput + File.separator + "TestingModelPhase2"), true); // true: recursively
		fs.delete(new Path(pathStrOutput + File.separator + "TestingModelPhase2_Final"), true); // true: recursively
		fs.delete(new Path(pathStrOutput + File.separator + "TestingModelPhase2_Text"), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_OurSystemInput" + File.separator + "data";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathTesting() + File.separator + "DummyDirectory" + File.separator + dt.getTime();

		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "TestModelPhase1Driver");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(TestingModelPhase2Driver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(TestingObjectPhase2.class); 
		
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Reducer
		job.setOutputKeyClass(LongPairWritable.class);
		job.setOutputValueClass(TestingObjectPhase1Output.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		job.waitForCompletion(true);
	
		mergeMapFiles();
		calculateFileSizes();
		updateInfo(evcg);
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "TestingModelPhase2"));
		
		long stop = System.nanoTime();
		evcg.evTesting.time_TestingModelPhase2Driver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Testing Model Phase2 Driver - Stop");
		evcg.hasChangedTesting = true;
		
		System.runFinalization();
		System.gc();
	}
	
}