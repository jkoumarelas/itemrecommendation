package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class UserAndCategoryPairWritable implements WritableComparable<UserAndCategoryPairWritable>{

	private long userID = 0;
	private int categoryID = 0;

	/**
	 * Set the left and right values.
	 */
	public void set(long userID, int categoryID) {
		this.userID = userID;
		this.categoryID = categoryID;
	}
	
	public int getCategoryID() {
		return categoryID;
	}
	public long getUserID() {
		return userID;
	}
	/**
	 * Read the two integers. 
	 * Encoded as: MIN_VALUE -> 0, 0 -> -MIN_VALUE, MAX_VALUE-> -1
	 */
	@Override
	public void readFields(DataInput in) throws IOException {
		userID = in.readLong() + Long.MIN_VALUE;
		categoryID = in.readInt() + Integer.MIN_VALUE;
	}
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(userID - Long.MIN_VALUE);
		out.writeInt(categoryID - Integer.MIN_VALUE);
	}
	@Override
	public int hashCode() {
		return (int)userID * 157 + categoryID;
	}
	
	@Override
	public boolean equals(Object right) {
		if (right instanceof UserAndCategoryPairWritable) {
			UserAndCategoryPairWritable r = (UserAndCategoryPairWritable) right;
			return r.userID == userID && r.categoryID == categoryID;
		} else {
			return false;
		}
	}
	/** A Comparator that compares serialized IntPair. */ 
	public static class Comparator extends WritableComparator {
		public Comparator() {
			super(UserAndCategoryPairWritable.class);
		}

		@Override
		public int compare(byte[] b1, int s1, int l1,
				byte[] b2, int s2, int l2) {
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}

	static {                                        // register this comparator
		WritableComparator.define(UserAndCategoryPairWritable.class, new Comparator());
	}

	@Override
	public int compareTo(UserAndCategoryPairWritable o) {
		if (userID != o.userID) {
			return userID < o.userID ? -1 : 1;
		} else if (categoryID != o.categoryID) {
			return categoryID < o.categoryID ? -1 : 1;
		} else {
			return 0;
		}
	}
	
}
