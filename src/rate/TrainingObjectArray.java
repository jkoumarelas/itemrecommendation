package rate;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Writable;

/**
 * 
 * Objects from this class are used in the TrainingModelDriver.
 * 
 * @author John Koumarelas
 *
 */
public class TrainingObjectArray extends ArrayWritable{
	public TrainingObjectArray(){
		super(TrainingObject.class);
	}
	
	public TrainingObjectArray clone(){
		TrainingObjectArray new_toa = new TrainingObjectArray();
		
		Writable[] wArray = this.get();
		TrainingObject[] toArray = new TrainingObject[wArray.length];
		for(int i = 0 ; i < wArray.length ; i++)
			toArray[i] = (TrainingObject)wArray[i];
		
		new_toa.set(toArray);
		return new_toa;
	}
	
	public String toString(){
		String str = "";
		
		Writable[] wArray = this.get();
		
		for(Writable w : wArray)
			str += w.toString() + "--";
		
		return str;
	}
}