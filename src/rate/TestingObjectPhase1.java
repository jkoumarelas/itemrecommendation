package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 
 * Objects from this class are used in the TestingModelPhase1Driver.
 * 
 * @author John Koumarelas
 *
 */
public class TestingObjectPhase1 implements WritableComparable<TestingObjectPhase1>{
	
	public long userID;
	public long itemID;

	public double normalizedRating;
	public double otherRecSysEstimationRating;
	
	public double dDimension;
	public double CIDimension;
	
	public TestingObjectPhase1() {
		userID = -1;
		itemID = -1;
		
		normalizedRating = -1;
		otherRecSysEstimationRating = -1;
		
		dDimension = -1;
		CIDimension = -1;
	}
	
	public TestingObjectPhase1 clone()
	{
		TestingObjectPhase1 objToReturn = new TestingObjectPhase1();
		
		objToReturn.setParameters(this);
		
		return objToReturn;
	}
	
	public TestingObjectPhase1(long userID, long itemID ,double normalizedRating, double otherRecSysEstimationRating, double dDimension, double CIDimension) {
		this.userID = userID;
		this.itemID = itemID;

		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		
		this.dDimension = dDimension;
		this.CIDimension = CIDimension;
	}
	
	public TestingObjectPhase1(TestingObjectPhase1 to) {
		this.userID = to.userID;
		this.itemID = to.itemID;

		this.normalizedRating = to.normalizedRating;
		this.otherRecSysEstimationRating = to.otherRecSysEstimationRating;
		
		this.dDimension = to.dDimension;
		this.CIDimension = to.CIDimension;
	}
	
	public void setParameters(long userID, long itemID ,double normalizedRating, double otherRecSysEstimationRating, double dDimension, double CIDimension)
	{
		this.userID = userID;
		this.itemID = itemID;

		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		
		this.dDimension = dDimension;
		this.CIDimension = CIDimension;
	}
	
	public void setParameters(TestingObjectPhase1 to)
	{
		this.userID = to.userID;
		this.itemID = to.itemID;

		this.normalizedRating = to.normalizedRating;
		this.otherRecSysEstimationRating = to.otherRecSysEstimationRating;
		
		this.dDimension = to.dDimension;
		this.CIDimension = to.CIDimension;
	}
	
	@Override
	public String toString(){
		String strReturn = "";
		
		strReturn += "userID: " + String.valueOf(userID);
		strReturn += " itemID: " + String.valueOf(itemID);

		strReturn += " normalizedRating:" + String.valueOf(normalizedRating);
		strReturn += " otherRecSysEstimationRating: " + String.valueOf(otherRecSysEstimationRating);
		
		strReturn += " dDimension:" + String.valueOf(dDimension) + " CIDimension:" + String.valueOf(CIDimension);
		
		return strReturn;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		userID = in.readLong();
		itemID = in.readLong();

		normalizedRating = in.readDouble();
		otherRecSysEstimationRating = in.readDouble();
		
		dDimension = in.readDouble();
		CIDimension = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(userID);
		out.writeLong(itemID);

		out.writeDouble(normalizedRating);
		out.writeDouble(otherRecSysEstimationRating);
		
		out.writeDouble(dDimension);
		out.writeDouble(CIDimension);
	}

	public int compareTo(TestingObjectPhase1 to) {
		if( to == this) return 0;
		if (userID != to.userID) {
			return userID < to.userID ? -1 : 1;
		} else if (itemID != to.itemID) {
			return itemID < to.itemID ? -1 : 1;
		} else {
			return 0;
		}
	}
}
