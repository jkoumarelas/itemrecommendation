package rate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.LongPairWritable;

/**
 * 
 * SimilarityEstimation.java
 * 
 * This class calculates the similarity between the user rating
 * and our estimation, with the metrics RMSE (Root Mean Square Error)
 * and DCG (Discounted Cumulative Gain).
 * 
 * @author John Koumarelas
 *
 */
public class SimilarityEstimation {
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	private static MapFile.Reader readerUserInterest = null;
	
	private static double b_Fmeasure = 1.0;
	
	private static double Fmeasure(double a, double b){
		return ((1+Math.pow(b_Fmeasure,2.0)) * a * b ) / (Math.pow(b_Fmeasure,2.0)*a+b);
	}
	
	public class ItemRatingSortByRating implements Comparator<ItemRating>{

	    public int compare(ItemRating ir1, ItemRating ir2) {
	    	if( ir1 == ir2 ) return 0;
	    	
	    	if(ir1.rating == ir2.rating ) return 0;
	    	else
	    		return (ir1.rating < ir2.rating ? -1 : 1);	//	ascending
	    }
	}
	
	public class ItemRatingSortByOrderingNumber implements Comparator<ItemRating>{

	    public int compare(ItemRating ir1, ItemRating ir2) {
	    	if( ir1 == ir2 ) return 0;
	    	
	    	if(ir1.orderingNumber == ir2.orderingNumber ) return 0;
	    	else
	    		return (ir1.orderingNumber < ir2.orderingNumber ? -1 : 1);	//	asceding
	    }
	}
	
	public class ItemRating {
		public long itemID;
		public int[] categoriesArray;
		public double rating;
		
		public int orderingNumber;
		
		ItemRating(long itemID, int[] categoriesArray, double rating)
		{
			this.itemID = itemID;
			this.categoriesArray = categoriesArray;
			this.rating = rating;
		}
		
		ItemRating(long itemID, int[] categoriesArray, double rating, int orderingNumber)
		{
			this.itemID = itemID;
			this.categoriesArray = categoriesArray;
			this.rating = rating;
			this.orderingNumber = orderingNumber;
		}
		
		public String toString()
		{
			String strReturn = "";
			
			strReturn += "itemID: " + itemID + " rating: " + rating + " orderingNumber: " + orderingNumber;
			
			strReturn += " categories: ";
			for(int i = 0 ; i < categoriesArray.length ; i++)
			{
				strReturn += String.valueOf(categoriesArray[i]);
				if((i+1) != categoriesArray.length)
					strReturn += ",";
			}
			
			return strReturn;
		}
		
		public int compareTo(ItemRating ir) {
	    	if( this == ir ) return 0;
	    	
	    	if(this.itemID == ir.itemID ) return 0;
	    	else
	    		return (this.itemID < ir.itemID ? -1 : 1);	//	ascending
	    }
	}
	
	public void applyUserInterestCriteriaDCG(LinkedList<ItemRating> ratings, LinkedList<ItemRating> tempRatings, HashMap<Integer, Double> hmCatInterest)
	{
		TreeMap<Double, LinkedList<ItemRating>> tm = new TreeMap<Double, LinkedList<ItemRating>>();
		
		for(ItemRating ir : tempRatings)
		{
			int[] categoriesArray = ir.categoriesArray;
			
			double sumUserInterest = 0.0;
			for(int i = 0 ; i < categoriesArray.length ; i++)
			{
				int categoryID = categoriesArray[i];
				Double ui =  hmCatInterest.get(categoryID);
				if(ui != null)
					sumUserInterest += ui.doubleValue();
			}
			
			LinkedList<ItemRating> tempLL = tm.get(sumUserInterest);
			
			if(tempLL == null)
				tempLL = new LinkedList<ItemRating>();
			
			tempLL.add(ir);
			
			tm.put(sumUserInterest, tempLL);
		}
		
		int lowerOrderingNumber = -1;
		int counter = 0;
		Iterator<Entry< Double, LinkedList<ItemRating>>> itEntry = tm.entrySet().iterator();
		while(itEntry.hasNext())
		{
			Entry<Double, LinkedList<ItemRating>> entry = itEntry.next();
			LinkedList<ItemRating> tempLL = entry.getValue();
			
			if(lowerOrderingNumber == -1)	//	first group of sumUserInterest. Ordering number from this objects will not be changed.
			{
				lowerOrderingNumber = tempLL.getFirst().orderingNumber;
				
				for(ItemRating ir2 : ratings)
				{
					if(ir2.orderingNumber > lowerOrderingNumber)
						ir2.orderingNumber = ir2.orderingNumber + (tm.size() - 1);
				}
				
				continue;
			}
			
			counter++;
			
			for(ItemRating ir : tempLL)
				ir.orderingNumber = ir.orderingNumber + counter;
		}
		
		
		
	}
	
	public void applyFilterForEqualRatings(ExecutionVariablesCurrentGroup evcg, LinkedList<ItemRating> ratings, long lUserID) throws IOException
	{
		HashMap<Integer, Double> hmCatInterest = new HashMap<Integer, Double>();
		
		{
			LongWritable key = new LongWritable(lUserID);
			MapWritable mw = new MapWritable();
	    	SimilarityEstimation.readerUserInterest.get(key, mw);	//	key: userID
	    	
	    	Iterator<Entry<Writable, Writable>> itMwCatInt =  mw.entrySet().iterator();
	    	while(itMwCatInt.hasNext())
	    	{
	    		Entry<Writable, Writable> entry = itMwCatInt.next();
	    		
	    		hmCatInterest.put(((IntWritable)entry.getKey()).get(), ((DoubleWritable)entry.getValue()).get());
	    	}
		}
    	
    	LinkedList<ItemRating> tempRatings = new LinkedList<ItemRating>();
    	
    	ItemRating previousIR = null;
    	Iterator<ItemRating> itIR = ratings.iterator();
    	while(itIR.hasNext())
    	{
    		ItemRating ir = itIR.next();
    		
    		if(previousIR == null)
    		{
    			previousIR = ir;
    			continue;
    		}
    		
    		if(ir.rating == previousIR.rating)
    		{
    			if(!tempRatings.contains(previousIR))
    				tempRatings.add(previousIR);
    			
    			tempRatings.add(ir);
    		}
    		else
    		{
    			if(tempRatings.size() > 0)
    			{
    				applyUserInterestCriteriaDCG(ratings, tempRatings, hmCatInterest);
    				tempRatings.clear();
    			}
    		}
    		previousIR = ir;
    	}
    	
    	if(tempRatings.size() > 0)
		{
			applyUserInterestCriteriaDCG(ratings, tempRatings, hmCatInterest);
			tempRatings.clear();
		}
	}
	
	public double calculateDiscountedCumulativeGain(LinkedList<ItemRating> rates, int topK){
		Iterator<ItemRating> itRate = rates.descendingIterator();
		
		int rel1 = itRate.next().orderingNumber;
		
		double sumDCG = 0.0;
		long i = 1;
		while(itRate.hasNext())
		{
			if(i == topK) 
				break;
			
			int reli = itRate.next().orderingNumber;
			double logi = Math.log10(++i) / Math.log10(2.0);
			
			sumDCG +=  reli / logi ;
		}
		
		return (rel1 + sumDCG);
	}
	
	
	public void calculateSumRootMeanSquareError(LinkedList<ItemRating> normalizedRatings, LinkedList<ItemRating> otherRatings, int topK, Double[] sumRMSE_Param, Long[] counterRMSE_Param){
		double sumRMSE = 0.0;
		long counterRMSE = 0;
		
		Iterator<ItemRating> itOthRat = otherRatings.descendingIterator();
		
		if(topK > otherRatings.size()){
			
			while(itOthRat.hasNext()){
				ItemRating otherRating = itOthRat.next();
				long itemIDOtherRating = otherRating.itemID;
				double itemValueOtherRating = otherRating.rating;
				
				Iterator<ItemRating> itNormRat = normalizedRatings.descendingIterator();
				while(itNormRat.hasNext()){
					ItemRating normalizedRating = itNormRat.next();
					long itemIDNormalizedRating = normalizedRating.itemID;
					double itemValueNormalizedRating = normalizedRating.rating;

					if(itemIDOtherRating == itemIDNormalizedRating){
						sumRMSE += Math.pow(itemValueOtherRating - itemValueNormalizedRating, 2.0);
						counterRMSE++;
						break;
					}
				}
			}
		}
		else {
			int counterTopK = 0;
			
			while(itOthRat.hasNext() && (counterTopK++) < topK){
				ItemRating otherRating = itOthRat.next();
				long itemIDOtherRating = otherRating.itemID;
				double itemValueOtherRating = otherRating.rating;
				
				Iterator<ItemRating> itActRat = normalizedRatings.descendingIterator();
				while(itActRat.hasNext()){
					ItemRating actualRating = itActRat.next();
					long itemIDActualRating = actualRating.itemID;
					double itemValueActualRating = actualRating.rating;
					
					if(itemIDOtherRating == itemIDActualRating){
						sumRMSE += Math.pow(itemValueOtherRating - itemValueActualRating, 2.0);
						counterRMSE++;
						break;
					}
				}
			}
		}
		
		sumRMSE_Param[0] = sumRMSE;
		counterRMSE_Param[0] = counterRMSE;
	}

	public void estimateSimilarities(ExecutionVariablesCurrentGroup evcg) throws IOException{
		//
		LocalFileSystem lfs = LocalFileSystem.getLocal(conf);
	    
	    lfs.delete(new Path("localMapFiles"), true);
	    lfs.mkdirs(new Path("localMapFiles"));
	    
	    {
	    	Path pathHDFS = new Path(evcg.getOutputPathTesting() + File.separator +"RatePrediction" + File.separator + "TestingModelPhase2" + "_Final");
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "TestingModelPhase2" + "_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
	    }
		//
    	Path qualifiedDirNameInput = lfs.makeQualified(new Path( "localMapFiles" + File.separator + "TestingModelPhase2" + "_Final"));
		MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		
		LongPairWritable key = new LongPairWritable();
		key.set(Long.MIN_VALUE, Long.MIN_VALUE);
		TestingObjectPhase2Output tObjPh2Out = new TestingObjectPhase2Output();

		{
			String pathStr_mf_ui = evcg.getOutputPathGeneral() + File.separator +"UserPipeline" + File.separator + "UserInterestOnCategory" + "_Final";
			
			Path pathHDFS = new Path(pathStr_mf_ui);
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "UserInterestOnCategory" + "_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
			
			Path qualifiedDirNameInput_mf_ui = lfs.makeQualified(pathLocal);
	    	MapFile.Reader readerUserInterest = new MapFile.Reader(lfs, qualifiedDirNameInput_mf_ui.toString(),conf);
	    	SimilarityEstimation.readerUserInterest = readerUserInterest;
		}
		
		// normalizedRateToItemID: normalized rate by the user. (Actual User's Rate * User's Disposition)
		LinkedList<ItemRating> normalizedRatings = new LinkedList<ItemRating>();
		LinkedList<ItemRating> otherRecSysEstimationRatings = new LinkedList<ItemRating>();
		LinkedList<ItemRating> ourEstimationRatings = new LinkedList<ItemRating>();
		
		// RMSE
		double rmse_otherRecSys_sum = 0.0;
		double rmse_ourRecSys_sum = 0.0;
		
		long rmse_otherRecSys_counter = 0;
		long rmse_ourRecSys_counter = 0;
		
		Double[] sumRMSE_Param = new Double[1];
		Long[] counterRMSE_Param = new Long[1];
		
		// DCG
		double ndcg_otherRecSys_sum = 0.0;
		double ndcg_ourRecSys_sum = 0.0;
		
		int ndcg_otherRecSys_counter = 0;
		int ndcg_ourRecSys_counter = 0;
		
		key = (LongPairWritable) reader.getClosest(key, tObjPh2Out);
		
		// First User, first Item
			normalizedRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,tObjPh2Out.normalizedRating));
			otherRecSysEstimationRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,tObjPh2Out.otherRecSysEstimationRating));
			
			double FmeasureEstimation = Fmeasure(tObjPh2Out.otherRecSysEstimationRating,tObjPh2Out.ourEstimationRating);
			if(Double.isNaN(FmeasureEstimation))	//	In arithmetic problems, just use the other System's value.
				FmeasureEstimation = tObjPh2Out.otherRecSysEstimationRating;
			
			ourEstimationRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,FmeasureEstimation));
		
		long previousUser = key.getFirst();
		
		while(reader.next(key, tObjPh2Out)){
			
			if(key != null && key.getFirst() == previousUser)
			{
				normalizedRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,tObjPh2Out.normalizedRating));
				otherRecSysEstimationRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,tObjPh2Out.otherRecSysEstimationRating));
				
				FmeasureEstimation = Fmeasure(tObjPh2Out.otherRecSysEstimationRating,tObjPh2Out.ourEstimationRating);
				if(Double.isNaN(FmeasureEstimation))	//	In arithmetic problems, just use the other System's value.
				{
					FmeasureEstimation = tObjPh2Out.otherRecSysEstimationRating;
				}
				
				ourEstimationRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,FmeasureEstimation));
			}
			else
			{
				ItemRatingSortByRating comparatorSortByRating = new ItemRatingSortByRating();
				Collections.sort(normalizedRatings, comparatorSortByRating);
				Collections.sort(otherRecSysEstimationRatings, comparatorSortByRating);
				Collections.sort(ourEstimationRatings, comparatorSortByRating);
				
				// RMSE
				calculateSumRootMeanSquareError(normalizedRatings, otherRecSysEstimationRatings, evcg.evSimEstim.topK_SimilarityEstimation, sumRMSE_Param, counterRMSE_Param);
				rmse_otherRecSys_sum += sumRMSE_Param[0];
				rmse_otherRecSys_counter += counterRMSE_Param[0];
				
				calculateSumRootMeanSquareError(normalizedRatings, ourEstimationRatings, evcg.evSimEstim.topK_SimilarityEstimation, sumRMSE_Param, counterRMSE_Param);
				rmse_ourRecSys_sum += sumRMSE_Param[0];
				rmse_ourRecSys_counter += counterRMSE_Param[0];
				
				// DCG
				Iterator<ItemRating> itNormRate = normalizedRatings.iterator();
				int counter = 0;
				
				ItemRating normRatePrevious = null;
				
				while(itNormRate.hasNext())
				{
					ItemRating normRate = itNormRate.next();
					
					if(normRatePrevious != null)
					{
						if(normRate.rating == normRatePrevious.rating)
							normRate.orderingNumber = normRatePrevious.orderingNumber;
						else
							normRate.orderingNumber = ++counter;
					}
					else
						normRate.orderingNumber = ++counter;
					
					normRatePrevious = normRate;
				}
				
				//
				applyFilterForEqualRatings(evcg, normalizedRatings, key.getFirst());
				ItemRatingSortByOrderingNumber comparatorSortByOrderingNumber = new ItemRatingSortByOrderingNumber();
				Collections.sort(normalizedRatings, comparatorSortByOrderingNumber);
				//
				
				Iterator<ItemRating> itOtherRecSysRate = otherRecSysEstimationRatings.iterator();
				while(itOtherRecSysRate.hasNext())
				{
					ItemRating otherRecSysRate = itOtherRecSysRate.next();
					
					itNormRate = normalizedRatings.iterator();
					while(itNormRate.hasNext())
					{
						ItemRating normRate = itNormRate.next();
						
						if(otherRecSysRate.itemID == normRate.itemID)
							otherRecSysRate.orderingNumber = normRate.orderingNumber;
					}
				}
				
				Iterator<ItemRating> itOurEstRate = ourEstimationRatings.iterator();
				while(itOurEstRate.hasNext())
				{
					ItemRating ourEstRate= itOurEstRate.next();
					
					itNormRate = normalizedRatings.iterator();
					while(itNormRate.hasNext())
					{
						ItemRating normRate = itNormRate.next();
						
						if(ourEstRate.itemID == normRate.itemID)
							ourEstRate.orderingNumber = normRate.orderingNumber;
					}
				}
				

				double idcg_normalizedRatings = calculateDiscountedCumulativeGain(normalizedRatings, evcg.evSimEstim.topK_SimilarityEstimation);

				double dcg_otherRecSys = calculateDiscountedCumulativeGain(otherRecSysEstimationRatings, evcg.evSimEstim.topK_SimilarityEstimation);

				double dcg_ourRecSys = calculateDiscountedCumulativeGain(ourEstimationRatings, evcg.evSimEstim.topK_SimilarityEstimation);
				
				double ndcg_otherRecSys = dcg_otherRecSys / idcg_normalizedRatings;
				double ndcg_ourRecSys = dcg_ourRecSys / idcg_normalizedRatings;
				
				if((Double.isNaN(ndcg_otherRecSys) == false) && (Double.isNaN(ndcg_ourRecSys) == false))
				{
					ndcg_otherRecSys_sum += ndcg_otherRecSys;
					ndcg_otherRecSys_counter++;
					
					ndcg_ourRecSys_sum += ndcg_ourRecSys;
					ndcg_ourRecSys_counter++;
				}
				
				normalizedRatings.clear();
				otherRecSysEstimationRatings.clear();
				ourEstimationRatings.clear();
				
				if(key != null)
				{
					previousUser = key.getFirst();
					
					normalizedRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,tObjPh2Out.normalizedRating));
//					actualRateToItemID.put(tObjPh2Out.normalizedRating, key.getSecond());
					otherRecSysEstimationRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,tObjPh2Out.otherRecSysEstimationRating));
//					otherRecSysEstimationRateToItemID.put(tObjPh2Out.otherRecSysEstimationRating, key.getSecond());
					
					FmeasureEstimation = Fmeasure(tObjPh2Out.otherRecSysEstimationRating,tObjPh2Out.ourEstimationRating);
					if(Double.isNaN(FmeasureEstimation))	//	In arithmetic problems, just use the other System's value.
						FmeasureEstimation = tObjPh2Out.otherRecSysEstimationRating;
					
					ourEstimationRatings.add(new ItemRating(key.getSecond(),tObjPh2Out.categoriesArray,FmeasureEstimation));
//					ourEstimationRateToItemID.put(FmeasureEstimation, key.getSecond());
				}
			}
		}
		
		double rmse_otherRecSys = Math.sqrt(rmse_otherRecSys_sum / rmse_otherRecSys_counter);
		double rmse_ourRecSys = Math.sqrt(rmse_ourRecSys_sum / rmse_ourRecSys_counter);
		
		System.out.println("RMSE - Other Recommendation System: " + rmse_otherRecSys);
		System.out.println("RMSE - Our Recommendation System: " + rmse_ourRecSys);
		System.out.println("RMSE - Improvement (%): "  +  (100 - ((100 * rmse_ourRecSys) / rmse_otherRecSys)));
		
		double ndcg_otherRecSys = (ndcg_otherRecSys_sum / ndcg_otherRecSys_counter);
		double ndcg_ourRecSys = (ndcg_ourRecSys_sum / ndcg_ourRecSys_counter);
		
		System.out.println("nDCG - Other Recommendation System: " + ndcg_otherRecSys);
		System.out.println("nDCG - Our Recommendation System: " + ndcg_ourRecSys);
		System.out.println("nDCG - Improvement (%): " + (((100 * ndcg_ourRecSys) / ndcg_otherRecSys) - 100));
		
		if(fs.exists(new Path(evcg.getOutputPathSimilarityEstimation()))){
			fs.delete(new Path(evcg.getOutputPathSimilarityEstimation()), true); // true: recursively
			fs.mkdirs(new Path(evcg.getOutputPathSimilarityEstimation()));
		}
		else{
			fs.mkdirs(new Path(evcg.getOutputPathSimilarityEstimation()));
		}

		Path pathQualifiedOutputTesting = fs.makeQualified(new Path(evcg.getOutputPathSimilarityEstimation() + File.separator + "RMSE_DCG"));
		FSDataOutputStream	fsOutStreamTesting = fs.create(pathQualifiedOutputTesting);
		BufferedWriter out_RMSE_DCG = new BufferedWriter(new OutputStreamWriter(fsOutStreamTesting));
		
		out_RMSE_DCG.write("RMSE - Other Recommendation System: " + rmse_otherRecSys + "\r\n");
		out_RMSE_DCG.write("RMSE - Our Recommendation System: " + rmse_ourRecSys + "\r\n");
		out_RMSE_DCG.write("RMSE - Improvement (%): "  +  (100 - ((100 * rmse_ourRecSys) / rmse_otherRecSys)) + "\r\n");
		
		out_RMSE_DCG.write("nDCG - Other Recommendation System: " + ndcg_otherRecSys + "\r\n");
		out_RMSE_DCG.write("nDCG - Our Recommendation System: " + ndcg_ourRecSys + "\r\n");
		out_RMSE_DCG.write("nDCG - Improvement (%): " + (((100 * ndcg_ourRecSys) / ndcg_otherRecSys) - 100) + "\r\n");
		
		out_RMSE_DCG.close();
		reader.close();
		readerUserInterest.close();
		
		evcg.evSimEstim.RMSE_OtherRecommendationSystem = rmse_otherRecSys;
		evcg.evSimEstim.RMSE_OurRecommendationSystem = rmse_ourRecSys;
		evcg.evSimEstim.nDCG_OtherRecommendationSystem = ndcg_otherRecSys;
		evcg.evSimEstim.nDCG_OurRecommendationSystem = ndcg_ourRecSys;
		
		lfs.delete(new Path("localMapFiles"), true);
	}
	
	public static void run(ExecutionVariablesCurrentGroup evcg) throws IOException{
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Similarity Estimation - Start");
	
		long start = System.nanoTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		b_Fmeasure = evcg.evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation;
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(evcg.getOutputPathSimilarityEstimation())))
			{
				System.out.println("*Similarity Estimation*");
				System.out.println("Folder: " + evcg.getOutputPathSimilarityEstimation());
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				else
				{
					in.close();
					fs.delete(new Path(evcg.getOutputPathSimilarityEstimation()), true); // true: recursively
					fs.mkdirs(new Path(evcg.getOutputPathSimilarityEstimation()));
				}
			}
			else
				fs.mkdirs(new Path(evcg.getOutputPathSimilarityEstimation()));
		}
		else
		{
			if(fs.exists(new Path(evcg.getOutputPathSimilarityEstimation())))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
			else
				fs.mkdirs(new Path(evcg.getOutputPathSimilarityEstimation()));
		}
		SimilarityEstimation se = new SimilarityEstimation();
		try {
			se.estimateSimilarities(evcg);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		catch(Exception e){
			e.printStackTrace();
		}
		
		long stop = System.nanoTime();
		evcg.evSimEstim.time_SimilarityEstimation = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Similarity Estimation - Stop");
		evcg.hasChangedSimilarityEstimation = true;
		
		conf.clear();
		fs.close();
		
		System.runFinalization();
		System.gc();
	}
	
}
