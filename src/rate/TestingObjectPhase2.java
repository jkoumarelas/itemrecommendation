package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 
 * Objects from this class are used in the TestingModelPhase2Driver.
 * 
 * @author John Koumarelas
 *
 */
public class TestingObjectPhase2 implements WritableComparable<TestingObjectPhase2>{
//	public long userID;
	public long itemID;
	
	public int categoriesArrayLength;
	public int[] categoriesArray;

	public double normalizedRating;
	public double otherRecSysEstimationRating;
	public double ourEstimationRating;
	
	public TestingObjectPhase2() {
		itemID = -1;
		
		categoriesArrayLength = -1;
		categoriesArray = null;
		
		normalizedRating = -1;
		otherRecSysEstimationRating = -1;
		ourEstimationRating = -1;
	}
	
	public TestingObjectPhase2(long itemID, int[] categoriesArray,double normalizedRating, double otherRecSysEstimationRating, double ourEstimationRating) {
		this.itemID = itemID;
		
		this.categoriesArrayLength = categoriesArray.length;
		this.categoriesArray = categoriesArray;

		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		this.ourEstimationRating = ourEstimationRating;
	}
	
	public TestingObjectPhase2 clone()
	{
		TestingObjectPhase2 objectToReturn = new TestingObjectPhase2();
		
		objectToReturn.setParameters(this);
		
		return objectToReturn;
	}
	
	public TestingObjectPhase2(TestingObjectPhase2 to) {
		this.itemID = to.itemID;
		
		this.categoriesArrayLength = to.categoriesArrayLength;
		this.categoriesArray = to.categoriesArray.clone();

		this.normalizedRating = to.normalizedRating;
		this.otherRecSysEstimationRating = to.otherRecSysEstimationRating;
		this.ourEstimationRating = to.ourEstimationRating;
	}
	
	public void setParameters(long userID, long itemID, int[] categoriesArray ,double normalizedRating, double otherRecSysEstimationRating, double ourEstimationRating)
	{
		this.itemID = itemID;
		
		this.categoriesArrayLength = categoriesArray.length;
		this.categoriesArray = categoriesArray;

		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = otherRecSysEstimationRating;
		this.ourEstimationRating = ourEstimationRating;
	}
	
	public void setParameters(TestingObjectPhase2 to)
	{
		this.itemID = to.itemID;
		
		this.categoriesArrayLength = to.categoriesArrayLength;
		this.categoriesArray = to.categoriesArray.clone();

		this.normalizedRating = to.normalizedRating;
		this.otherRecSysEstimationRating = to.otherRecSysEstimationRating;
		this.ourEstimationRating = to.ourEstimationRating;
	}
	
	@Override
	public String toString(){
		String strReturn = "";
		
		strReturn += "itemID: " + String.valueOf(itemID);
		
		strReturn += " categories: ";
		for(int i = 0 ; i < categoriesArrayLength ; i++)
		{
			strReturn += String.valueOf(categoriesArray[i]);
			if((i+1) != categoriesArrayLength)
				strReturn += ",";
		}

		strReturn += " normalizedRating:" + String.valueOf(normalizedRating);
		strReturn += " otherRecSysEstimationRating: " + String.valueOf(otherRecSysEstimationRating);
		strReturn += " ourEstimationRating: " + String.valueOf(ourEstimationRating);
		
		return strReturn;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		itemID = in.readLong();
		
		categoriesArrayLength = in.readInt();
		categoriesArray = new int[categoriesArrayLength];
		for(int i = 0 ; i < categoriesArrayLength ; i++)
			categoriesArray[i] = in.readInt();

		normalizedRating = in.readDouble();
		otherRecSysEstimationRating = in.readDouble();
		ourEstimationRating = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(itemID);
		
		out.writeInt(categoriesArrayLength);
		for(int i = 0 ; i < categoriesArrayLength ; i++)
			out.writeInt(categoriesArray[i]);

		out.writeDouble(normalizedRating);
		out.writeDouble(otherRecSysEstimationRating);
		out.writeDouble(ourEstimationRating);
	}

	public int compareTo(TestingObjectPhase2 to) {
		if( to == this) return 0;

		if (itemID != to.itemID) {
			return (itemID < to.itemID ? -1 : 1);
		} else {
			return 0;
		}
	}
}
