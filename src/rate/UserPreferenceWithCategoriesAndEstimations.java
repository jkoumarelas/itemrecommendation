package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 
 * This class is used in TestingModelPhase1Driver.
 * 
 * @author John Koumarelas
 *
 */
public class UserPreferenceWithCategoriesAndEstimations implements WritableComparable<UserPreferenceWithCategoriesAndEstimations>{
	public long itemID;
	public int categoriesArrayLength;
	public int[] categoriesArray;
	public double normalizedRating;
	public double otherRecSysEstimationRating;
	public double ourEstimationRating;
	
	public UserPreferenceWithCategoriesAndEstimations() {
		itemID = -1;
		categoriesArrayLength = -1;
		categoriesArray = null;
		normalizedRating = -1;
		otherRecSysEstimationRating = -1;
		ourEstimationRating = -1;
	}
	
	public UserPreferenceWithCategoriesAndEstimations(long itemID, int[] categoriesArray ,double normalizedRating, double estimationRating, double ourEstimationRating) {
		this.itemID = itemID;
		this.categoriesArrayLength = categoriesArray.length;
		this.categoriesArray = categoriesArray;
		this.normalizedRating = normalizedRating;
		this.otherRecSysEstimationRating = estimationRating;
		this.ourEstimationRating = ourEstimationRating;
	}
	
	public UserPreferenceWithCategoriesAndEstimations(UserPreferenceWithCategoriesAndEstimations up) {
		this.itemID = up.itemID;
		this.categoriesArrayLength = up.categoriesArrayLength;
		this.categoriesArray = up.categoriesArray;
		this.normalizedRating = up.normalizedRating;
		this.otherRecSysEstimationRating = up.otherRecSysEstimationRating;
		this.ourEstimationRating = up.ourEstimationRating;
	}
	
	@Override
	public int compareTo(UserPreferenceWithCategoriesAndEstimations o) {
		if( o == this) return 0;
		if( itemID == o.itemID) return 0;
		else
			return ( itemID < o.itemID ? -1 : 1);
	}
	
	@Override
	public int hashCode() {
		return (int)itemID;
	}
	
	@Override
	public String toString(){
		String strReturn = "itemID: " + String.valueOf(itemID)+ " categoriesArrayLength: " + categoriesArrayLength + " categories: ";
		for(int i = 0 ; i < categoriesArray.length ; i++){
			if((i+1)!=categoriesArray.length)
				strReturn += categoriesArray[i] + ", ";
			else
				strReturn += categoriesArray[i];
		}
		strReturn += " normalizedRating:" + String.valueOf(normalizedRating);
		strReturn += " otherRecSysEstimationRating: " + String.valueOf(otherRecSysEstimationRating);
		strReturn += " ourEstimationRating: " + String.valueOf(ourEstimationRating);
		return strReturn;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		itemID = in.readLong();
		
		categoriesArrayLength = in.readInt();
		
		categoriesArray = new int[categoriesArrayLength];

		for(int i = 0 ; i < categoriesArrayLength ; i++){
			categoriesArray[i]  = in.readInt();
		}

		normalizedRating = in.readDouble();
		otherRecSysEstimationRating = in.readDouble();
		ourEstimationRating = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(itemID);

		categoriesArrayLength = categoriesArray.length;
		out.writeInt(categoriesArrayLength);
		
		for(int i : categoriesArray ){
			out.writeInt(i);
		}

		out.writeDouble(normalizedRating);
		out.writeDouble(otherRecSysEstimationRating);
		out.writeDouble(ourEstimationRating);
	}
}
