package rate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * 
 * Objects from this class are used in the TrainingModelDriver.
 * 
 * @author John Koumarelas
 *
 */
public class TrainingObject implements WritableComparable<TrainingObject>{

	public long userID;
	public long itemID;
	
	public double normalizedRating;
	public double dDimension;
	public double CIDimension;
	
	public TrainingObject() {
		userID = -1L;
		itemID = -1L;
		
		normalizedRating = -1;
		dDimension = -1;
		CIDimension = -1;
	}
	
	public TrainingObject clone()
	{
		TrainingObject objectToReturn = new TrainingObject();
		
		objectToReturn.userID = this.userID;
		objectToReturn.itemID = this.itemID;
		objectToReturn.normalizedRating = this.normalizedRating;
		objectToReturn.dDimension = this.dDimension;
		objectToReturn.CIDimension = this.CIDimension;
		
		return objectToReturn;
	}
	
	public TrainingObject(double normRating,double dDimension, double GIDimension, long userID, long itemID) {
		this.userID = userID;
		this.itemID = itemID;
		
		this.normalizedRating = normRating;
		this.dDimension = dDimension;
		this.CIDimension = GIDimension;
	}
	
	public TrainingObject(TrainingObject to) {
		this.userID = to.userID;
		this.itemID = to.itemID;
		
		this.normalizedRating = to.normalizedRating;
		this.dDimension = to.dDimension;
		this.CIDimension = to.CIDimension;
	}
	
	@Override
	public int hashCode() {
		return (int)userID * 157 + (int)itemID;
	}
	
	@Override
	public String toString(){
		return "userID: " + userID + "itemID: " + itemID + "normalizedRating: " + String.valueOf(normalizedRating) +" dDimension:" + String.valueOf(dDimension) + " CIDimension:" + String.valueOf(CIDimension);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		userID = in.readLong();
		itemID = in.readLong();
		
		normalizedRating = in.readDouble();
		dDimension = in.readDouble();
		CIDimension = in.readDouble();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(userID);
		out.writeLong(itemID);
		
		out.writeDouble(normalizedRating);
		out.writeDouble(dDimension);
		out.writeDouble(CIDimension);
	}

	@Override
	public int compareTo(TrainingObject to) {
		if (userID != to.userID) {
			return userID < to.userID ? -1 : 1;
		} else if (itemID != to.itemID) {
			return itemID < to.itemID ? -1 : 1;
		} else {
			return 0;
		}
	}
}
