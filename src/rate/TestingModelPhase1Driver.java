package rate;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import net.robowiki.knn.implementations.Rednaxela3rdGenIteratedTreeKNNSearch;
import net.robowiki.knn.util.KNNPoint;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.IntPairWritable;
import commons.LongPairWritable;

/**
 * 
 * TestingModelPhase1Driver.java
 * 
 * This class is the first of the two Testing classes.
 * 
 * In this phase we calculate for each testing item, the K-Nearest Neighbors 
 * and then save them so that in the next phase, we calculate the K-Nearest 
 * Neighbors from all the category file parts (each category file part maps 
 * to a KdTree file). So in the end we will have the K most Nearest Neighbors 
 * for each category of the testing item.
 * 
 * @author John Koumarelas
 *
 */
public class TestingModelPhase1Driver {
	
	private static Configuration conf = null;
	private static FileSystem fs = null;

	private static String pathStrOutput = null;
	private static String pathStrOutputGeneral = null;
	private static String pathStrOutputTraining = null;

	private static double disGenresThreshold = -1.0;
	
	private static TreeSet<Integer> categoriesList = null;
	
	private static int k_of_kNN = -1;
	
	private static TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TestingModel1_keys = new TreeMap<Integer,TreeMap<Integer,Long>>();
	private static TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TestingModel1_index_sizeInBytes = new TreeMap<Integer,TreeMap<Integer,Long>>();
	
	public static void clearFields()
	{
		file_MapFile_TestingModel1_keys.clear();
		file_MapFile_TestingModel1_index_sizeInBytes.clear();
	}
	
	public static void addFileDataKeys(TreeMap<Integer,TreeMap<Integer,Long>> tmTestingModel1Keys)
	{
		
		Iterator<Entry<Integer,TreeMap<Integer,Long>>> itTmTestingModel1Keys = tmTestingModel1Keys.entrySet().iterator();
		while(itTmTestingModel1Keys.hasNext())
		{
			Entry<Integer,TreeMap<Integer,Long>> entryTsTestingModel1Keys = itTmTestingModel1Keys.next();
			
			TreeMap<Integer,Long> tmFilePartAndKeysCopy = new TreeMap<Integer,Long>();
			
			Iterator<Entry<Integer,Long>> itFilePartsKeys = entryTsTestingModel1Keys.getValue().entrySet().iterator();
			while(itFilePartsKeys.hasNext())
			{
				Entry<Integer,Long> entryFilePartsKeys = itFilePartsKeys.next();
				
				tmFilePartAndKeysCopy.put(entryFilePartsKeys.getKey(), entryFilePartsKeys.getValue());
			}
			
			file_MapFile_TestingModel1_keys.put(entryTsTestingModel1Keys.getKey(), tmFilePartAndKeysCopy);
		}
		
	}
	
	public static class Map extends Mapper<LongWritable, Text, Text, TestingObjectPhase1> {

		private String pathStrOutputGeneral = null;
		private String pathStrOutputTraining = null;

		private double disGenresThreshold = -1.0;
		
		private TreeSet<Integer> categoriesList = null;
		
		private HashMap<Integer,FileStatus[]> hmFst = null;

		private MapFile.Reader readerUserInterestOnCategory = null;
		private HashMap<IntPairWritable,Double> hmDMatrix = null;
		
		private Configuration conf;
		private FileSystem fs;
		
		public class PathFilterTrainingKDTrees implements PathFilter
		{
			public int categoryID = -1;
			
			PathFilterTrainingKDTrees(int categoryID)
			{
				this.categoryID = categoryID;
			}
			
			public boolean accept(Path path) {
				if(path.getName().toString().startsWith(String.valueOf(categoryID) + "_"))
					return true;
				else
					return false;
			}
		}
		
		
		
		protected void setup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,Text,TestingObjectPhase1>.Context context) throws IOException ,InterruptedException {
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);

			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputGeneral = conf.get("pathStrOutputGeneral");
			pathStrOutputTraining = conf.get("pathStrOutputTraining");
			
			disGenresThreshold = Double.parseDouble(conf.get("disGenresThreshold"));
			k_of_kNN = Integer.parseInt(conf.get("k_of_kNN"));
			
			String[] categoriesListStrArray = conf.get("categoriesList").split(",");
			categoriesList = new TreeSet<Integer>();
			for(int i = 0 ; i < categoriesListStrArray.length ; i++)
				categoriesList.add(Integer.parseInt(categoriesListStrArray[i]));
			
			{
				Path pathInput = new Path(pathStrOutputGeneral + File.separator + "UserPipeline" + File.separator + "UserInterestOnCategory_Final");
		    	Path qualifiedDirNameInput = fs.makeQualified(pathInput);
		    	readerUserInterestOnCategory = new MapFile.Reader(fs, qualifiedDirNameInput.toString(),conf);
			}

			MapFile.Reader readerDMatrix = null;
			{
		    	Path pathInputDMatrix = new Path(pathStrOutputGeneral + File.separator + "DMatrix" + "_Final");
		    	Path qualifiedDirNameInputDMatrix = fs.makeQualified(pathInputDMatrix);
				readerDMatrix = new MapFile.Reader(fs, qualifiedDirNameInputDMatrix.toString(),conf);
			}
			
			{
				hmDMatrix = new HashMap<IntPairWritable,Double>();
				
				IntPairWritable keyDMatrix = new IntPairWritable();
				DoubleWritable dpwValue = new DoubleWritable();
				
				Iterator<Integer> itCat1 = categoriesList.iterator();
				while(itCat1.hasNext())
				{
					Integer categoryID1 = itCat1.next();
					
					Iterator<Integer> itCat2 = categoriesList.iterator();
					while(itCat2.hasNext())
					{
						Integer categoryID2 = itCat2.next();
						
						if(categoryID1 == categoryID2) continue;

						keyDMatrix.set(categoryID1, categoryID2);
						dpwValue.set(0.0);
						try {
							readerDMatrix.get(keyDMatrix, dpwValue);
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						hmDMatrix.put(keyDMatrix.clone(), dpwValue.get());
					}
				}
			}
			readerDMatrix.close();
			
			Path trainingPath = new Path(pathStrOutputTraining + File.separator + "RatePrediction" + File.separator +  "TrainingModel");
			hmFst = new HashMap<Integer,FileStatus[]>();
			
			Iterator<Integer> itCat = categoriesList.iterator();
			while(itCat.hasNext())
			{
				Integer categoryID = itCat.next();

				FileStatus[] categoryFiles = null;
				try {
					categoryFiles = fs.listStatus(trainingPath, new PathFilterTrainingKDTrees(categoryID));
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				hmFst.put(categoryID, categoryFiles);
			}
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,Text,TestingObjectPhase1>.Context context) throws IOException ,InterruptedException {
			readerUserInterestOnCategory.close();

			super.cleanup(context);
		};
		
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			LongWritable userID = new LongWritable( Long.parseLong(strUserID));
			
			LinkedList<UserPreferenceWithCategoriesAndEstimations> ll = new LinkedList<UserPreferenceWithCategoriesAndEstimations>();
			// We begin from 1, because in 0 index userID is stored.
			if(strUserPreferences.length > 1)
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];

				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		normalizedRating
				//	strItemInfo[3]:		estimationRating from other Recommendation System (e.g. SlopeOne)
				String[] strItemInfo = strUP.split("-");
				
				long itemID = Long.parseLong(strItemInfo[0]);
				double normalizedRating = -1; 
				
				double estimationRating = -1;
				
				if(strItemInfo.length == 4)
				{
					normalizedRating = Double.parseDouble(strItemInfo[2]);
					estimationRating = Double.parseDouble(strItemInfo[3]);
				}
				else if (strItemInfo.length > 4)	//exponential forms
				{
					if(strItemInfo.length == 5) // one exponential form
					{
						if(strItemInfo[2].endsWith("E")) // the first
						{
							normalizedRating = Double.parseDouble(strItemInfo[2] + "-" + strItemInfo[3]);
							estimationRating = Double.parseDouble(strItemInfo[4]);
						}
						else
						{
							normalizedRating = Double.parseDouble(strItemInfo[2]);
							estimationRating = Double.parseDouble(strItemInfo[3] + "-" + strItemInfo[4]);
						}
					}
					else if(strItemInfo.length == 6) // two exponential forms
					{
						normalizedRating = Double.parseDouble(strItemInfo[2] + "-" + strItemInfo[3]);
						estimationRating = Double.parseDouble(strItemInfo[4] + "-" + strItemInfo[5]);
					}
				}
				
				String[] strCategoriesIDArray = strItemInfo[1].split("_");
				
				int[] intArrayCategories = new int[strCategoriesIDArray.length];
				
				for(int j = 0 ; j < strCategoriesIDArray.length ; j++) {
					intArrayCategories[j] = Integer.parseInt(strCategoriesIDArray[j]);
				}
				
				UserPreferenceWithCategoriesAndEstimations upwc = new UserPreferenceWithCategoriesAndEstimations(itemID, intArrayCategories, normalizedRating, estimationRating, -1.0);
				
				ll.add(upwc);
			}
			
			// User's Interest on a Category 
	    	
	    	MapWritable mw = new MapWritable();
	    	try {
				readerUserInterestOnCategory.get(userID, mw);
			} catch (IOException e) {
				e.printStackTrace();
			}	//	key: userID
    		
	    	double maxUserInterestOnCategory = -1;
    		int maxCategoryID = -1;
	    	
    		Set<Entry<Writable,Writable>> semw = mw.entrySet();
    		Iterator<Entry<Writable,Writable>> itEn = semw.iterator();
    		while(itEn.hasNext()){
    			Entry<Writable,Writable> en = itEn.next();
    			int categoryID = ((IntWritable)en.getKey()).get();
    			double categoryInterest = ((DoubleWritable)en.getValue()).get();
    			
    			if(categoryInterest > maxUserInterestOnCategory){
    				maxUserInterestOnCategory = categoryInterest;
    				maxCategoryID = categoryID;
    			}
    		}
    		
    		// D Matrix
    		IntPairWritable keyDMatrix = new IntPairWritable();

	    	Double valueDMatrix;
	    
	    	double sumDMatrixMaxCategoryID = 0;
			Iterator<Integer> itCat = categoriesList.iterator();
			while(itCat.hasNext())
			{
				Integer categoryID = itCat.next();
				if(maxCategoryID == categoryID.intValue()) continue;
				
				keyDMatrix.set(maxCategoryID, categoryID);

				valueDMatrix = hmDMatrix.get(keyDMatrix);

				sumDMatrixMaxCategoryID += valueDMatrix;
			}
	    	
	    	// Process...
			
			for(UserPreferenceWithCategoriesAndEstimations upwcae : ll)
			{
				double sum_dDimension = 0.0;
				double sum_CIDimension = 0.0;
				int counter_CIDimension = 0;

				int[] categoriesArray = upwcae.categoriesArray;
								
				DoubleWritable dwMaxCategory = (DoubleWritable)mw.get(new IntWritable(maxCategoryID));
				
				for(int i = 0 ; i < categoriesArray.length ; i++){
					
					if(maxCategoryID != categoriesArray[i])
					{
						keyDMatrix.set(maxCategoryID, categoriesArray[i]);
						valueDMatrix = hmDMatrix.get(keyDMatrix);
					}
					else
						valueDMatrix = 0.0;
					
					sum_dDimension += valueDMatrix;
					
					DoubleWritable dwTemp = (DoubleWritable)mw.get(new IntWritable(categoriesArray[i]));
					if(dwTemp != null)
					{
						sum_CIDimension += dwTemp.get();
						counter_CIDimension++;
					}
					
				}
				
				double dDimension = 0.0;
				double CIDimension = 0.0;

				dDimension = sum_dDimension / sumDMatrixMaxCategoryID;
				if(counter_CIDimension == 0) counter_CIDimension = 1;
				
				CIDimension = sum_CIDimension / (counter_CIDimension * dwMaxCategory.get());
				//
				
				for(int i = 0 ; i < categoriesArray.length ; i++){
					
					if(maxCategoryID != categoriesArray[i])
					{
						keyDMatrix.set(maxCategoryID, categoriesArray[i]);
						valueDMatrix = hmDMatrix.get(keyDMatrix);
					}
					else
						valueDMatrix = 0.0;
					
					if(valueDMatrix < disGenresThreshold)
						continue;

					TestingObjectPhase1 testingObjectP1 = new TestingObjectPhase1();
					testingObjectP1.setParameters(userID.get(), upwcae.itemID, upwcae.normalizedRating, upwcae.otherRecSysEstimationRating, dDimension, CIDimension);
					FileStatus[] fStatArray = hmFst.get(categoriesArray[i]);
					if(fStatArray.length > 0)
					{
						for(FileStatus fStat : fStatArray)
						{			
							Text textToSend = new Text(fStat.getPath().getName());
							
							context.write(textToSend, testingObjectP1);
						}
					}
				}
			}
		}
	}
	
	
	public static class Reduce extends Reducer<Text, TestingObjectPhase1, LongPairWritable, TestingObjectPhase1Output> {
		
		public long itemsOurRecSystemEstimatedValue;
		public long itemsOtherRecSystemEstimatedValue;
		
		private Configuration conf;
		private FileSystem fs;
		
		private String pathStrOutput = null;
		private String pathStrOutputTraining = null;

		private TreeSet<Integer> categoriesList = null;
		
		private int k_of_kNN = -1;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<Text,TestingObjectPhase1,LongPairWritable,TestingObjectPhase1Output>.Context context) throws IOException ,InterruptedException {
			file_MapFile_TestingModel1_keys = new TreeMap<Integer,TreeMap<Integer,Long>>();
			
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputGeneral = conf.get("pathStrOutputGeneral");
			pathStrOutputTraining = conf.get("pathStrOutputTraining");
			
			disGenresThreshold = Double.parseDouble(conf.get("disGenresThreshold"));
			k_of_kNN = Integer.parseInt(conf.get("k_of_kNN"));
			
			String[] categoriesListStrArray = conf.get("categoriesList").split(",");
			categoriesList = new TreeSet<Integer>();
			for(int i = 0 ; i < categoriesListStrArray.length ; i++)
				categoriesList.add(Integer.parseInt(categoriesListStrArray[i]));
			
			itemsOtherRecSystemEstimatedValue = 0L;
			itemsOurRecSystemEstimatedValue = 0L;
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<Text,TestingObjectPhase1,LongPairWritable,TestingObjectPhase1Output>.Context context) throws IOException ,InterruptedException {

			super.cleanup(context);
		};
		
		public class PathFilterTrainingKDTrees implements PathFilter
		{
			public int categoryID = -1;
			
			PathFilterTrainingKDTrees(int categoryID)
			{
				this.categoryID = categoryID;
			}
			
			public boolean accept(Path path) {
				if(path.getName().toString().startsWith(String.valueOf(categoryID) + "_"))
					return true;
				else
					return false;
			}
		}
		
		private Rednaxela3rdGenIteratedTreeKNNSearch readKDTree(Path fileStatusPath)
		{
			Rednaxela3rdGenIteratedTreeKNNSearch kDTree = null;
			
			Path fstQualifiedPath = fs.makeQualified(fileStatusPath);
			
			ObjectInputStream ois = null;

			try {
				ois = new ObjectInputStream(fs.open(fstQualifiedPath));
				kDTree = (Rednaxela3rdGenIteratedTreeKNNSearch) ois.readObject();
				ois.close();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return kDTree;
		}
		
		////////
		
		private TestingObjectPhase1Output calculateKNNPoints(double dDimension, double CIDimension, Rednaxela3rdGenIteratedTreeKNNSearch kDTree)
		{
			double[] loc = new double[2];
    		loc[0] = CIDimension;
    		loc[1] = dDimension;

			KNNPoint[] kNNPointArray = null;
			
			try
			{
				kNNPointArray = kDTree.getNearestNeighbors(loc, k_of_kNN);
			}
			catch(Exception e)
			{
				return null;
			}
			if(kNNPointArray == null)
				return null;
			
			int kNNPointsLength = kNNPointArray.length;
			double[] kNNPointsDistance = new double[kNNPointArray.length];
			double[] kNNPointsValueNormalizedRating = new double[kNNPointArray.length];
			
			for(int i = 0 ; i < kNNPointArray.length ; i++)
			{
				String valueStr = kNNPointArray[i].getValue();
				
				kNNPointsDistance[i] = kNNPointArray[i].getDistance();
				kNNPointsValueNormalizedRating[i] = Double.valueOf(valueStr);
			}
			
			TestingObjectPhase1Output tObjPh1Out = new TestingObjectPhase1Output();
			tObjPh1Out.kNNPointsLength = kNNPointsLength;
			tObjPh1Out.kNNPointsDistance = kNNPointsDistance;
			tObjPh1Out.kNNPointsValueNormalizedRating = kNNPointsValueNormalizedRating;
			
			return tObjPh1Out;
		}
		
		public void reduce(Text key, Iterable<TestingObjectPhase1> values, Context context) {
			String fileKDTree = key.toString();
			
			TreeSet<TestingObjectPhase1> tsTestObjPh1 = new TreeSet<TestingObjectPhase1>();
			
			for(TestingObjectPhase1 testObjPh1 : values)
			{
				tsTestObjPh1.add(testObjPh1.clone());
			}
			
			Rednaxela3rdGenIteratedTreeKNNSearch kDTree = readKDTree(new Path(pathStrOutputTraining + File.separator + "RatePrediction" + File.separator +  "TrainingModel" + File.separator + fileKDTree));
			
			String pathStr = pathStrOutput + File.separator + "TestingModelPhase1" + File.separator + fileKDTree;
			Path path = new Path(pathStr);
			Path qualifiedDirName = fs.makeQualified(path);
			
			MapFile.Writer writer = null;
			try {
				writer = new MapFile.Writer(conf, fs, qualifiedDirName.toString(), LongPairWritable.class, TestingObjectPhase1Output.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			long tempKeysCounter = 0L;
			
			Iterator<TestingObjectPhase1> itTestObjPh1  = tsTestObjPh1.iterator();
			while(itTestObjPh1.hasNext())
			{
				TestingObjectPhase1 tObjPh1 = itTestObjPh1.next();
				
				TestingObjectPhase1Output tObjPh1Out = calculateKNNPoints(tObjPh1.dDimension,tObjPh1.CIDimension,kDTree);
				if(tObjPh1Out != null)
				{
					tObjPh1Out.setParameters(tObjPh1.normalizedRating, tObjPh1.otherRecSysEstimationRating);
					
					tObjPh1Out.CIDimension = tObjPh1.CIDimension;
					tObjPh1Out.dDimension = tObjPh1.dDimension;
					
					LongPairWritable lpw = new LongPairWritable();
					lpw.set(tObjPh1.userID, tObjPh1.itemID);
					
					try {
						writer.append(lpw, tObjPh1Out);
					} catch (IOException e) {
						e.printStackTrace();
					}
					tempKeysCounter++;
				}
			}
			
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Path pathQualifiedFrom = fs.makeQualified(new Path(pathStrOutput + File.separator + "TestingModelPhase1" + File.separator + fileKDTree));
			Path pathQualifiedTo = fs.makeQualified(new Path(pathStrOutput + File.separator + "TestingModelPhase1" + File.separator + fileKDTree + "_" + tempKeysCounter));
			
			try {
				fs.rename(pathQualifiedFrom, pathQualifiedTo);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} 
	}
	
	private static void getFileKeysAndSize()
	{
		Path path = new Path(pathStrOutput + File.separator + "TestingModelPhase1");
		Path pathQualified = fs.makeQualified(path);
	    
		FileStatus[] fstatMapFiles = null;
		try {
			fstatMapFiles = fs.listStatus(pathQualified);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(FileStatus fstat : fstatMapFiles)
		{
			Path pathFstat = fstat.getPath();
			String strFileName = pathFstat.getName();
			
			String[] strFileNameTokensArray = strFileName.split("_");
			
			int categoryID = Integer.parseInt(strFileNameTokensArray[0]);
			int categoryFilePart = Integer.parseInt(strFileNameTokensArray[1]);
			
			long keys = Long.parseLong(strFileNameTokensArray[2]);
			
			//
			TreeMap<Integer, Long> tmKeysCategoryPart = file_MapFile_TestingModel1_keys.get(categoryID);
			
			if(tmKeysCategoryPart == null)
				tmKeysCategoryPart = new TreeMap<Integer, Long>();
			
			tmKeysCategoryPart.put(categoryFilePart, keys);
			
			file_MapFile_TestingModel1_keys.put(categoryID, tmKeysCategoryPart);
			//
			
			//
			TreeMap<Integer, Long> tmSizeCategoryPart = file_MapFile_TestingModel1_index_sizeInBytes.get(categoryID);
			
			if(tmSizeCategoryPart == null)
				tmSizeCategoryPart = new TreeMap<Integer, Long>();
			
			tmSizeCategoryPart.put(categoryFilePart, keys);
			
			file_MapFile_TestingModel1_index_sizeInBytes.put(categoryID, tmSizeCategoryPart);
			//
		}
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evTesting.copyValues(file_MapFile_TestingModel1_keys, file_MapFile_TestingModel1_index_sizeInBytes);
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputGeneral", pathStrOutputGeneral);
		conf.set("pathStrOutputTraining", pathStrOutputTraining);
		
		conf.set("disGenresThreshold", String.valueOf(disGenresThreshold));
		conf.set("k_of_kNN", String.valueOf(k_of_kNN));
		
		String categoriesListStr = "";
		Iterator<Integer> itCat = categoriesList.iterator();
		while(itCat.hasNext())
		{
			Integer categoryID = itCat.next();
			categoriesListStr += categoryID;
			if(itCat.hasNext())
				categoriesListStr += ",";
		}
		
		conf.set("categoriesList", categoriesListStr);
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Testing Model Phase1 Driver - Start");
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathTesting() + File.separator +"RatePrediction";
		pathStrOutputGeneral = evcg.getOutputPathGeneral();
		pathStrOutputTraining = evcg.getOutputPathTraining();
		
		TestingModelPhase1Driver.disGenresThreshold = evcg.evTraining.disGenresThreshold.getValue(evcg.evGeneral);
		TestingModelPhase1Driver.categoriesList = evcg.evGeneral.categoriesList;
		TestingModelPhase1Driver.k_of_kNN = evcg.evTesting.k_of_kNN_TestingModel;
		
		setParameters(conf);
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TestingModelPhase1")))
			{
				System.out.println("*Testing Model Driver*");
				System.out.println("Folder: " + pathStrOutput + File.separator + "TestingModelPhase1");
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TestingModelPhase1")))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}

		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput + File.separator + "TestingModelPhase1"), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_OurSystemInput" + File.separator + "data";
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathTesting() + File.separator + "DummyDirectory" + File.separator + dt.getTime();

		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "TestModelPhase1Driver");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(TestingModelPhase1Driver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(TestingObjectPhase1.class); 
		
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Reducer
		job.setOutputKeyClass(LongPairWritable.class);
		job.setOutputValueClass(TestingObjectPhase1Output.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);

		getFileKeysAndSize();
		updateInfo(evcg);
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "TestingModel"));
		
		long stop = System.nanoTime();
		evcg.evTesting.time_TestingModelPhase1Driver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Testing Model Phase1 Driver - Stop");
		evcg.hasChangedTesting = true;
		
		conf.clear();
		fs.close();
		
		System.runFinalization();
		System.gc();
	}
	
}