package commons;

import java.io.Serializable;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

/**
 * 
 * An array for the IntWritable objects.
 * 
 * @author john
 *
 */
public class IntArrayWritable  extends ArrayWritable implements Serializable{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = -5921036216206105580L;

	public IntArrayWritable() 
	{ 
		super(IntWritable.class); 
	}
	
	@Override
	public String toString(){
		Writable[] writableArray = this.get();
		
		String result = "";
		for(int i = 0 ; i < writableArray.length ; i++) { 
			if((i+1)==writableArray.length)
				result += ((IntWritable) writableArray[i]).toString();
			else
				result += ((IntWritable) writableArray[i]).toString() + ", ";
		}
		return result;
	}
}