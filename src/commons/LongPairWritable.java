package commons;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * 
 * As the name of this class implies it's a writable object defined by
 * two long values.
 * 
 * @author John Koumarelas
 *
 */
public class LongPairWritable implements WritableComparable<LongPairWritable>{
	private long first = 0;
	private long second = 0;

	/**
	 * Set the left and right values.
	 */
	public void set(long left, long right) {
		first = left;
		second = right;
	}
	public long getFirst() {
		return first;
	}
	public long getSecond() {
		return second;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		first = in.readLong() + Long.MIN_VALUE;
		second = in.readLong() + Long.MIN_VALUE;
	}
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(first - Long.MIN_VALUE);
		out.writeLong(second - Long.MIN_VALUE);
	}
	@Override
	public int hashCode() {
		return (int)first * 157 + (int)second;
	}
	@Override
	public boolean equals(Object right) {
		if (right instanceof LongPairWritable) {
			LongPairWritable r = (LongPairWritable) right;
			return r.first == first && r.second == second;
		} else {
			return false;
		}
	}
	/** A Comparator that compares serialized IntPair. */ 
	public static class Comparator extends WritableComparator {
		public Comparator() {
			super(LongPairWritable.class);
		}

		@Override
		public int compare(byte[] b1, int s1, int l1,
				byte[] b2, int s2, int l2) {
			return compareBytes(b1, s1, l1, b2, s2, l2);
		}
	}

	static {                                        // register this comparator
		WritableComparator.define(LongPairWritable.class, new Comparator());
	}

	@Override
	public int compareTo(LongPairWritable o) {
		if (first != o.first) {
			return first < o.first ? -1 : 1;
		} else if (second != o.second) {
			return second < o.second ? -1 : 1;
		} else {
			return 0;
		}
	}
}
