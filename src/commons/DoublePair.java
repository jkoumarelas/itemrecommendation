package commons;

/**
 * 
 * As the name of this class implies it's an object defined by
 * two double values.
 * 
 * @author John Koumarelas
 *
 */
public class DoublePair implements Comparable<DoublePair>{
	private double first = 0;
	private double second = 0;

	public void set(DoublePair dp)
	{
		this.first = dp.first;
		this.second = dp.second;
	}
	/**
	 * Set the left and right values.
	 */
	public void set(double left, double right) {
		first = left;
		second = right;
	}
	public double getFirst() {
		return first;
	}
	public double getSecond() {
		return second;
	}

//	@Override
//	public int hashCode() {
//		return (int)first * 157 + (int)second;
//	}
	@Override
	public boolean equals(Object right) {
		if (right instanceof DoublePair) {
			DoublePair r = (DoublePair) right;
			return r.first == first;
		} else {
			return false;
		}
	}

	public int compareTo(DoublePair o) {
		if (first != o.first) {
			return first < o.first ? -1 : 1;
		} 
//		else if (second != o.second) {
//			return second < o.second ? -1 : 1;
//		} 
		else {
			return 0;
		}
	}
}
