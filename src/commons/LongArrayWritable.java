package commons;

import java.io.Serializable;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

/**
 * 
 * An array for the LongWritable objects.
 * 
 * @author John Koumarelas
 *
 */
public class LongArrayWritable  extends ArrayWritable implements Serializable{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = -5654733121679854069L;

	public LongArrayWritable() 
	{ 
		super(LongWritable.class); 
	}
	
	public String toString(){
		Writable[] writableArray = this.get();
		
		String result = "";
		for(int i = 0 ; i < writableArray.length ; i++) { 
			if((i+1)==writableArray.length)
				result += ((LongWritable) writableArray[i]).toString();
			else
				result += ((LongWritable) writableArray[i]).toString() + ", ";
		}
		return result;
	}
}