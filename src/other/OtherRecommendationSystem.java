package other;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.CachingRecommender;
import org.apache.mahout.cf.taste.impl.recommender.slopeone.SlopeOneRecommender;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.Recommender;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;
import other.executionEnvironment.ExecutionVariablesOtherRecommendationSystem;
import other.naiveBayes.NaiveBayesEvaluator;
import other.naiveBayes.Tuple;
import other.naiveBayes.User;

/**
 * 
 * OtherRecommendationSystem.java
 * 
 * This class is the place where another recommendation system provides us
 * with it's recommendations which we save and use in the following Training,
 * Testing and SimilarityEstimation phases.
 * 
 * Adding an extra Recommendation System is a very easy task.
 * 		You need to provide a function in this class that outputs it's results
 * 		in the "userPreferences_testing_RecSysOutput", as you can find in the
 * 		SlopeOne's function. You must call it in this class's run() method and
 * 		add it where it's necessary (during parsing, enumaration in 
 * 		OtherRecommendationSystem etc.)
 * 
 * @author John Koumarelas
 *
 */
public class OtherRecommendationSystem {
	private static Configuration conf = null;
	private static FileSystem fs = null;

public static boolean NaiveBayes(ExecutionVariablesCurrentGroup evcg) throws IOException, TasteException {
		
		// We don't output to the normal "userPreferences_testing_RecSysOutput", because we can skip the next phase which is merging
		//	the results with our system's, and go directly to the results that the merging phase would output.
//		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysOutput";
		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_OurSystemInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Other Recommendation System*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		
		String trainingFilePathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_training_RecSysInput" + File.separator + "data";
		String testingFilePathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysInput" + File.separator + "data";
		String outputFilePathStr = outputPathStr + File.separator + "data";
		
		FSDataInputStream fsInputStreamTraining = fs.open(new Path(trainingFilePathStr));
		BufferedReader brTraining = new BufferedReader(new InputStreamReader(fsInputStreamTraining));

		FSDataInputStream fsInputStreamTesting = fs.open(new Path(testingFilePathStr));
		BufferedReader brTesting = new BufferedReader(new InputStreamReader(fsInputStreamTesting));
		
		FSDataOutputStream fsOutputStreamTesting = fs.create(new Path(outputFilePathStr));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutputStreamTesting));

		String strLineTraining;
		String strLineTesting;
		
		boolean firstTime = true;
		boolean previousUserPrinted = false;
		
		long itemsEstimatedValues = 0L;
		long itemsDidNotEstimateValues = 0L;
		
		while (true)   {
		 
			strLineTraining = brTraining.readLine();
			strLineTesting = brTesting.readLine();
			
			if ((strLineTraining == null) || (strLineTesting == null))
				break;
			
			try{
				String[] strTokensTraining = strLineTraining.split("\t");
				  
				String userIDTrainingStr = strTokensTraining[0];
				long userIDTraining = Long.valueOf(userIDTrainingStr);
				
				String[] strTokensTesting = strLineTesting.split("\t");
				
				String userIDTestingStr = strTokensTesting[0];
				long userIDTesting = Long.valueOf(userIDTestingStr);
				
				while(userIDTraining != userIDTesting)
				{
					if(userIDTraining > userIDTesting)
					{
						strLineTesting = brTesting.readLine();
						
						strTokensTesting = strLineTesting.split("\t");
						
						userIDTestingStr = strTokensTesting[0];
						userIDTesting = Long.valueOf(userIDTestingStr);
					}
					else if(userIDTraining < userIDTesting)
					{
						strLineTraining = brTraining.readLine();
						
						strTokensTraining = strLineTraining.split("\t");
						  
						userIDTrainingStr = strTokensTraining[0];
						userIDTraining = Long.valueOf(userIDTrainingStr);
					}
				}
				
				Map<Long, Tuple> trainingData = new TreeMap<Long, Tuple>();
				
				for(int i = 1 ; i < strTokensTraining.length ; i++)
				{
					String[] strUP = strTokensTraining[i].split("-");
					
					String itemIDTrainingStr = strUP[0];
					String ratingTrainingStr = "";
					
					if(strUP.length == 3)
						ratingTrainingStr = strUP[2];
					else if(strUP.length == 4)
						ratingTrainingStr = strUP[2] + "-" + strUP[3];	//	exponential form: e.g. 5.1234E-14
					
					long itemIDTraining = Long.valueOf(itemIDTrainingStr);
					double ratingTraining = Double.parseDouble(ratingTrainingStr);
					
					String[] categoriesStr = strUP[1].split("_");
					
					TreeSet<Integer> tsCategories = new TreeSet<Integer>();
					for(String catStr : categoriesStr)
						tsCategories.add(Integer.parseInt(catStr));
					
					Vector<Boolean> dataVector = new Vector<Boolean>();
					
					Iterator<Integer> itTotalCategories = evcg.evGeneral.categoriesList.iterator();
					while(itTotalCategories.hasNext())
					{
						Integer categoryID = itTotalCategories.next();
						
						if(tsCategories.contains(categoryID))
							dataVector.add(true);
						else
							dataVector.add(false);
					}
					
					Tuple tuple = new Tuple(dataVector, ratingTraining, userIDTraining, itemIDTraining);
					trainingData.put(itemIDTraining, tuple);
				}
				
				Map<Long, Tuple> testingData = new TreeMap<Long, Tuple>();
				
				for(int i = 1 ; i < strTokensTesting.length ; i++)
				{
					String[] strUP = strTokensTesting[i].split("-");
					
					String itemIDTestingStr = strUP[0];
					String ratingTestingStr = strUP[2];
					
					if(strUP.length == 3)
						ratingTestingStr = strUP[2];
					else if(strUP.length == 4)
						ratingTestingStr = strUP[2] + "-" + strUP[3];	//	exponential form: e.g. 5.1234E-14
					
					long itemIDTesting = Long.valueOf(itemIDTestingStr);
					double ratingTesting = Double.parseDouble(ratingTestingStr);

					String[] categoriesStr = strUP[1].split("_");
					
					TreeSet<Integer> tsCategories = new TreeSet<Integer>();
					for(String catStr : categoriesStr)
						tsCategories.add(Integer.parseInt(catStr));
					
					Vector<Boolean> dataVector = new Vector<Boolean>();
					
					Iterator<Integer> itTotalCategories = evcg.evGeneral.categoriesList.iterator();
					while(itTotalCategories.hasNext())
					{
						Integer categoryID = itTotalCategories.next();
						
						if(tsCategories.contains(categoryID))
							dataVector.add(true);
						else
							dataVector.add(false);
					}
					
					Tuple tuple = new Tuple(dataVector, ratingTesting, userIDTesting, itemIDTesting);
					testingData.put(itemIDTesting, tuple);
				}
				
				User userTraining = new User(userIDTraining,trainingData);
				User userTesting = new User(userIDTesting, testingData);
				
				TreeMap<Long, Double> testingEstimations = NaiveBayesEvaluator.runNaiveBayes(userTraining, userTesting);
				
				if(testingEstimations.size() > 0)
				{
					if(firstTime)
						firstTime = false;
					else
					{
						if(previousUserPrinted)
							bw.write("\r\n");
					}
					
					boolean printedUserID = false;
					previousUserPrinted = false;
					
						for(int i = 1 ; i < strTokensTesting.length ; i++)
						{
							String[] strUP = strTokensTesting[i].split("-");
							
							String itemIDTestingStr = strUP[0];
							
							String ratingTestingStr = "";
							
							if(strUP.length == 3)
								ratingTestingStr = strUP[2];
							else if(strUP.length == 4)
								ratingTestingStr = strUP[2] + "-" + strUP[3];
							
							long itemIDTesting = Long.valueOf(itemIDTestingStr);
							
							for(Entry<Long, Double> entry : testingEstimations.entrySet())
							{
								if(entry.getKey() == itemIDTesting)
								{
									Double naiveBayesEstimation = entry.getValue();
									
									if(naiveBayesEstimation > 0.0)
									{
										itemsEstimatedValues++;
										if(printedUserID == false)
										{
											printedUserID = true;
											previousUserPrinted = true;
											bw.write(userIDTestingStr);
										}
										
										bw.write("\t" + itemIDTestingStr + "-" + strUP[1] + "-" + ratingTestingStr + "-" + naiveBayesEstimation);
									}
									else
										itemsDidNotEstimateValues++;
								}
							}
							
						}
						
				}
			}
			catch (Exception e){
				  System.err.println("Error: " + e.getMessage());
		    }
	    }
		bw.close();
		brTraining.close();
		brTesting.close();
		
		evcg.evOtherRecSys.itemsEstimatedValues = itemsEstimatedValues;
		evcg.evOtherRecSys.itemsDidNotEstimateValues = itemsDidNotEstimateValues;
		
	    return true;
	}
	
	public static boolean SlopeOne(ExecutionVariablesCurrentGroup evcg) throws IOException, TasteException {
		
		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysOutput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Other Recommendation System*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		
		String trainingFilePath = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_training_RecSysInput" + File.separator + "data";
		String localTrainingFilePath = "dataTraining";
		
		String testingFilePath = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysInput" + File.separator + "data";
		String outputFilePath = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysOutput" + File.separator + "data";
		
		File localTrainingFile = new File(localTrainingFilePath);
		localTrainingFile.delete();
		File localTrainingFileCrc = new File("." + localTrainingFilePath + ".crc");
		localTrainingFileCrc.delete();
		
		fs.copyToLocalFile(new Path(trainingFilePath), new Path(localTrainingFilePath));
		
		DataModel model = null;
		model = new FileDataModel(new File(localTrainingFilePath));
		
        // Make a weighted slope one recommender
        Recommender recommender = null;
		recommender = new SlopeOneRecommender(model);

        Recommender cachingRecommender = null;
		cachingRecommender = new CachingRecommender(recommender);

		FSDataInputStream fsStream = fs.open(new Path(testingFilePath));
		
//		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fsStream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		FSDataOutputStream fsStreamOut = fs.create(new Path(outputFilePath));
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsStreamOut));

		long itemsEstimatedValues = 0L;
		long itemsDidNotEstimateValues = 0L;
		
		String strLine;

		while ((strLine = br.readLine()) != null)   {
		 
			try{
				itemsEstimatedValues++;
				
				String[] strTokens = strLine.split(",");
				  
				String userID = strTokens[0];
				String itemID = strTokens[1];
				
				String rating = "";
				
				if(strTokens.length == 3)
					rating = strTokens[2];
				else if(strTokens.length == 4)
					rating = strTokens[2] + "-" + strTokens[3]; //	exponential form: e.g. 5.1234E-14
				 
				  
				long userID_L = Long.valueOf(userID);
				long itemID_L = Long.valueOf(itemID);
				  
				float estimationSlopeOne = cachingRecommender.estimatePreference(userID_L, itemID_L);
				
				if(estimationSlopeOne > 5.0)
					estimationSlopeOne = 5.0f;
				else if(estimationSlopeOne < 0.0)
					estimationSlopeOne = 0.0f;
				  
				bw.write(userID + "," + itemID +"," + rating + "," + String.valueOf(estimationSlopeOne) + "\r\n");
			}
			catch (Exception e){
				  System.err.println("Error: " + e.getMessage());
		    }
	    }
		bw.close();
	    br.close();
	    
	    localTrainingFile.delete();
	    localTrainingFileCrc.delete();
	    
	    evcg.evOtherRecSys.itemsEstimatedValues = itemsEstimatedValues;
		evcg.evOtherRecSys.itemsDidNotEstimateValues = itemsDidNotEstimateValues;
	    
	    return true;
	}
	
	public static void run(ExecutionVariablesCurrentGroup evcg){
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Other Recommendation System - Start");
		
		long start = System.nanoTime();
		
		try {
			conf = new Configuration();
			fs = FileSystem.get(conf);

			if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.SLOPE_ONE)
			{
				if(SlopeOne(evcg) == false)
					return;
			}
			else if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.NAIVE_BAYES)
			{
				if(NaiveBayes(evcg) == false)
					return;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TasteException e) {
			e.printStackTrace();
		}
		
		long stop = System.nanoTime();
		evcg.evOtherRecSys.time_OtherRecommendationSystem = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Other Recommendation System - Stop");
		evcg.hasChangedOtherRecommendationSystem = true;
		
		System.runFinalization();
		System.gc();
	}
	
}
