package other.executionEnvironment;

import java.io.Serializable;

/**
 * 
 * ExecutionVariablesSimilarityEstimation.java
 * 
 * This class provides an easy and structured way of giving access
 * to parameters that are provided in "ExecutionParameters.txt".
 * 
 * We also maintain other fields that help other Classes during the execution,
 * and there are also fields for measuring execution time and specific file sizes
 * (files that must be loaded in the RAM, such as MapFile's index).
 * 
 * Objects from this class and the other ExecutionVariablesX classes, are saved
 * and can be loaded again for future statistics and refreshing of statistics
 * by our program.
 * 
 * @author John Koumarelas
 * @see ExecutionVariablesGeneral, ExecutionVariablesOtherRecommendationSystem, 
 * 		ExecutionVariablesTraining, ExecutionVariablesTesting
 */
public class ExecutionVariablesSimilarityEstimation implements Serializable{
	
	private static final long serialVersionUID = -6877059655455427263L;
	
	// Execution Parameters
	public double b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation = -1.0;
	public int topK_SimilarityEstimation = -1;
	
	// Execution Times
	public long time_SimilarityEstimation = -1L;
	
	// Execution Output
	public double RMSE_OtherRecommendationSystem = -1.0;
	public double RMSE_OurRecommendationSystem = -1.0;
	
	public double nDCG_OtherRecommendationSystem = -1.0;
	public double nDCG_OurRecommendationSystem = -1.0;
	
	public ExecutionVariablesSimilarityEstimation clone()
	{
		ExecutionVariablesSimilarityEstimation evSimEstim = new ExecutionVariablesSimilarityEstimation();
		
		evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation = this.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation;
		evSimEstim.topK_SimilarityEstimation = this.topK_SimilarityEstimation;
		
		evSimEstim.time_SimilarityEstimation = this.time_SimilarityEstimation;
		
		evSimEstim.RMSE_OtherRecommendationSystem = this.RMSE_OtherRecommendationSystem;
		evSimEstim.RMSE_OurRecommendationSystem = this.RMSE_OurRecommendationSystem;
		
		evSimEstim.nDCG_OtherRecommendationSystem = this.nDCG_OtherRecommendationSystem;
		evSimEstim.nDCG_OurRecommendationSystem = this.nDCG_OurRecommendationSystem;
		
		return evSimEstim;
	}
	
	public String toStringStatistics(int numTabs){
		String str = "";
		
		String tabsStr = ExecutionVariablesGeneral.retNumTabs(numTabs);
		
		str += tabsStr + "Execution Parameters" + "\r\n\r\n";
		
		str += tabsStr + "b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation: " + b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation + "\r\n";
		str += tabsStr + "topK_SimilarityEstimation: " + topK_SimilarityEstimation + "\r\n\r\n";
		
		str += tabsStr + "Execution Times: ns: nanoseconds, s: seconds" + "\r\n\r\n";
		
		str += tabsStr + "SimilarityEstimation: " + time_SimilarityEstimation + "ns\t" + time_SimilarityEstimation / 1000000000.0 + "s" + "\r\n\r\n";
		
		str += tabsStr + "Execution Output" + "\r\n\r\n";
		
		str += tabsStr + "RMSE - RMSE_OtherRecommendationSystem: " + RMSE_OtherRecommendationSystem + "\r\n";
		str += tabsStr + "RMSE - RMSE_OurRecommendationSystem: " + RMSE_OurRecommendationSystem + "\r\n";
		str += tabsStr + "RMSE - Improvement (%): "  +  (100 - ((100 * RMSE_OurRecommendationSystem) / RMSE_OtherRecommendationSystem)) + "\r\n";
		
		str += tabsStr + "DCG - nDCG_OtherRecommendationSystem: " + nDCG_OtherRecommendationSystem + "\r\n";
		str += tabsStr + "DCG - nDCG_OurRecommendationSystem: " + nDCG_OurRecommendationSystem + "\r\n";
		str += tabsStr + "DCG - Improvement (%): "  +  (100 - ((100 * nDCG_OurRecommendationSystem) / nDCG_OtherRecommendationSystem)) + "\r\n";
		
		return str;
	}
}
