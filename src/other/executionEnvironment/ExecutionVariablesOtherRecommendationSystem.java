package other.executionEnvironment;

import java.io.Serializable;
import java.util.LinkedList;


/**
 * 
 * ExecutionVariablesOtherRecommendationSystem.java
 * 
 * This class provides an easy and structured way of giving access
 * to parameters that are provided in "ExecutionParameters.txt".
 * 
 * We also maintain other fields that help other Classes during the execution,
 * and there are also fields for measuring execution time and specific file sizes
 * (files that must be loaded in the RAM, such as MapFile's index).
 * 
 * Objects from this class and the other ExecutionVariablesX classes, are saved
 * and can be loaded again for future statistics and refreshing of statistics
 * by our program.
 * 
 * @author John Koumarelas
 * @see ExecutionVariablesGeneral, ExecutionVariablesTraining, 
 * 		ExecutionVariablesTesting, ExecutionVariablesSimilarityEstimation
 */
public class ExecutionVariablesOtherRecommendationSystem implements Serializable{

	private static final long serialVersionUID = -4701676291342695141L;

	// Execution Variables - Test
	public LinkedList<ExecutionVariablesTraining> llTraining = new LinkedList<ExecutionVariablesTraining>();
	
	// Execution Parameters-Values
	public OtherRecommendationSystem otherRecSys = null; 
	public enum OtherRecommendationSystem {
		SLOPE_ONE,
		NAIVE_BAYES;
		
		public String toString(){
			if(this == SLOPE_ONE)
				return "SLOPE_ONE";
			else if(this == NAIVE_BAYES)
				return "NAIVE_BAYES";
			else
				return "NULL_OTHER_RECOMMENDATION_SYSTEM";
		}
	}
	public long itemsEstimatedValues = -1L;
	public long itemsDidNotEstimateValues = -1L;
	
	public String toString()
	{
		return otherRecSys.toString();
	}
	
	// Execution Times
	public long time_NormalizeRatings = -1L;
	public long time_OtherRecommendationSystem = -1L;
	public long time_MergeOtherRecSysWithOur = -1L;
	
	// Execution Values
	
	
	public String toStringStatistics(int numTabs){
		String str = "";
		
		String tabsStr = ExecutionVariablesGeneral.retNumTabs(numTabs);
		
		str += tabsStr + "Execution Parametes-Values" + "\r\n\r\n";
		
		str += tabsStr + "otherRecSys: " + otherRecSys.toString() + "\r\n\r\n";
		str += tabsStr + "itemsEstimatedValues: " + itemsEstimatedValues + "\r\n";
		str += tabsStr + "itemsDidNotEstimateValues: " + itemsDidNotEstimateValues + "\r\n";
		
		str += tabsStr + "Execution Times: ns: nanoseconds, s: seconds" + "\r\n\r\n";
		
		str += tabsStr + "NormalizeRatings: " + time_NormalizeRatings + "ns\t" + time_NormalizeRatings / 1000000000.0 + "s"  + "\r\n";
		str += tabsStr + "OtherRecommendationSystem: " + time_OtherRecommendationSystem + "ns\t" + time_OtherRecommendationSystem / 1000000000.0 + "s" + "\r\n";
		str += tabsStr + "MergeOtherRecSysWithOur: " + time_MergeOtherRecSysWithOur + "ns\t" + time_MergeOtherRecSysWithOur / 1000000000.0 + "s" + "\r\n";
		
		
		
		return str;
	}
}
