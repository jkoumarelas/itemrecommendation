package other.executionEnvironment;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * 
 * In this class we read the parameters that are specified in the
 * "ExecutionParameters.txt". For more info about the template
 * in which the parameters are expected please see the
 * "Readme_ExecutionParameters.txt".
 * 
 * @author John Koumarelas
 *
 */
public class ReadParameters {

	public static boolean run(String[] args, LinkedList<ExecutionVariablesGeneral> llEvg)
	{
		boolean refreshStatistics = false;
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			Path fromFSFilePath = new Path(args[0] + File.separator + "ExecutionParameters.txt");
			String outputPath = "./ExecutionParameters.txt";
			Path toLocalFilePath = new Path(outputPath);
			
			fs.copyToLocalFile(fromFSFilePath, toLocalFilePath);
			
			FileInputStream fstream = new FileInputStream("./ExecutionParameters.txt");
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			
			while (true)   {
				strLine = br.readLine();
				if(strLine == null)
					break;
				
				if(strLine.trim().length() == 0)	//	white space only
					continue;
				else
				{
					String[] firstLineTokens = strLine.split(",");
					
					strLine = br.readLine();
					if(strLine == null)
					{
						System.err.println("Wrong ExecutionParameters.txt format.");
						System.exit(-1);
					}
					
					String[] secondLineTokens = strLine.split(",");
					
					LinkedList<String> llMainParameters = new LinkedList<String>();
					while(((strLine = br.readLine()) != null) && (strLine.trim().length() != 0))
					{
						llMainParameters.add(strLine);
					}
										
					ExecutionVariablesGeneral tempEvGeneral = new ExecutionVariablesGeneral();
					
					//
					tempEvGeneral.inputPath = args[0];
					tempEvGeneral.outputPath = args[1];

					/*****************First Line***************/
					//
					tempEvGeneral.numberOfReduceTasks = Integer.parseInt(firstLineTokens[0]);
					
					//
					if(firstLineTokens[1].compareToIgnoreCase("true") == 0)
						tempEvGeneral.showStartStopPrints = true;
					else
						tempEvGeneral.showStartStopPrints = false;
					
					//
					if(firstLineTokens[2].compareToIgnoreCase("true") == 0)
						tempEvGeneral.clearWholeOutputFolderBeforeStart = true;
					else
						tempEvGeneral.clearWholeOutputFolderBeforeStart = false;
					
					//
					if(firstLineTokens[3].compareToIgnoreCase("true") == 0)
						tempEvGeneral.deleteTempFilesAfterExecution = true;
					else
						tempEvGeneral.deleteTempFilesAfterExecution = false;
					
					//
					if(firstLineTokens[4].compareToIgnoreCase("true") == 0)
						tempEvGeneral.askBeforeOverwritingΕxistingFolders = true;
					else
						tempEvGeneral.askBeforeOverwritingΕxistingFolders = false;
					
					//
					if(firstLineTokens[5].compareToIgnoreCase("true") == 0)
						tempEvGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders = true;
					else
						tempEvGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders = false;
					
					/***********Second Line***************/
					
					//
					tempEvGeneral.datasetName = secondLineTokens[0];
					
					//
					tempEvGeneral.trainingPercentage = Double.parseDouble(secondLineTokens[1].substring(0, secondLineTokens[1].indexOf("%"))) / 100.0;	// In our system 0.3 represents 30%.
					
					//
					tempEvGeneral.testingPercentage = Double.parseDouble(secondLineTokens[2].substring(0, secondLineTokens[2].indexOf("%"))) / 100.0;	// In our system 0.3 represents 30%.
					
					//
					if(secondLineTokens[3].indexOf("%") != -1)
						tempEvGeneral.usersPercentage = Double.parseDouble(secondLineTokens[3].substring(0, secondLineTokens[3].indexOf("%"))) / 100.0;
					else
						tempEvGeneral.usersNumber = Long.parseLong(secondLineTokens[3]);
						
					/***********Third Line***************/
					
					
					for(String mainParametersStr : llMainParameters)
					{
						String[] mainParametersTokens = mainParametersStr.split(" ");
						
						//
						if(mainParametersTokens[0].compareToIgnoreCase("all") == 0)
						{
							String firstParameterTokens[] = mainParametersTokens[1].split(",");
							
							String secondParameterTokens[] = mainParametersTokens[2].split(",");
							
							String thirdParameterTokens[] = mainParametersTokens[3].split(",");
							
							String fourthParameterTokens[] = mainParametersTokens[4].split(",");
							
							String fifthParameterTokens[] = mainParametersTokens[5].split(",");
							
							String sixthParameterTokens[] = mainParametersTokens[6].split(",");
							
							for(String s1 : firstParameterTokens)
							{
							
								ExecutionVariablesGeneral evGeneral = tempEvGeneral.clone();
								evGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity = Double.parseDouble(s1);
								
								boolean foundInListGeneral = false;
								Iterator<ExecutionVariablesGeneral> itEvGeneral = llEvg.iterator();
								while(itEvGeneral.hasNext())
								{
									ExecutionVariablesGeneral tEvGeneral = itEvGeneral.next();
									
									boolean boolean1 = tEvGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity == evGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
									boolean boolean2 = tEvGeneral.testingPercentage == evGeneral.testingPercentage;
									boolean boolean3 = tEvGeneral.trainingPercentage == evGeneral.trainingPercentage;
									boolean boolean4 = false;
									
									if((tEvGeneral.usersNumber > -1L) && (evGeneral.usersNumber > -1L))
										boolean4 = tEvGeneral.usersNumber == evGeneral.usersNumber;
									else
										boolean4 = tEvGeneral.usersPercentage == evGeneral.usersPercentage;
									
									boolean boolean5 = false;
									if(tEvGeneral.datasetName.compareToIgnoreCase(evGeneral.datasetName) == 0)
										boolean5 = true;
									
									// ... we can compute the rest parameters...
									
									boolean booleanTotal = boolean1 && boolean2 && boolean3 && boolean4 && boolean5;
									
									if(booleanTotal)
									{
										evGeneral = tEvGeneral;
										foundInListGeneral = true;
										break;
									}
								}
								
								for(String s2 : secondParameterTokens)
								{
									ExecutionVariablesOtherRecommendationSystem evOtherRecSys = new ExecutionVariablesOtherRecommendationSystem();
									
									if(s2.compareToIgnoreCase("naive-bayes") == 0)
										evOtherRecSys.otherRecSys = ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.NAIVE_BAYES;
									else if(s2.compareToIgnoreCase("slope-one") == 0)
										evOtherRecSys.otherRecSys = ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.SLOPE_ONE;
									else
									{
										System.err.println("Unknown other recommendation system in parameters. Exiting...");
										System.exit(-1);
									}
									
									boolean foundInListOtherRecommendationSystem = false;
									Iterator<ExecutionVariablesOtherRecommendationSystem> itEvOtherRecSys = evGeneral.llOtherRecSys.iterator();
									while(itEvOtherRecSys.hasNext())
									{
										ExecutionVariablesOtherRecommendationSystem tEvOtherRecSys = itEvOtherRecSys.next();
										if(tEvOtherRecSys.otherRecSys == evOtherRecSys.otherRecSys)
										{
											evOtherRecSys = tEvOtherRecSys;
											foundInListOtherRecommendationSystem = true;
											break;
										}
									}
									
									for(String s3 : thirdParameterTokens)
									{
										ExecutionVariablesTraining evTraining = new ExecutionVariablesTraining();
										
										if(s3.compareToIgnoreCase("zero") == 0)
											evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.ZERO;
										else if(s3.compareToIgnoreCase("median") == 0)
											evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN;
										else if(s3.compareToIgnoreCase("median+std") == 0)
											evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_PLUS_STD;
										else if(s3.compareToIgnoreCase("median+2std") == 0)
											evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_PLUS_2_STD;
										else if(s3.compareToIgnoreCase("median-std") == 0)
											evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_MINUS_STD;
										else if(s3.compareToIgnoreCase("median-2std") == 0)
											evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_MINUS_2_STD;
										else
										{
											System.err.println("Unknown threshold in parameters. Exiting...");
											System.exit(-1);
										}
										
										boolean foundInListTraining = false;
										Iterator<ExecutionVariablesTraining> itEvTraining = evOtherRecSys.llTraining.iterator();
										while(itEvTraining.hasNext())
										{
											ExecutionVariablesTraining tEvTraining = itEvTraining.next();
											if(tEvTraining.disGenresThreshold == evTraining.disGenresThreshold)
											{
												evTraining = tEvTraining;
												foundInListTraining = true;
												break;
											}
										}
										
										for(String s4 : fourthParameterTokens)
										{
											ExecutionVariablesTesting evTest = new ExecutionVariablesTesting();
											evTest.k_of_kNN_TestingModel = Integer.parseInt(s4);
											
											boolean foundInListTest = false;
											Iterator<ExecutionVariablesTesting> itEvTest = evTraining.llTesting.iterator();
											while(itEvTest.hasNext())
											{
												ExecutionVariablesTesting tEvTest = itEvTest.next();
												if(tEvTest.k_of_kNN_TestingModel == evTest.k_of_kNN_TestingModel)
												{
													evTest = tEvTest;
													foundInListTest = true;
													break;
												}
											}
											
											for(String s5 : fifthParameterTokens)
											{
												ExecutionVariablesSimilarityEstimation tempEvSimEstim = new ExecutionVariablesSimilarityEstimation();
												tempEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation = Double.parseDouble(s5);
												
												Iterator<ExecutionVariablesSimilarityEstimation> itSimEstim = evTest.llSimEstim.iterator();
												while(itSimEstim.hasNext())
												{
													ExecutionVariablesSimilarityEstimation tEvSimEstim = itSimEstim.next();
													if(tEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation == tempEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation)
													{
														tempEvSimEstim = tEvSimEstim;
														break;
													}
												}
												
												for(String s6 : sixthParameterTokens)
												{
													ExecutionVariablesSimilarityEstimation evSimEstim = tempEvSimEstim.clone();
													
													evSimEstim.topK_SimilarityEstimation = Integer.parseInt(s6);
													
													boolean foundInListSimEstim = false;
													itSimEstim = evTest.llSimEstim.iterator();
													while(itSimEstim.hasNext())
													{
														ExecutionVariablesSimilarityEstimation tEvSimEstim = itSimEstim.next();
														if( tEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation == tempEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation)
														{
															if(tEvSimEstim.topK_SimilarityEstimation == tempEvSimEstim.topK_SimilarityEstimation)
															{
																tempEvSimEstim = tEvSimEstim;
																foundInListSimEstim = true;
																break;
															}
														}
													}
													
													if(foundInListSimEstim == false)
														evTest.llSimEstim.add(evSimEstim);
												}
											}
											if(foundInListTest == false)
												evTraining.llTesting.add(evTest);
										}
										if(foundInListTraining == false)
											evOtherRecSys.llTraining.add(evTraining);
									}
									if(foundInListOtherRecommendationSystem == false)
										evGeneral.llOtherRecSys.add(evOtherRecSys);
								}
								if(foundInListGeneral == false)
									llEvg.add(evGeneral);
							}
						}
						else if(mainParametersTokens[0].compareToIgnoreCase("specific") == 0)
						{
							for(int i = 1 ; i < mainParametersTokens.length ; i++)
							{
								String[] mainParametersSingleExecutionTokens = mainParametersTokens[i].split(",");
								
								//
								String generalStr = mainParametersSingleExecutionTokens[0];
								ExecutionVariablesGeneral evGeneral = tempEvGeneral.clone();
								evGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity = Double.parseDouble(generalStr);
								
								boolean foundInListGeneral = false;
								Iterator<ExecutionVariablesGeneral> itEvGeneral = llEvg.iterator();
								while(itEvGeneral.hasNext())
								{
									ExecutionVariablesGeneral tEvGeneral = itEvGeneral.next();
									if(tEvGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity == evGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity)
									{
										evGeneral = tEvGeneral;
										foundInListGeneral = true;
										break;
									}
								}
								
								//
								String otherRecSysStr = mainParametersSingleExecutionTokens[1];
								ExecutionVariablesOtherRecommendationSystem evOtherRecSys = new ExecutionVariablesOtherRecommendationSystem();
								
								if(otherRecSysStr.compareToIgnoreCase("naive-bayes") == 0)
									evOtherRecSys.otherRecSys = ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.NAIVE_BAYES;
								else if(otherRecSysStr.compareToIgnoreCase("slope-one") == 0)
									evOtherRecSys.otherRecSys = ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.SLOPE_ONE;
								else
								{
									System.err.println("Unknown other recommendation system in parameters. Exiting...");
									System.exit(-1);
								}
								
								boolean foundInListOtherRecommendationSystem = false;
								Iterator<ExecutionVariablesOtherRecommendationSystem> itEvOtherRecSys = evGeneral.llOtherRecSys.iterator();
								while(itEvOtherRecSys.hasNext())
								{
									ExecutionVariablesOtherRecommendationSystem tEvOtherRecSys = itEvOtherRecSys.next();
									if(tEvOtherRecSys.otherRecSys == evOtherRecSys.otherRecSys)
									{
										evOtherRecSys = tEvOtherRecSys;
										foundInListOtherRecommendationSystem = true;
										break;
									}
								}
								
								//
								String trainingStr = mainParametersSingleExecutionTokens[1];
								ExecutionVariablesTraining evTraining = new ExecutionVariablesTraining();
								
								if(trainingStr.compareToIgnoreCase("zero") == 0)
									evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.ZERO;
								else if(trainingStr.compareToIgnoreCase("median") == 0)
									evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN;
								else if(trainingStr.compareToIgnoreCase("median+std") == 0)
									evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_PLUS_STD;
								else if(trainingStr.compareToIgnoreCase("median+2std") == 0)
									evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_PLUS_2_STD;
								else if(trainingStr.compareToIgnoreCase("median-std") == 0)
									evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_MINUS_STD;
								else if(trainingStr.compareToIgnoreCase("median-2std") == 0)
									evTraining.disGenresThreshold = ExecutionVariablesTraining.DisGenresThresholdValues.MEDIAN_MINUS_2_STD;
								else
								{
									System.err.println("Unknown threshold in parameters. Exiting...");
									System.exit(-1);
								}
								
								boolean foundInListTraining = false;
								Iterator<ExecutionVariablesTraining> itEvTraining = evOtherRecSys.llTraining.iterator();
								while(itEvTraining.hasNext())
								{
									ExecutionVariablesTraining tEvTraining = itEvTraining.next();
									if(tEvTraining.disGenresThreshold == evTraining.disGenresThreshold)
									{
										evTraining = tEvTraining;
										foundInListTraining = true;
										break;
									}
								}
								
								//
								String testStr = mainParametersSingleExecutionTokens[2];
								
								ExecutionVariablesTesting evTest = new ExecutionVariablesTesting();
								evTest.k_of_kNN_TestingModel = Integer.parseInt(testStr);
								
								boolean foundInListTest = false;
								Iterator<ExecutionVariablesTesting> itEvTest = evTraining.llTesting.iterator();
								while(itEvTest.hasNext())
								{
									ExecutionVariablesTesting tEvTest = itEvTest.next();
									if(tEvTest.k_of_kNN_TestingModel == evTest.k_of_kNN_TestingModel)
									{
										evTest = tEvTest;
										foundInListTest = true;
										break;
									}
								}
								
								//
								String simEstimFmeasureStr = mainParametersSingleExecutionTokens[3];
								
								ExecutionVariablesSimilarityEstimation evSimEstim = new ExecutionVariablesSimilarityEstimation();
								evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation = Double.parseDouble(simEstimFmeasureStr);
								
								Iterator<ExecutionVariablesSimilarityEstimation> itSimEstim = evTest.llSimEstim.iterator();
								while(itSimEstim.hasNext())
								{
									ExecutionVariablesSimilarityEstimation tEvSimEstim = itSimEstim.next();
									if(tEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation == evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation)
									{
										evSimEstim = tEvSimEstim;
										break;
									}
								}
								
								//
								String simEstimTopKStr = mainParametersSingleExecutionTokens[4];
								
								evSimEstim.topK_SimilarityEstimation = Integer.parseInt(simEstimTopKStr);
								
								boolean foundInListSimEstim = false;
								itSimEstim = evTest.llSimEstim.iterator();
								while(itSimEstim.hasNext())
								{
									ExecutionVariablesSimilarityEstimation tEvSimEstim = itSimEstim.next();
									if( tEvSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation == evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation)
									{
										if(tEvSimEstim.topK_SimilarityEstimation == evSimEstim.topK_SimilarityEstimation)
										{
											evSimEstim = tEvSimEstim;
											foundInListSimEstim = true;
											break;
										}
									}
								}
								
								//
								if(foundInListSimEstim == false)
									evTest.llSimEstim.add(evSimEstim);
								
								if(foundInListTest == false)
									evTraining.llTesting.add(evTest);
								
								if(foundInListTraining == false)
									evOtherRecSys.llTraining.add(evTraining);
								
								if(foundInListOtherRecommendationSystem == false)
									evGeneral.llOtherRecSys.add(evOtherRecSys);
								
								if(foundInListGeneral == false)
									llEvg.add(evGeneral);
							}
						}
					}
				}
			}
			//Close the input stream
			in.close();
			
			File tempLocalParametersFile = new File(outputPath);
			tempLocalParametersFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return refreshStatistics;
	}
}
