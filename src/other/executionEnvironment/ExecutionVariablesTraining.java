package other.executionEnvironment;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * 
 * ExecutionVariablesTraining.java
 * 
 * This class provides an easy and structured way of giving access
 * to parameters that are provided in "ExecutionParameters.txt".
 * 
 * We also maintain other fields that help other Classes during the execution,
 * and there are also fields for measuring execution time and specific file sizes
 * (files that must be loaded in the RAM, such as MapFile's index).
 * 
 * Objects from this class and the other ExecutionVariablesX classes, are saved
 * and can be loaded again for future statistics and refreshing of statistics
 * by our program.
 * 
 * @author John Koumarelas
 * @see ExecutionVariablesGeneral, ExecutionVariablesOtherRecommendationSystem, 
 * 		ExecutionVariablesTesting, ExecutionVariablesSimilarityEstimation
 */
public class ExecutionVariablesTraining implements Serializable{

	private static final long serialVersionUID = 5547436332576866251L;

	// Execution Variables - Test
	public LinkedList<ExecutionVariablesTesting> llTesting = new LinkedList<ExecutionVariablesTesting>();
	
	// Execution Parameters-Values
	public DisGenresThresholdValues disGenresThreshold = null; 
	public enum DisGenresThresholdValues {
		ZERO,
		MEDIAN,
		MEDIAN_PLUS_STD,
		MEDIAN_PLUS_2_STD,
		MEDIAN_MINUS_STD,
		MEDIAN_MINUS_2_STD;
		
		public String toString(){
			if(this == ZERO)
				return "ZERO";
			else if(this == MEDIAN)
				return "MEDIAN";
			else if(this == MEDIAN_PLUS_STD)
				return "MEDIAN_PLUS_STD";
			else if(this == MEDIAN_PLUS_2_STD)
				return "MEDIAN_PLUS_2_STD";
			else if(this == MEDIAN_MINUS_STD)
				return "MEDIAN_MINUS_STD";
			else if(this == MEDIAN_MINUS_2_STD)
				return "MEDIAN_MINUS_2_STD";
			else
				return "NULL_THRESHOLD";
		}
		
		public double getValue(ExecutionVariablesGeneral evGeneral){
			if(this == ZERO)
				return 0.0;
			else if(this == MEDIAN)
				return evGeneral.medianOfDMatrix;
			else if(this == MEDIAN_PLUS_STD)
				return evGeneral.medianOfDMatrix + evGeneral.stdOfDMatrix;
			else if(this == MEDIAN_PLUS_2_STD)
				return evGeneral.medianOfDMatrix + 2*evGeneral.stdOfDMatrix;
			else if(this == MEDIAN_MINUS_STD)
				return evGeneral.medianOfDMatrix - evGeneral.stdOfDMatrix;
			else if(this == MEDIAN_MINUS_2_STD)
				return evGeneral.medianOfDMatrix - 2*evGeneral.stdOfDMatrix;
			else
				return Double.MAX_VALUE;
		}
	}
	
	// Execution Times
	public long time_TrainingModelDriver = -1L;
	public long time_Test_TrainingModelDriver = -1L;
	
	// File Sizes
	public TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TrainingModel_entries = null;
	public TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TrainingModel_sizeInBytes = null;
	
	public String toStringStatistics(ExecutionVariablesGeneral evg, int numTabs){
		String str = "";
		
		String tabsStr = ExecutionVariablesGeneral.retNumTabs(numTabs);
		
		str += tabsStr + "Execution Parametes-Values" + "\r\n\r\n";
		
		str += tabsStr + "disGenresThreshold: " + disGenresThreshold.toString() + ": " + disGenresThreshold.getValue(evg) + "\r\n\r\n";
		
		str += tabsStr + "Execution Times: ns: nanoseconds, s: seconds" + "\r\n\r\n";
		
		str += tabsStr + "TrainingModelDriver: " + time_TrainingModelDriver + "ns\t" + time_TrainingModelDriver / 1000000000.0 + "s"  + "\r\n";
		str += tabsStr + "Test_TrainingModelDriver: " + time_Test_TrainingModelDriver + "ns\t" + time_Test_TrainingModelDriver / 1000000000.0 + "s" + "\r\n";
		
		str += tabsStr + "File Size info (size in Bytes)" + "\r\n\r\n";
		
		Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatEntries = file_MapFile_TrainingModel_entries.entrySet().iterator();
		Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatSize = file_MapFile_TrainingModel_sizeInBytes.entrySet().iterator();
		while(itTmCatEntries.hasNext() && itTmCatSize.hasNext())
		{
			Entry<Integer, TreeMap<Integer, Long>> entryTmCatEntries = itTmCatEntries.next();
			Entry<Integer, TreeMap<Integer, Long>> entryTmCatSize = itTmCatSize.next();
			
			Integer categoryID = entryTmCatEntries.getKey();
			
			Iterator<Entry<Integer, Long>> itTmCatFilePartEntries = entryTmCatEntries.getValue().entrySet().iterator();
			Iterator<Entry<Integer, Long>> itTmCatFilePartSize = entryTmCatSize.getValue().entrySet().iterator();
			while(itTmCatFilePartEntries.hasNext() && itTmCatFilePartSize.hasNext())
			{
				Entry<Integer, Long> entryTmCatFilePartEntries = itTmCatFilePartEntries.next();
				Entry<Integer, Long> entryTmCatFilePartSize = itTmCatFilePartSize.next();
				
				Integer filePart = entryTmCatFilePartEntries.getKey();
				
				Long entries = entryTmCatFilePartEntries.getValue();
				Long size = entryTmCatFilePartSize.getValue();
				
				str += tabsStr + "category: " + categoryID + " filePart: " + filePart + " entries: " + entries + " fileSize: " + size + "\r\n";
			}
		}
		
		return str;
	}
	
	public String toStringSize(int numTabs)
	{
		String str = "";
		
		String tabsStr = ExecutionVariablesGeneral.retNumTabs(numTabs);
		
		Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatEntries = file_MapFile_TrainingModel_entries.entrySet().iterator();
		Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatSize = file_MapFile_TrainingModel_sizeInBytes.entrySet().iterator();
		while(itTmCatEntries.hasNext() && itTmCatSize.hasNext())
		{
			Entry<Integer, TreeMap<Integer, Long>> entryTmCatEntries = itTmCatEntries.next();
			Entry<Integer, TreeMap<Integer, Long>> entryTmCatSize = itTmCatSize.next();
			
			Integer categoryID = entryTmCatEntries.getKey();
			
			Iterator<Entry<Integer, Long>> itTmCatFilePartEntries = entryTmCatEntries.getValue().entrySet().iterator();
			Iterator<Entry<Integer, Long>> itTmCatFilePartSize = entryTmCatSize.getValue().entrySet().iterator();
			while(itTmCatFilePartEntries.hasNext() && itTmCatFilePartSize.hasNext())
			{
				Entry<Integer, Long> entryTmCatFilePartEntries = itTmCatFilePartEntries.next();
				Entry<Integer, Long> entryTmCatFilePartSize = itTmCatFilePartSize.next();
				
				Integer filePart = entryTmCatFilePartEntries.getKey();
				
				Long entries = entryTmCatFilePartEntries.getValue();
				Long size = entryTmCatFilePartSize.getValue();
				
				str += tabsStr + "category: " + categoryID + " filePart: " + filePart + " entries: " + entries + " fileSize: " + size + "\r\n";
			}
		}
		
		return str;
	}
}
