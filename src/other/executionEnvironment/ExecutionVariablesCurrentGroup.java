package other.executionEnvironment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * 
 * This class helps the total execution of our program, by providing
 * an easy way of knowing which are the current parameters in a particular
 * time. See Main's run() code for better understanding the use of this class.
 * 
 * @author John Koumarelas
 *
 */
public class ExecutionVariablesCurrentGroup implements Serializable{

	private static final long serialVersionUID = 2254928457223574848L;
	
	public ExecutionVariablesGeneral evGeneral = null;
	public ExecutionVariablesOtherRecommendationSystem evOtherRecSys = null;
	public ExecutionVariablesTraining evTraining = null;
	public ExecutionVariablesTesting evTesting = null;
	public ExecutionVariablesSimilarityEstimation evSimEstim = null;
	
	public boolean hasChangedGeneral = false;
	public boolean hasChangedOtherRecommendationSystem = false;
	public boolean hasChangedTraining = false;
	public boolean hasChangedTesting = false;
	public boolean hasChangedSimilarityEstimation = false;
	
	public String getInputPathDatasetAndPercentages(){
		String pathToReturn = null;
		
		if(this.evGeneral.usersNumber > -1L)
			pathToReturn = this.evGeneral.inputPath + File.separator + "Splitted" + File.separator + this.evGeneral.datasetName + "_" + "Users-" + this.evGeneral.usersNumber + "_" + "Training-" + evGeneral.trainingPercentage*100 + "_" + "Testing-" + this.evGeneral.testingPercentage*100; 
		else
			pathToReturn = this.evGeneral.inputPath + File.separator + "Splitted" + File.separator + this.evGeneral.datasetName + "_" + "UsersPercentage-" + this.evGeneral.usersPercentage*100 + "_" + "Training-" + evGeneral.trainingPercentage*100 + "_" + "Testing-" + this.evGeneral.testingPercentage*100; 

		return pathToReturn;
	}
	
	public String getOutputPathDatasetAndPercentages(){
		String pathToReturn = null;
		
		if(this.evGeneral.usersNumber > -1L)
			pathToReturn = this.evGeneral.outputPath + File.separator + this.evGeneral.datasetName + "_" + "Users-" + this.evGeneral.usersNumber + "_" +"Training-" + evGeneral.trainingPercentage*100 + "_" + "Testing-" + this.evGeneral.testingPercentage*100; 
		else
			pathToReturn = this.evGeneral.outputPath + File.separator + this.evGeneral.datasetName + "_" + "UsersPercentage-" + this.evGeneral.usersPercentage*100 + "_" + "Training-" + evGeneral.trainingPercentage*100 + "_" + "Testing-" + this.evGeneral.testingPercentage*100; 

		return pathToReturn;
	}
	
	public String getOutputPathGeneral(){
		String pathToReturn = getOutputPathDatasetAndPercentages() + File.separator + "General_" + this.evGeneral.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
		return pathToReturn;
	}
	
	public String getOutputPathOtherRecommendationSystem(){
		String pathToReturn = getOutputPathGeneral() + File.separator + "OtherRecommendationSystem_" + this.evOtherRecSys.toString();
		return pathToReturn;
	}
	
	public String getOutputPathTraining(){
		String pathToReturn = getOutputPathOtherRecommendationSystem() + File.separator + "Training_" + this.evTraining.disGenresThreshold.toString();
		return pathToReturn;
	}
	
	public String getOutputPathTesting(){
		String pathToReturn = getOutputPathTraining() + File.separator + "Testing_" + this.evTesting.k_of_kNN_TestingModel;
		return pathToReturn;
	}
	
	public String getOutputPathSimilarityEstimation(){
		String pathToReturn = getOutputPathTesting() + File.separator + "SimilarityEstimation_" + this.evSimEstim.topK_SimilarityEstimation + "_" + this.evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation;
		return pathToReturn;
	}
	
	public static ExecutionVariablesGeneral getStatistics_General(Path fromFSFilePath){
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			String outputPath = "./Statistics.bin";
			Path toLocalFilePath = new Path(outputPath);
			
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			fs.copyToLocalFile(fromFSFilePath, toLocalFilePath);
			
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./Statistics.bin"));
			ExecutionVariablesGeneral tempEvGeneral = (ExecutionVariablesGeneral) ois.readObject();
			ois.close();
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			return tempEvGeneral;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static ExecutionVariablesOtherRecommendationSystem getStatistics_OtherRecommendationSystem(Path fromFSFilePath){
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			String outputPath = "./Statistics.bin";
			Path toLocalFilePath = new Path(outputPath);
			
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			fs.copyToLocalFile(fromFSFilePath, toLocalFilePath);
			
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./Statistics.bin"));
			ExecutionVariablesOtherRecommendationSystem tempEvOtherRecSys = (ExecutionVariablesOtherRecommendationSystem) ois.readObject();
			ois.close();
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			return tempEvOtherRecSys;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ExecutionVariablesTraining getStatistics_Training(Path fromFSFilePath){
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			String outputPath = "./Statistics.bin";
			Path toLocalFilePath = new Path(outputPath);
			
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			fs.copyToLocalFile(fromFSFilePath, toLocalFilePath);
			
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./Statistics.bin"));
			ExecutionVariablesTraining tempEvTraining = (ExecutionVariablesTraining) ois.readObject();
			ois.close();
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			return tempEvTraining;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ExecutionVariablesTesting getStatistics_Testing(Path fromFSFilePath){
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			String outputPath = "./Statistics.bin";
			Path toLocalFilePath = new Path(outputPath);
			
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			fs.copyToLocalFile(fromFSFilePath, toLocalFilePath);
			
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./Statistics.bin"));
			ExecutionVariablesTesting tempEvTesting = (ExecutionVariablesTesting) ois.readObject();
			ois.close();
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			return tempEvTesting;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ExecutionVariablesSimilarityEstimation getStatistics_SimilarityEstimation(Path fromFSFilePath){
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			String outputPath = "./Statistics.bin";
			Path toLocalFilePath = new Path(outputPath);
			
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			fs.copyToLocalFile(fromFSFilePath, toLocalFilePath);
			
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./Statistics.bin"));
			ExecutionVariablesSimilarityEstimation tempEvSimEstim = (ExecutionVariablesSimilarityEstimation) ois.readObject();
			ois.close();
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			return tempEvSimEstim;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void loadStatistics_General(Path fromFSFilePath){
		evGeneral  = getStatistics_General(fromFSFilePath);
	}
	
	public void loadStatistics_OtherRecommendationSystem(Path fromFSFilePath){
		evOtherRecSys = getStatistics_OtherRecommendationSystem(fromFSFilePath);
	}
	
	public void loadStatistics_Training(Path fromFSFilePath){
		evTraining = getStatistics_Training(fromFSFilePath);
	}
	
	public void loadStatistics_Testing(Path fromFSFilePath){
		evTesting = getStatistics_Testing(fromFSFilePath);
	}
	
	public void loadStatistics_SimilarityEstimation(Path fromFSFilePath){
		evSimEstim = getStatistics_SimilarityEstimation(fromFSFilePath);
	}
	
	public void saveStatistics_General(){
		try {
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./Statistics.bin"));
			oos.writeObject(evGeneral);
			oos.flush();
			oos.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			Path fromLocalFilePath = new Path("./Statistics.bin");
			String outputPath = getOutputPathGeneral() + File.separator + "Statistics.bin";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, false);
			Path tsFSFilePathCrc = new Path(getOutputPathGeneral() + File.separator + "." + "Statistics.bin" + ".crc");
			fs.delete(tsFSFilePathCrc, false);
			
			fs.copyFromLocalFile(true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveStatistics_OtherRecommendationSystem(){
		try {
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./Statistics.bin"));
			oos.writeObject(evOtherRecSys);
			oos.flush();
			oos.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			Path fromLocalFilePath = new Path("./Statistics.bin");
			String outputPath = getOutputPathOtherRecommendationSystem() + File.separator + "Statistics.bin";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathOtherRecommendationSystem() + File.separator + "." + "Statistics.bin" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveStatistics_Training(){
		try {
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./Statistics.bin"));
			oos.writeObject(evTraining);
			oos.flush();
			oos.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			Path fromLocalFilePath = new Path("./Statistics.bin");
			String outputPath = getOutputPathTraining() + File.separator + "Statistics.bin";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathTraining() + File.separator + "." + "Statistics.bin" + ".crc");
			fs.delete(tsFSFilePathCrc, true);

			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveStatistics_Testing(){
		try {
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./Statistics.bin"));
			oos.writeObject(evTesting);
			oos.flush();
			oos.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			Path fromLocalFilePath = new Path("./Statistics.bin");
			String outputPath = getOutputPathTesting() + File.separator + "Statistics.bin";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathTesting() + File.separator + "." + "Statistics.bin" + ".crc");
			fs.delete(tsFSFilePathCrc, true);

			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveStatistics_SimilarityEstimation(){
		try {
			File tempFile = new File("./Statistics.bin");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.bin.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileOutputStream fis = new FileOutputStream("./Statistics.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fis);
			oos.writeObject(evSimEstim);
			oos.flush();
			oos.close();

			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			
			Path fromLocalFilePath = new Path("./Statistics.bin");
			String outputPath = getOutputPathSimilarityEstimation() + File.separator + "Statistics.bin";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathSimilarityEstimation() + File.separator + "." + "Statistics.bin" + ".crc");
			fs.delete(tsFSFilePathCrc, true);

			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void exportStatistics_General(){
		try {
			File tempFile = new File("./Statistics.txt");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.txt.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./Statistics.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(this.evGeneral.toStringStatistics(0));
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./Statistics.txt");
			String outputPath = getOutputPathGeneral() + File.separator + "Statistics.txt";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathGeneral() + File.separator + "." + "Statistics.txt" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportStatistics_OtherRecommendationSystem(){
		try {
			File tempFile = new File("./Statistics.txt");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.txt.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./Statistics.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(this.evOtherRecSys.toStringStatistics(0));
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./Statistics.txt");
			String outputPath = getOutputPathGeneral() + File.separator + "Statistics.txt";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathGeneral() + File.separator + "." + "Statistics.txt" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void exportStatistics_Training(){
		try {
			File tempFile = new File("./Statistics.txt");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.txt.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./Statistics.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(this.evTraining.toStringStatistics(evGeneral,0));
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./Statistics.txt");
			String outputPath = getOutputPathTraining() + File.separator + "Statistics.txt";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathTraining() + File.separator + "." + "Statistics.txt" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void exportStatistics_Testing(){
		try {
			File tempFile = new File("./Statistics.txt");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.txt.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./Statistics.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(this.evTesting.toStringStatistics(0));
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./Statistics.txt");
			String outputPath = getOutputPathTesting() + File.separator + "Statistics.txt";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathTesting() + File.separator + "." + "Statistics.txt" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void exportStatistics_SimilarityEstimation(){
		try {
			File tempFile = new File("./Statistics.txt");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Statistics.txt.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./Statistics.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(this.evSimEstim.toStringStatistics(0));
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./Statistics.txt");
			String outputPath = getOutputPathSimilarityEstimation() + File.separator + "Statistics.txt";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathSimilarityEstimation() + File.separator + "." + "Statistics.txt" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void exportStatistics_Total_RMSE_DCG(){
		try {
			File tempFile = new File("./Total_RMSE_DCG.txt");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./.Total_RMSE_DCG.txt.crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./Total_RMSE_DCG.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(this.evGeneral.toStringTotal_RMSE_DCG(false));
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./Total_RMSE_DCG.txt");
			String outputPath = getOutputPathGeneral() + File.separator + "Total_RMSE_DCG.txt";
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path(getOutputPathGeneral() + File.separator + "." + "Total_RMSE_DCG.txt" + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
			
			if(tempFile.exists())
				tempFile.delete();
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void fillMissingValues_General() throws IOException
	{
		String statisticsFSPathStr = getOutputPathGeneral() + File.separator + "Statistics.bin";
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		
		if(!fs.exists(new Path(statisticsFSPathStr)))
			return;
		
		ExecutionVariablesGeneral tEvGeneral = (ExecutionVariablesGeneral)getStatistics_General(new Path(statisticsFSPathStr));
		
		if(tEvGeneral == null) return;
		
		if(evGeneral.time_CalculateTotalAverageDriver == -1L)
			evGeneral.time_CalculateTotalAverageDriver = tEvGeneral.time_CalculateTotalAverageDriver;
		if(evGeneral.time_CategoriesCommonUsersDriver == -1L)
			evGeneral.time_CategoriesCommonUsersDriver = tEvGeneral.time_CategoriesCommonUsersDriver;
		if(evGeneral.time_CategoryPipelineDriver == -1L)
			evGeneral.time_CategoryPipelineDriver = tEvGeneral.time_CategoryPipelineDriver;
		if(evGeneral.time_DMatrixDriver == -1L)
			evGeneral.time_DMatrixDriver = tEvGeneral.time_DMatrixDriver;
		if(evGeneral.time_Test_CategoriesCommonUsersDriver == -1L)
			evGeneral.time_Test_CategoriesCommonUsersDriver = tEvGeneral.time_Test_CategoriesCommonUsersDriver;
		if(evGeneral.time_Test_CategoryPipelineDriver == -1L)
			evGeneral.time_Test_CategoryPipelineDriver = tEvGeneral.time_Test_CategoryPipelineDriver;
		if(evGeneral.time_Test_DMatrixDriver == -1L)
			evGeneral.time_Test_DMatrixDriver = tEvGeneral.time_Test_DMatrixDriver;
		if(evGeneral.time_Test_UserInterestDriver == -1L)
			evGeneral.time_Test_UserInterestDriver = tEvGeneral.time_Test_UserInterestDriver;
		if(evGeneral.time_Test_UserPipelineDriver == -1L)
			evGeneral.time_Test_UserPipelineDriver = tEvGeneral.time_Test_UserPipelineDriver;
		if(evGeneral.time_UserInterestOnCategoryDriver == -1L)
			evGeneral.time_UserInterestOnCategoryDriver = tEvGeneral.time_UserInterestOnCategoryDriver;
		if(evGeneral.time_UserPipelineDriver == -1L)
			evGeneral.time_UserPipelineDriver = tEvGeneral.time_UserPipelineDriver;
		
		if(evGeneral.file_MapFile_UserPreferences_keys == -1L)
			evGeneral.file_MapFile_UserPreferences_keys = tEvGeneral.file_MapFile_UserPreferences_keys;
		if(evGeneral.file_MapFile_UserPreferences_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_UserPreferences_index_sizeInBytes = tEvGeneral.file_MapFile_UserPreferences_index_sizeInBytes;
		if(evGeneral.file_MapFile_UserPreferencesOnACategory_keys == -1L)
			evGeneral.file_MapFile_UserPreferencesOnACategory_keys = tEvGeneral.file_MapFile_UserPreferencesOnACategory_keys;
		if(evGeneral.file_MapFile_UserPreferencesOnACategory_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_UserPreferencesOnACategory_index_sizeInBytes = tEvGeneral.file_MapFile_UserPreferencesOnACategory_index_sizeInBytes;
		if(evGeneral.file_MapFile_UserDisposition_keys == -1L)
			evGeneral.file_MapFile_UserDisposition_keys = tEvGeneral.file_MapFile_UserDisposition_keys;
		if(evGeneral.file_MapFile_UserDisposition_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_UserDisposition_index_sizeInBytes = tEvGeneral.file_MapFile_UserDisposition_index_sizeInBytes;
		if(evGeneral.file_MapFile_UserAverageValueOnACategory_keys == -1L)
			evGeneral.file_MapFile_UserAverageValueOnACategory_keys = tEvGeneral.file_MapFile_UserAverageValueOnACategory_keys;
		if(evGeneral.file_MapFile_UserAverageValueOnACategory_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_UserAverageValueOnACategory_index_sizeInBytes = tEvGeneral.file_MapFile_UserAverageValueOnACategory_index_sizeInBytes;
		if(evGeneral.file_MapFile_CategorySetOfUsers_keys == -1L)
			evGeneral.file_MapFile_CategorySetOfUsers_keys = tEvGeneral.file_MapFile_CategorySetOfUsers_keys;
		if(evGeneral.file_MapFile_CategorySetOfUsers_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_CategorySetOfUsers_index_sizeInBytes = tEvGeneral.file_MapFile_CategorySetOfUsers_index_sizeInBytes;
		if(evGeneral.file_MapFile_CategoriesCommonUsers_keys == -1L)
			evGeneral.file_MapFile_CategoriesCommonUsers_keys = tEvGeneral.file_MapFile_CategoriesCommonUsers_keys;
		if(evGeneral.file_MapFile_CategoriesCommonUsers_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_CategoriesCommonUsers_index_sizeInBytes = tEvGeneral.file_MapFile_CategoriesCommonUsers_index_sizeInBytes;
		if(evGeneral.keys_MapFile_UserInterestOnCategory == -1L)
			evGeneral.keys_MapFile_UserInterestOnCategory = tEvGeneral.keys_MapFile_UserInterestOnCategory;
		if(evGeneral.index_sizeInBytes_MapFile_UserInterestOnCategory == -1L)
			evGeneral.index_sizeInBytes_MapFile_UserInterestOnCategory = tEvGeneral.index_sizeInBytes_MapFile_UserInterestOnCategory;
		if(evGeneral.file_MapFile_DMatrix_keys == -1L)
			evGeneral.file_MapFile_DMatrix_keys = tEvGeneral.file_MapFile_DMatrix_keys;
		if(evGeneral.file_MapFile_DMatrix_index_sizeInBytes == -1L)
			evGeneral.file_MapFile_DMatrix_index_sizeInBytes = tEvGeneral.file_MapFile_DMatrix_index_sizeInBytes;
		
		if(evGeneral.medianOfDMatrix == -1L)
			evGeneral.medianOfDMatrix = tEvGeneral.medianOfDMatrix;
		if(evGeneral.maxOfDMatrix == -1L)
			evGeneral.maxOfDMatrix = tEvGeneral.maxOfDMatrix;
		if(evGeneral.stdOfDMatrix == -1L)
			evGeneral.stdOfDMatrix = tEvGeneral.stdOfDMatrix;
	}
	
	public void fillMissingValues_OtherRecommendationSystem() throws IOException
	{
		String statisticsFSPathStr = getOutputPathOtherRecommendationSystem() + File.separator + "Statistics.bin";
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		
		if(!fs.exists(new Path(statisticsFSPathStr)))
			return;
		
		ExecutionVariablesOtherRecommendationSystem tEvOtherRecSys = (ExecutionVariablesOtherRecommendationSystem)getStatistics_OtherRecommendationSystem(new Path(statisticsFSPathStr));
		
		if(evOtherRecSys.time_NormalizeRatings == -1L)
			evOtherRecSys.time_NormalizeRatings = tEvOtherRecSys.time_NormalizeRatings;
		if(evOtherRecSys.time_OtherRecommendationSystem == -1L)
			evOtherRecSys.time_OtherRecommendationSystem = tEvOtherRecSys.time_OtherRecommendationSystem;
		if(evOtherRecSys.time_MergeOtherRecSysWithOur == -1L)
			evOtherRecSys.time_MergeOtherRecSysWithOur = tEvOtherRecSys.time_MergeOtherRecSysWithOur;
	}
	
	public void fillMissingValues_Training() throws IOException
	{
		String statisticsFSPathStr = getOutputPathTraining() + File.separator + "Statistics.bin";
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		
		if(!fs.exists(new Path(statisticsFSPathStr)))
			return;
		
		ExecutionVariablesTraining tEvTraining = (ExecutionVariablesTraining)getStatistics_Training(new Path(statisticsFSPathStr));
		
		if(evTraining.time_TrainingModelDriver == -1L)
			evTraining.time_TrainingModelDriver = tEvTraining.time_TrainingModelDriver;
		if(evTraining.time_Test_TrainingModelDriver == -1L)
			evTraining.time_Test_TrainingModelDriver = tEvTraining.time_TrainingModelDriver;
		
		if(evTraining.file_MapFile_TrainingModel_entries == null)
			evTraining.file_MapFile_TrainingModel_entries = tEvTraining.file_MapFile_TrainingModel_entries;
		if(evTraining.file_MapFile_TrainingModel_sizeInBytes == null)
			evTraining.file_MapFile_TrainingModel_sizeInBytes = tEvTraining.file_MapFile_TrainingModel_sizeInBytes;
	}
	
	public void fillMissingValues_Testing() throws IOException
	{
		String statisticsFSPathStr = getOutputPathTesting() + File.separator + "Statistics.bin";
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		
		if(!fs.exists(new Path(statisticsFSPathStr)))
			return;
		
		ExecutionVariablesTesting tEvTesting = (ExecutionVariablesTesting)getStatistics_Testing(new Path(statisticsFSPathStr));
		
		if(evTesting.time_ExportTestingModel == -1L)
			evTesting.time_ExportTestingModel = tEvTesting.time_ExportTestingModel;
		if(evTesting.time_TestingModelPhase1Driver == -1L)
			evTesting.time_TestingModelPhase1Driver = tEvTesting.time_TestingModelPhase1Driver;
		if(evTesting.time_TestingModelPhase2Driver == -1L)
			evTesting.time_TestingModelPhase2Driver = tEvTesting.time_TestingModelPhase2Driver;
		
		if(evTesting.file_MapFile_TestingModel1_keys == null)
			evTesting.file_MapFile_TestingModel1_keys = tEvTesting.file_MapFile_TestingModel1_keys;
		if(evTesting.file_MapFile_TestingModel1_index_sizeInBytes == null)
			evTesting.file_MapFile_TestingModel1_index_sizeInBytes = tEvTesting.file_MapFile_TestingModel1_index_sizeInBytes;
		
		if(evTesting.file_MapFile_TestingModel2_keys == -1L)
			evTesting.file_MapFile_TestingModel2_keys = tEvTesting.file_MapFile_TestingModel2_keys;
		if(evTesting.file_MapFile_TestingModel2_index_sizeInBytes == -1L)
			evTesting.file_MapFile_TestingModel2_index_sizeInBytes = tEvTesting.file_MapFile_TestingModel2_index_sizeInBytes;
	}
	
	public void fillMissingValues_SimilarityEstimation() throws IOException
	{
		String statisticsFSPathStr = getOutputPathSimilarityEstimation() + File.separator + "Statistics.bin";
		
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		
		if(!fs.exists(new Path(statisticsFSPathStr)))
			return;
		
		ExecutionVariablesSimilarityEstimation tEvSimEstim = (ExecutionVariablesSimilarityEstimation)getStatistics_SimilarityEstimation(new Path(statisticsFSPathStr));
		
		if(evSimEstim.time_SimilarityEstimation == -1L)
			evSimEstim.time_SimilarityEstimation = tEvSimEstim.time_SimilarityEstimation;
		
		if(evSimEstim.RMSE_OtherRecommendationSystem == -1L)
			evSimEstim.RMSE_OtherRecommendationSystem = tEvSimEstim.RMSE_OtherRecommendationSystem;
		if(evSimEstim.RMSE_OurRecommendationSystem == -1L)
			evSimEstim.RMSE_OurRecommendationSystem = tEvSimEstim.RMSE_OurRecommendationSystem;
		
		if(evSimEstim.nDCG_OtherRecommendationSystem == -1L)
			evSimEstim.nDCG_OtherRecommendationSystem = tEvSimEstim.nDCG_OtherRecommendationSystem;
		if(evSimEstim.nDCG_OurRecommendationSystem == -1L)
			evSimEstim.nDCG_OurRecommendationSystem = tEvSimEstim.nDCG_OurRecommendationSystem;
	}
	
}
