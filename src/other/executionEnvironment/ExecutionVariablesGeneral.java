package other.executionEnvironment;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * 
 * ExecutionVariablesGeneral.java
 * 
 * This class provides an easy and structured way of giving access
 * to parameters that are provided in "ExecutionParameters.txt".
 * 
 * We also maintain other fields that help other Classes during the execution,
 * and there are also fields for measuring execution time and specific file sizes
 * (files that must be loaded in the RAM, such as MapFile's index).
 * 
 * Objects from this class and the other ExecutionVariablesX classes, are saved
 * and can be loaded again for future statistics and refreshing of statistics
 * by our program.
 * 
 * @author John Koumarelas
 * @see ExecutionVariablesOtherRecommendationSystem, ExecutionVariablesTraining, 
 * 		ExecutionVariablesTesting, ExecutionVariablesSimilarityEstimation
 */
public class ExecutionVariablesGeneral implements Serializable{

	private static final long serialVersionUID = -9212785494824062970L;
	
	// Execution Parameters
	public String inputPath = null;
	public String outputPath = null;
	public int numberOfReduceTasks = -1;
	public boolean showStartStopPrints = false;
	public boolean deleteTempFilesAfterExecution = false;
	public boolean clearWholeOutputFolderBeforeStart = false;
	public boolean askBeforeOverwritingΕxistingFolders = false;
	public boolean onFalseAskBeforeOverwriting_KeepExistingFolders = false;
	public double b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity = 0.0;
	public String datasetName = null;
	public double usersPercentage = -1.0;
	public long usersNumber = -1L;
	public double trainingPercentage = -1.0;
	public double testingPercentage = -1.0;
	
	public TreeSet<Integer> categoriesList = null;
	
	// Execution Variables - Other Recommendation System
	public LinkedList<ExecutionVariablesOtherRecommendationSystem> llOtherRecSys = new LinkedList<ExecutionVariablesOtherRecommendationSystem>();
	
	// Execution Values
	public double medianOfDMatrix = -1.0;
	public double maxOfDMatrix = -1.0;
	public double stdOfDMatrix = -1.0;
	
	// Execution Times
	public long time_CalculateTotalAverageDriver = -1L;
	public long time_UserPipelineDriver = -1L;
	public long time_Test_UserPipelineDriver = -1L;
	public long time_CategoryPipelineDriver = -1L;
	public long time_Test_CategoryPipelineDriver = -1L;
	public long time_CategoriesCommonUsersDriver = -1L;
	public long time_Test_CategoriesCommonUsersDriver = -1L;
	public long time_UserInterestOnCategoryDriver = -1L;
	public long time_Test_UserInterestDriver = -1L;
	public long time_DMatrixDriver = -1L;
	public long time_Test_DMatrixDriver = -1L;
	public long time_TotalSummedExecutionTime = -1L;
	public long time_TotalExecutionTime = -1L;
	
	// File Sizes
	public long file_MapFile_UserPreferences_keys = -1L;
	public long file_MapFile_UserPreferences_index_sizeInBytes = -1L;
	public long file_MapFile_UserPreferencesOnACategory_keys = -1L;
	public long file_MapFile_UserPreferencesOnACategory_index_sizeInBytes = -1L;
	public long file_MapFile_UserDisposition_keys = -1L;
	public long file_MapFile_UserDisposition_index_sizeInBytes = -1L;
	public long file_MapFile_UserAverageValueOnACategory_keys = -1L;
	public long file_MapFile_UserAverageValueOnACategory_index_sizeInBytes = -1L;
	public long file_MapFile_CategorySetOfUsers_keys = -1L;
	public long file_MapFile_CategorySetOfUsers_index_sizeInBytes = -1L;
	public long file_MapFile_CategoriesCommonUsers_keys = -1L;
	public long file_MapFile_CategoriesCommonUsers_index_sizeInBytes = -1L;
	public long keys_MapFile_UserInterestOnCategory = -1L;
	public long index_sizeInBytes_MapFile_UserInterestOnCategory = -1L;
	public long file_MapFile_DMatrix_keys = -1L;
	public long file_MapFile_DMatrix_index_sizeInBytes = -1L;
	
	public ExecutionVariablesGeneral clone(){
		ExecutionVariablesGeneral evg = new ExecutionVariablesGeneral();
		
		evg.inputPath = this.inputPath;
		evg.outputPath = this.outputPath;
		evg.numberOfReduceTasks = this.numberOfReduceTasks;
		evg.showStartStopPrints = this.showStartStopPrints;
		evg.deleteTempFilesAfterExecution = this.deleteTempFilesAfterExecution;
		evg.clearWholeOutputFolderBeforeStart = this.clearWholeOutputFolderBeforeStart;
		evg.askBeforeOverwritingΕxistingFolders = this.askBeforeOverwritingΕxistingFolders;
		evg.onFalseAskBeforeOverwriting_KeepExistingFolders = this.onFalseAskBeforeOverwriting_KeepExistingFolders;
		evg.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity = this.b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
		evg.datasetName = this.datasetName;
		evg.usersPercentage = this.usersPercentage;
		evg.usersNumber = this.usersNumber;
		evg.trainingPercentage = this.trainingPercentage;
		evg.testingPercentage = this.testingPercentage;

		return evg;
	}
	
	public ExecutionVariablesGeneral(){
		
	}
	
	public void calculateTotalSummedExecutionTime(){
		time_TotalSummedExecutionTime = 0;
		time_TotalSummedExecutionTime += time_CalculateTotalAverageDriver;
		time_TotalSummedExecutionTime += time_UserPipelineDriver;
		time_TotalSummedExecutionTime += time_Test_UserPipelineDriver;
		time_TotalSummedExecutionTime += time_CategoryPipelineDriver;
		time_TotalSummedExecutionTime += time_Test_CategoryPipelineDriver;
		time_TotalSummedExecutionTime += time_CategoriesCommonUsersDriver;
		time_TotalSummedExecutionTime += time_Test_CategoriesCommonUsersDriver;
		time_TotalSummedExecutionTime += time_UserInterestOnCategoryDriver;
		time_TotalSummedExecutionTime += time_Test_UserInterestDriver;
		time_TotalSummedExecutionTime += time_DMatrixDriver;
		time_TotalSummedExecutionTime += time_Test_DMatrixDriver;
		
		for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : llOtherRecSys)
		{
			time_TotalSummedExecutionTime += evOtherRecSys.time_NormalizeRatings;
			time_TotalSummedExecutionTime += evOtherRecSys.time_OtherRecommendationSystem;
			time_TotalSummedExecutionTime += evOtherRecSys.time_MergeOtherRecSysWithOur;
			for(ExecutionVariablesTraining evTraining : evOtherRecSys.llTraining)
			{
				time_TotalSummedExecutionTime += evTraining.time_TrainingModelDriver;
				time_TotalSummedExecutionTime += evTraining.time_Test_TrainingModelDriver;
				for(ExecutionVariablesTesting evTesting : evTraining.llTesting)
				{
					time_TotalSummedExecutionTime += evTesting.time_TestingModelPhase1Driver;
					time_TotalSummedExecutionTime += evTesting.time_TestingModelPhase2Driver;
					time_TotalSummedExecutionTime += evTesting.time_ExportTestingModel;
					for(ExecutionVariablesSimilarityEstimation evSimEstim : evTesting.llSimEstim)
					{
						time_TotalSummedExecutionTime += evSimEstim.time_SimilarityEstimation;
					}
				}
			}
		}
		
	}
	
	public static String retNumTabs(int numberOfTabsToReturn)
	{
		String str = "";
		
		for(int i = 0 ; i < numberOfTabsToReturn ; i++)
			str = str.concat("\t");
		
		return str;
	}
	
	public String toStringStatistics(int numTabs){
		String str = "";
		
		String tabsStr = retNumTabs(numTabs);
		
		str += tabsStr + "Execution Parameters" + "\r\n\r\n";
		
		str += tabsStr + "inputPath: " + inputPath + "\r\n";
		str += tabsStr + "outputPath: " + outputPath + "\r\n";
		str += tabsStr + "numberOfReduceTasks: " + numberOfReduceTasks + "\r\n";
		str += tabsStr + "showStartStopPrints: " + (showStartStopPrints ? "true" : "false") + "\r\n";
		str += tabsStr + "deleteTempFilesAfterExecution: " + (deleteTempFilesAfterExecution ? "true" : "false") + "\r\n";
		str += tabsStr + "clearWholeOutputFolderBeforeStart: " + (clearWholeOutputFolderBeforeStart ? "true" : "false") + "\r\n";
		str += tabsStr + "askBeforeOverwritingΕxistingFolders: " + (askBeforeOverwritingΕxistingFolders ? "true" : "false") + "\r\n";
		str += tabsStr + "onFalseAskBeforeOverwriting_KeepExistingFolders: " + (onFalseAskBeforeOverwriting_KeepExistingFolders ? "true" : "false") + "\r\n";
		str += tabsStr + "b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity: " + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity + "\r\n";
		str += tabsStr + "datasetName: " + datasetName + "\r\n";
		
		if(usersNumber > -1L)
			str += tabsStr + "usersNumber: " + usersNumber + "\r\n";
		else
			str += tabsStr + "usersPercentage: " + usersPercentage + "\r\n";
		
		str += tabsStr + "trainingPercentage: " + trainingPercentage + "\r\n";
		str += tabsStr + "testingPercentage: " + testingPercentage + "\r\n";
		
		
		if(categoriesList != null)
		{
			str += tabsStr + "categoriesList: ";
			Iterator<Integer> it = categoriesList.iterator();
			while(it.hasNext()) 
			{
				str += String.valueOf(it.next());
				if(it.hasNext())
					str += ",";
			}
			str += "\r\n";
		}
		str += "\r\n";
		
		str += tabsStr + "Execution Values" + "\r\n\r\n";
		
		str += tabsStr + "medianOfDMatrix: " + medianOfDMatrix + "\r\n";
		str += tabsStr + "maxOfDMatrix: " + maxOfDMatrix + "\r\n";
		str += tabsStr + "stdOfDMatrix: " + stdOfDMatrix + "\r\n";
		str += "\r\n";
		
		str += tabsStr + "Execution Times: ns: nanoseconds, s: seconds" + "\r\n\r\n";
		
		str += tabsStr + "CalculateTotalAverageDriver: " + time_CalculateTotalAverageDriver + "ns\t" + time_CalculateTotalAverageDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "UserPipelineDriver: " + time_UserPipelineDriver + "ns\t" + time_UserPipelineDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "\tTest_UserPipelineDriver: " + time_Test_UserPipelineDriver + "ns\t" + time_Test_UserPipelineDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "CategoryPipelineDriver: " + time_CategoryPipelineDriver + "ns\t" + time_CategoryPipelineDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "\tTest_CategoryPipelineDriver: " + time_Test_CategoryPipelineDriver + "ns\t" + time_Test_CategoryPipelineDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "CategoriesCommonUsersDriver: " + time_CategoriesCommonUsersDriver + "ns\t" + time_CategoriesCommonUsersDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "\tTest_CategoriesCommonUsersDriver: " + time_Test_CategoriesCommonUsersDriver + "ns\t" + time_Test_CategoriesCommonUsersDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "UserInterestOnCategoryDriver: " + time_UserInterestOnCategoryDriver + "ns\t" + time_UserInterestOnCategoryDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "\tTest_UserInterestDriver: " + time_Test_UserInterestDriver + "ns\t" + time_Test_UserInterestDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "DMatrixDriver: " + time_DMatrixDriver + "ns\t" + time_DMatrixDriver / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "\tTest_DMatrixDriver: " + time_Test_DMatrixDriver + "ns\t" + time_Test_DMatrixDriver / 1000000000.0 +"s" + "\r\n";
		str += "\r\n";
		
		calculateTotalSummedExecutionTime();
		
		str += tabsStr + "TotalSummedExecutionTime: " + time_TotalSummedExecutionTime + "ns\t" + time_TotalSummedExecutionTime / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "TotalExecutionTime: " + time_TotalExecutionTime + "ns\t" + time_TotalExecutionTime / 1000000000.0 +"s" + "\r\n";
		str += tabsStr + "Overhead time (TotalExecutionTime - TotalSummedExecutionTime): " + (time_TotalExecutionTime - time_TotalSummedExecutionTime)  + "ns\t" + (time_TotalExecutionTime - time_TotalSummedExecutionTime) / 1000000000.0 +"s" + "\r\n";
		str += "\r\n";
		
		str += tabsStr + "FileSizes" + "\r\n\r\n";
		
		str += tabsStr + "file_MapFile_UserPreferences_keys: " + file_MapFile_UserPreferences_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserPreferences_index_sizeInBytes: " + file_MapFile_UserPreferences_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserPreferencesOnACategory_keys: " + file_MapFile_UserPreferencesOnACategory_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserPreferencesOnACategory_index_sizeInBytes: " + file_MapFile_UserPreferencesOnACategory_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserDisposition_keys: " + file_MapFile_UserDisposition_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserDisposition_index_sizeInBytes: " + file_MapFile_UserDisposition_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserAverageValueOnACategory_keys: " + file_MapFile_UserAverageValueOnACategory_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserAverageValueOnACategory_index_sizeInBytes: " + file_MapFile_UserAverageValueOnACategory_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_CategorySetOfUsers_keys: " + file_MapFile_CategorySetOfUsers_keys + "\r\n";
		str += tabsStr + "file_MapFile_CategorySetOfUsers_index_sizeInBytes: " + file_MapFile_CategorySetOfUsers_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_CategoriesCommonUsers_keys: " + file_MapFile_CategoriesCommonUsers_keys + "\r\n";
		str += tabsStr + "file_MapFile_CategoriesCommonUsers_index_sizeInBytes: " + file_MapFile_CategoriesCommonUsers_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserInterestOnCategory_keys: " + keys_MapFile_UserInterestOnCategory + "\r\n";
		str += tabsStr + "file_MapFile_UserInterestOnCategory_index_sizeInBytes: " + index_sizeInBytes_MapFile_UserInterestOnCategory + "\r\n";
		str += tabsStr + "file_MapFile_DMatrix_keys: " + file_MapFile_DMatrix_keys + "\r\n";
		str += tabsStr + "file_MapFile_DMatrix_index_sizeInBytes: " + file_MapFile_DMatrix_index_sizeInBytes + "\r\n";
		
		return str;
	}
	
	public String toStringTotal_Statistics(boolean extraTab)
	{
		String str = "";
		
		String tabsStr = (extraTab ? "\t" : "");
		
		str += tabsStr + "General" + "\r\n";
		str += tabsStr + "b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity: " + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity + "\r\n";
		
		str += toStringStatistics((extraTab ? 1 : 0));
		
		str += "\r\n\r\n";
		
		str += tabsStr + "\t" + "Other Recommendation System" + "\r\n";
		
		for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : llOtherRecSys)
		{
			str += tabsStr + "\t" + "otherRecSys: " + evOtherRecSys.otherRecSys.toString() + "\r\n";
			
			str += evOtherRecSys.toStringStatistics((extraTab ? 2 : 1));
			
			str += tabsStr + "\t\t" + "Training" + "\r\n";
			
			for(ExecutionVariablesTraining evTraining : evOtherRecSys.llTraining)
			{
				str += tabsStr + "\t\t" + "disGenresThreshold: " + evTraining.disGenresThreshold + "\r\n";
				
				str += evTraining.toStringStatistics(this, (extraTab ? 3 : 2));
				
				str += tabsStr + "\t\t\t" + "Test" + "\r\n";
				
				for(ExecutionVariablesTesting evTesting : evTraining.llTesting)
				{
					str += tabsStr + "\t\t\t" + "k_of_kNN_TestModel: " + evTesting.k_of_kNN_TestingModel + "\r\n";
					
					str += evTesting.toStringStatistics((extraTab ? 4 : 3));
					
					str += tabsStr + "\t\t\t\t" + "SimilarityEstimation" + "\r\n";
					
					for(ExecutionVariablesSimilarityEstimation evSimEstim : evTesting.llSimEstim)
					{
						 str += evSimEstim.toStringStatistics((extraTab ? 5 : 4));
					}
					str += "\r\n\r\n";
				}
				str += "\r\n\r\n";
			}
		}

		str += "\r\n\r\n";
		
		return str;
	}
	
	public String toString_Size(int numTabs)
	{
		String str = "";
		
		String tabsStr = retNumTabs(numTabs);
		
		str += tabsStr + "file_MapFile_UserPreferences_keys: " + file_MapFile_UserPreferences_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserPreferences_index_sizeInBytes: " + file_MapFile_UserPreferences_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserPreferencesOnACategory_keys: " + file_MapFile_UserPreferencesOnACategory_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserPreferencesOnACategory_index_sizeInBytes: " + file_MapFile_UserPreferencesOnACategory_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserDisposition_keys: " + file_MapFile_UserDisposition_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserDisposition_index_sizeInBytes: " + file_MapFile_UserDisposition_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserAverageValueOnACategory_keys: " + file_MapFile_UserAverageValueOnACategory_keys + "\r\n";
		str += tabsStr + "file_MapFile_UserAverageValueOnACategory_index_sizeInBytes: " + file_MapFile_UserAverageValueOnACategory_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_CategorySetOfUsers_keys: " + file_MapFile_CategorySetOfUsers_keys + "\r\n";
		str += tabsStr + "file_MapFile_CategorySetOfUsers_index_sizeInBytes: " + file_MapFile_CategorySetOfUsers_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_CategoriesCommonUsers_keys: " + file_MapFile_CategoriesCommonUsers_keys + "\r\n";
		str += tabsStr + "file_MapFile_CategoriesCommonUsers_index_sizeInBytes: " + file_MapFile_CategoriesCommonUsers_index_sizeInBytes + "\r\n";
		str += tabsStr + "file_MapFile_UserInterestOnCategory_keys: " + keys_MapFile_UserInterestOnCategory + "\r\n";
		str += tabsStr + "file_MapFile_UserInterestOnCategory_index_sizeInBytes: " + index_sizeInBytes_MapFile_UserInterestOnCategory + "\r\n";
		str += tabsStr + "file_MapFile_DMatrix_keys: " + file_MapFile_DMatrix_keys + "\r\n";
		str += tabsStr + "file_MapFile_DMatrix_index_sizeInBytes: " + file_MapFile_DMatrix_index_sizeInBytes + "\r\n";
		
		return str;
	}
	
	public String toStringTotal_Size(boolean extraTab)
	{
		String str = "";
		
		String tabsStr = (extraTab ? "\t" : "");
		
		str += tabsStr + "General" + "\r\n";
		str += tabsStr + "b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity: " + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity + "\r\n";
		
		str += toString_Size((extraTab ? 1 : 0));
		
		str += "\r\n\r\n";
		
		str += tabsStr + "\t" + "Other Recommendation System" + "\r\n";
		
		for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : llOtherRecSys)
		{
			str += tabsStr + "\t" + "otherRecSys: " + evOtherRecSys.otherRecSys.toString() + "\r\n";
			
			str += tabsStr + "\t\t" + "Training" + "\r\n";
			
			for(ExecutionVariablesTraining evTraining : evOtherRecSys.llTraining)
			{
				str += tabsStr + "\t\t" + "disGenresThreshold: " + evTraining.disGenresThreshold + "\r\n";
				
				str += evTraining.toStringSize((extraTab ? 3 : 2));
				
				str += tabsStr + "\t\t\t" + "Test" + "\r\n";
				
				for(ExecutionVariablesTesting evTesting : evTraining.llTesting)
				{
					str += tabsStr + "\t\t\t" + "k_of_kNN_TestModel: " + evTesting.k_of_kNN_TestingModel + "\r\n";
					
					str += evTesting.toStringSize((extraTab ? 4 : 3));

					str += "\r\n\r\n";
				}
				str += "\r\n\r\n";
			}
		}

		str += "\r\n\r\n";
		
		return str;
	}
	
	public String toStringTotal_RMSE_DCG(boolean extraTab)
	{
		String str = "";
		
		String tabStr = (extraTab ? "\t" : "");
		
		str += tabStr + "General" + "\r\n";
		str += tabStr + "b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity: " + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity + "\r\n";
		
		str += tabStr + "\t" + "Other Recommendation System" + "\r\n";
		
		for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : llOtherRecSys)
		{
			str += tabStr + "\t" + "otherRecSys: " + evOtherRecSys.otherRecSys.toString() + "\r\n";
			
			str += tabStr + "\t\t" + "Training" + "\r\n";
			
			for(ExecutionVariablesTraining evTraining : evOtherRecSys.llTraining)
			{
				str += tabStr + "\t\t" + "disGenresThreshold: " + evTraining.disGenresThreshold + "\r\n";
				
				str += tabStr + "\t\t\t" + "Test" + "\r\n";
				
				for(ExecutionVariablesTesting evTesting : evTraining.llTesting)
				{
					str += tabStr + "\t\t\t" + "k_of_kNN_TestModel: " + evTesting.k_of_kNN_TestingModel + "\r\n";
					
					str += tabStr + "\t\t\t\t" + "SimilarityEstimation" + "\r\n";
					
					for(ExecutionVariablesSimilarityEstimation evSimEstim : evTesting.llSimEstim)
					{
						str += tabStr + "\t\t\t\t" + "b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation: " + evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation + "\r\n";
						
						str += tabStr + "\t\t\t\t\t" + "topK_SimilarityEstimation: " + evSimEstim.topK_SimilarityEstimation + "\r\n";
						
						str += tabStr + "\t\t\t\t\t\t" + "RMSE_OtherRecommendationSystem: " + evSimEstim.RMSE_OtherRecommendationSystem + "\r\n";
						str += tabStr + "\t\t\t\t\t\t" + "RMSE_OurRecommendationSystem: " + evSimEstim.RMSE_OurRecommendationSystem + "\r\n";
						str += tabStr + "\t\t\t\t\t\t" + "RMSE_Improvement (%): "  +  (100 - ((100 * evSimEstim.RMSE_OurRecommendationSystem) / evSimEstim.RMSE_OtherRecommendationSystem)) + "\r\n";
					
						str += "\r\n";
						
						str += tabStr + "\t\t\t\t\t\t" + "nDCG_OtherRecommendationSystem: " + evSimEstim.nDCG_OtherRecommendationSystem + "\r\n";
						str += tabStr + "\t\t\t\t\t\t" + "nDCG_OurRecommendationSystem: " + evSimEstim.nDCG_OurRecommendationSystem + "\r\n";
						str += tabStr + "\t\t\t\t\t\t" + "nDCG_Improvement (%): "  +  (((100 * evSimEstim.nDCG_OurRecommendationSystem) / evSimEstim.nDCG_OtherRecommendationSystem) - 100) + "\r\n";
					}
					str += "\r\n\r\n";
				}
				str += "\r\n\r\n";
			}
		}

		str += "\r\n\r\n";
		
		return str;
	}
	
	public class RatingAndString implements Comparable<RatingAndString>{
		public String str = null;
		public double rating = -1.0;
		
		public RatingAndString(String str, double rating)
		{
			this.str = str;
			this.rating = rating;
		}

		public int compareTo(RatingAndString ras) {
			if (rating != ras.rating) {
				return rating < ras.rating ? -1 : 1;
			} else {
				return 0;
			}
		}

	}
	
	public String getToString_RMSE_nDCG(LinkedList<RatingAndString> ll_RMSE, LinkedList<RatingAndString> ll_nDCG)
	{
		String str = "\r\n" + "RMSE in descending order" + "\r\n";
		
		Collections.sort(ll_RMSE);
		Iterator<RatingAndString> it_RMSE = ll_RMSE.descendingIterator();
		while(it_RMSE.hasNext())
		{
			RatingAndString ras = it_RMSE.next();
			str += ras.str;// + "\r\n";
		}
		
		/////////////////////
		
		str += "\r\n" + "nDCG in descending order" + "\r\n";
		
		Collections.sort(ll_nDCG);
		Iterator<RatingAndString> it_nDCG = ll_nDCG.descendingIterator();
		while(it_nDCG.hasNext())
		{
			RatingAndString ras = it_nDCG.next();
			str += ras.str;// + "\r\n";
		}
	
		////////////////////
		
		return str;
	}
	
	public String toStringTotalCSV_RMSE_DCG()
	{
		String str = "";
		str += "\r\n";
		
		str += "-------------------------";
		
		for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : llOtherRecSys)
		{
			for(ExecutionVariablesTraining evTraining : evOtherRecSys.llTraining)
			{
				for(ExecutionVariablesTesting evTesting : evTraining.llTesting)
				{
					for(ExecutionVariablesSimilarityEstimation evSimEstim : evTesting.llSimEstim)
					{
						str += evOtherRecSys.otherRecSys.toString();
						str += "," + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
						str += "," + evTraining.disGenresThreshold;
						str += "," + evTesting.k_of_kNN_TestingModel;
						str += "," + evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation;
						str += "," + evSimEstim.topK_SimilarityEstimation;
						str += "," + evSimEstim.RMSE_OtherRecommendationSystem;
						str += "," + evSimEstim.RMSE_OurRecommendationSystem;
						str += "," + (100 - ((100 * evSimEstim.RMSE_OurRecommendationSystem) / evSimEstim.RMSE_OtherRecommendationSystem));
						str += "," + evSimEstim.nDCG_OtherRecommendationSystem;
						str += "," + evSimEstim.nDCG_OurRecommendationSystem;
						str += "," + (((100 * evSimEstim.nDCG_OurRecommendationSystem) / evSimEstim.nDCG_OtherRecommendationSystem) - 100);
						str += "\r\n";
					}
				}
			}
		}
		
		str += "-------------------------";
		str += "\r\n\r\n";
		
		return str;
	}
	
	public String toStringTotalCSV_Sorted_RMSE_DCG(boolean fullInfo)
	{
		String str = "";
		str += "\r\n";
		
		str += "-------------------------";
				
		LinkedList<RatingAndString> ll_RMSE = new LinkedList<RatingAndString>();
		LinkedList<RatingAndString> ll_nDCG = new LinkedList<RatingAndString>();
		
		for(int counterSelect = 1 ; counterSelect <= 1 ; counterSelect++)
		{
			for(int i = 0 ; i < llOtherRecSys.size() ; i++)
			{
				ExecutionVariablesOtherRecommendationSystem evOtherRecSys = llOtherRecSys.get(i);
				
				if(i > 0 && counterSelect >= 1)
				{
					str += "\r\n";
				}
				
				for(int j = 0 ; j < evOtherRecSys.llTraining.size() ; j++)
				{
					ExecutionVariablesTraining evTraining = evOtherRecSys.llTraining.get(j);
					for(int k = 0 ; k < evTraining.llTesting.size() ; k++)
					{
						ExecutionVariablesTesting evTesting = evTraining.llTesting.get(k);
						for(int l = 0 ; l < evTesting.llSimEstim.size() ; l++)
						{
							ExecutionVariablesSimilarityEstimation evSimEstim = evTesting.llSimEstim.get(l);
							
							String tempStr = "";
							
							tempStr += evOtherRecSys.otherRecSys.toString();
							tempStr += "," + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
							tempStr += "," + evTraining.disGenresThreshold;
							tempStr += "," + evTesting.k_of_kNN_TestingModel;
							tempStr += "," + evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation;
							tempStr += "," + evSimEstim.topK_SimilarityEstimation;
							
							if(fullInfo)
							{
								tempStr += "," + evSimEstim.RMSE_OtherRecommendationSystem;
								tempStr += "," + evSimEstim.RMSE_OurRecommendationSystem;
								tempStr += "," + (100 - ((100 * evSimEstim.RMSE_OurRecommendationSystem) / evSimEstim.RMSE_OtherRecommendationSystem));
								
								tempStr += "," + evSimEstim.nDCG_OtherRecommendationSystem;
								tempStr += "," + evSimEstim.nDCG_OurRecommendationSystem;
								tempStr += "," + (((100 * evSimEstim.nDCG_OurRecommendationSystem) / evSimEstim.nDCG_OtherRecommendationSystem) - 100);
								tempStr += "\r\n";
							}

							double improvement_RMSE = (100 - ((100 * evSimEstim.RMSE_OurRecommendationSystem) / evSimEstim.RMSE_OtherRecommendationSystem));
							double improvement_nDCG = (((100 * evSimEstim.nDCG_OurRecommendationSystem) / evSimEstim.nDCG_OtherRecommendationSystem) - 100);
							
							if(counterSelect <= 3)
							{
								ll_RMSE.add(new RatingAndString(tempStr + (fullInfo ? "" : "," + improvement_RMSE + "\r\n"), improvement_RMSE));
								ll_nDCG.add(new RatingAndString(tempStr + (fullInfo ? "" : "," + improvement_nDCG + "\r\n"), improvement_nDCG));
							}
						}
					}
				}
				
				if(counterSelect == 1)
				{
					str += getToString_RMSE_nDCG(ll_RMSE,ll_nDCG);
					ll_RMSE.clear();
					ll_nDCG.clear();
				}
				
			}
		}

		str += "-------------------------";
		str += "\r\n\r\n";
		
		return str;
	}
	
	public String toStringTotalCSV_Max_RMSE_DCG(boolean fullInfo)
	{
		String str = "";
				
		LinkedList<RatingAndString> ll_RMSE = new LinkedList<RatingAndString>();
		LinkedList<RatingAndString> ll_nDCG = new LinkedList<RatingAndString>();
		
		TreeMap<String, LinkedList<RatingAndString>> tm_RMSE = new TreeMap<String, LinkedList<RatingAndString>>();
		TreeMap<String, LinkedList<RatingAndString>> tm_nDCG = new TreeMap<String, LinkedList<RatingAndString>>();
		
		for(int counterSelect = 1 ; counterSelect <= 4 ; counterSelect++)
		{
		
			str += "\r\n";
			
			if( counterSelect == 1)
				str += "Values grouped by: Other Recommendation System - <1>";
			else if( counterSelect == 2)
				str += "Values grouped by: Training - <2>";
			else if( counterSelect == 3)
				str += "Values grouped by: Testing - <3>";
			else if( counterSelect == 4)
				str += "Values grouped by: Similarity Estimation - b Fmeasure <otherRecSys,ourRecSys> - <4>";
					
			str += "\r\n";
			
			for(int i = 0 ; i < llOtherRecSys.size() ; i++)
			{
				ExecutionVariablesOtherRecommendationSystem evOtherRecSys = llOtherRecSys.get(i);
				
				if(i > 0 && counterSelect >= 1)
				{
					str += "\r\n";
				}
				
				for(int j = 0 ; j < evOtherRecSys.llTraining.size() ; j++)
				{
					ExecutionVariablesTraining evTraining = evOtherRecSys.llTraining.get(j);
					
					if(j > 0 && counterSelect == 2)
						str += "\r\n";
					
					for(int k = 0 ; k < evTraining.llTesting.size() ; k++)
					{
						ExecutionVariablesTesting evTesting = evTraining.llTesting.get(k);
						
						if(k > 0 && counterSelect == 3)
							str += "\r\n";
						
						for(int l = 0 ; l < evTesting.llSimEstim.size() ; l++)
						{
							if(l > 0 && counterSelect == 4)
								str += "\r\n";
							
							ExecutionVariablesSimilarityEstimation evSimEstim = evTesting.llSimEstim.get(l);
							
							String tempStr = "";
							
							tempStr += evOtherRecSys.otherRecSys.toString();
							tempStr += "," + b_Fmeasure_UserInterestOnCategory_CategoryLikeness_CategoryPopularity;
							tempStr += "," + evTraining.disGenresThreshold;
							tempStr += "," + evTesting.k_of_kNN_TestingModel;
							tempStr += "," + evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation;
							tempStr += "," + evSimEstim.topK_SimilarityEstimation;
							
							if(fullInfo)
							{
								tempStr += "," + evSimEstim.RMSE_OtherRecommendationSystem;
								tempStr += "," + evSimEstim.RMSE_OurRecommendationSystem;
								tempStr += "," + (100 - ((100 * evSimEstim.RMSE_OurRecommendationSystem) / evSimEstim.RMSE_OtherRecommendationSystem));
								
								tempStr += "," + evSimEstim.nDCG_OtherRecommendationSystem;
								tempStr += "," + evSimEstim.nDCG_OurRecommendationSystem;
								tempStr += "," + (((100 * evSimEstim.nDCG_OurRecommendationSystem) / evSimEstim.nDCG_OtherRecommendationSystem) - 100);
							}

							double improvement_RMSE = (100 - ((100 * evSimEstim.RMSE_OurRecommendationSystem) / evSimEstim.RMSE_OtherRecommendationSystem));
							double improvement_nDCG = (((100 * evSimEstim.nDCG_OurRecommendationSystem) / evSimEstim.nDCG_OtherRecommendationSystem) - 100);
							
							if(counterSelect <= 3)
							{
								ll_RMSE.add(new RatingAndString(tempStr + (fullInfo ? "" : "," + improvement_RMSE + "\r\n"), improvement_RMSE));
								ll_nDCG.add(new RatingAndString(tempStr + (fullInfo ? "" : "," + improvement_nDCG + "\r\n"), improvement_nDCG));
							}
							
							if(counterSelect == 4)
							{
								LinkedList<RatingAndString> temp_ll_RMSE = tm_RMSE.get(String.valueOf(evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation));
								if(temp_ll_RMSE == null)
									temp_ll_RMSE = new LinkedList<RatingAndString>();
								
								LinkedList<RatingAndString> temp_ll_nDCG = tm_nDCG.get(String.valueOf(evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation));
								if(temp_ll_nDCG == null)
									temp_ll_nDCG = new LinkedList<RatingAndString>();
								
								temp_ll_RMSE.add(new RatingAndString(tempStr + (fullInfo ? "" : "," + improvement_RMSE + "\r\n"), improvement_RMSE));
								temp_ll_nDCG.add(new RatingAndString(tempStr + (fullInfo ? "" : "," + improvement_nDCG + "\r\n"), improvement_nDCG));
								
								tm_RMSE.put(String.valueOf(evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation), temp_ll_RMSE);
								tm_nDCG.put(String.valueOf(evSimEstim.b_Fmeasure_SimilarityEstimation_OtherRecSysEstimation_OurRecSysEstimation), temp_ll_nDCG);
							}
						}
						
						if(counterSelect == 3)
						{
							str += "<3>" + "\r\n";
							str += getToString_max_RMSE_nDCG(ll_RMSE,ll_nDCG);
							str += "</3>" + "\r\n";
							ll_RMSE.clear();
							ll_nDCG.clear();
						}
						
						if(counterSelect == 4)
						{
							Iterator<Entry<String,LinkedList<RatingAndString>>> it_RMSE = tm_RMSE.entrySet().iterator();
							Iterator<Entry<String,LinkedList<RatingAndString>>> it_nDCG = tm_nDCG.entrySet().iterator();
							
							str += "<4>" + "\r\n";
							while(it_RMSE.hasNext() && it_nDCG.hasNext())
							{
								Entry<String, LinkedList<RatingAndString>> entry_RMSE = it_RMSE.next();
								Entry<String, LinkedList<RatingAndString>> entry_nDCG = it_nDCG.next();
																
								str += getToString_max_RMSE_nDCG(entry_RMSE.getValue(),entry_nDCG.getValue());
							}
							
							str += "</4>" + "\r\n";
							tm_RMSE.clear();
							tm_nDCG.clear();
						}
					}
					
					if(counterSelect == 2)
					{
						str += "<2>" + "\r\n";
						str += getToString_max_RMSE_nDCG(ll_RMSE,ll_nDCG);
						str += "</2>" + "\r\n";
						ll_RMSE.clear();
						ll_nDCG.clear();
					}
					
				}
				
				if(counterSelect == 1)
				{
					str += "<1>" + "\r\n";
					str += getToString_max_RMSE_nDCG(ll_RMSE,ll_nDCG);
					str += "</1>" + "\r\n";
					ll_RMSE.clear();
					ll_nDCG.clear();
				}
				
			}
		}
		
		return str;
	}
	
	public String getToString_max_RMSE_nDCG(LinkedList<RatingAndString> ll_RMSE, LinkedList<RatingAndString> ll_nDCG)
	{
		String str = "\r\n" + "RMSE in descending order" + "\r\n";
		
		Collections.sort(ll_RMSE);
		Iterator<RatingAndString> it_RMSE = ll_RMSE.descendingIterator();
		while(it_RMSE.hasNext())
		{
			RatingAndString ras = it_RMSE.next();
			
			String[] tokens = ras.str.split(",");
			
			if(tokens[5].compareTo(String.valueOf(Integer.MAX_VALUE)) == 0)
			{
				str += ras.str;// + "\r\n";
				break;
			}
		}
		
		/////////////////////
		
		str += "\r\n" + "nDCG in descending order" + "\r\n";
		
		Collections.sort(ll_nDCG);
		Iterator<RatingAndString> it_nDCG = ll_nDCG.descendingIterator();
		while(it_nDCG.hasNext())
		{
			RatingAndString ras = it_nDCG.next();
			String[] tokens = ras.str.split(",");
			
			if(tokens[5].compareTo(String.valueOf(Integer.MAX_VALUE)) == 0)
			{
				str += ras.str;// + "\r\n";
				break;
			}
		}
	
		////////////////////
		
		return str;
	}
	
}
