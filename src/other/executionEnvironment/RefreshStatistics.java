package other.executionEnvironment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;

/**
 * 
 * This class refreshes the statistics that are outputted in text form
 * in our folders.
 * 
 * @author John Koumarelas
 *
 */
public class RefreshStatistics {
	
	public PathFilterGeneral varPathFilterGeneral = new PathFilterGeneral();
	public PathFilterOtherRecommendationSystem varPathFilterOtherRecommendationSystem = new PathFilterOtherRecommendationSystem();
	public PathFilterTraining varPathFilterTraining = new PathFilterTraining();
	public PathFilterTesting varPathFilterTesting = new PathFilterTesting();
	public PathFilterSimilarityEstimation varPathFilterSimilarityEstimation = new PathFilterSimilarityEstimation();
	
	public class PathFilterGeneral implements PathFilter
	{
		public boolean accept(Path path) {
			if(path.getName().toString().startsWith("General_"))
				return true;
			else
				return false;
		}
	}
	
	public class PathFilterOtherRecommendationSystem implements PathFilter
	{
		public boolean accept(Path path) {
			if(path.getName().toString().startsWith("OtherRecommendationSystem_"))
				return true;
			else
				return false;
		}
	}
	
	public class PathFilterTraining implements PathFilter
	{
		public boolean accept(Path path) {
			if(path.getName().toString().startsWith("Training_"))
				return true;
			else
				return false;
		}
	}
	
	public class PathFilterTesting implements PathFilter
	{
		public boolean accept(Path path) {
			if(path.getName().toString().startsWith("Testing_"))
				return true;
			else
				return false;
		}
	}
	
	public class PathFilterSimilarityEstimation implements PathFilter
	{
		public boolean accept(Path path) {
			if(path.getName().toString().startsWith("SimilarityEstimation_"))
				return true;
			else
				return false;
		}
	}
	
	public void refreshAll_Size(String outputPathStr)
	{
		Configuration conf = new Configuration();
		try {
			FileSystem fs = FileSystem.get(conf);
			
			Path outputPath = new Path(outputPathStr);
			
			FileStatus[] fsArrayDatasetUsersTrainingTesting = fs.listStatus(outputPath);
			
			String total_Size_OfAllDatasetsStr = "";
			
			for(FileStatus fstDatasetUsersTrainingTesting : fsArrayDatasetUsersTrainingTesting)
			{
				FileStatus[] fsArrayGeneral = fs.listStatus(fstDatasetUsersTrainingTesting.getPath(),varPathFilterGeneral);
				
				total_Size_OfAllDatasetsStr += fstDatasetUsersTrainingTesting.getPath().getName() + "\r\n\r\n";
				
				for(FileStatus fstGeneral : fsArrayGeneral)
				{
					
					String generalStr = fstGeneral.getPath().toString() + File.separator + "Statistics.bin";
					
					ExecutionVariablesGeneral evGeneral = ExecutionVariablesCurrentGroup.getStatistics_General(new Path(generalStr));
										
					FileStatus[] fsArrayOtherRecommendationSystem = fs.listStatus(fstGeneral.getPath(), varPathFilterOtherRecommendationSystem);
					
					if(fsArrayOtherRecommendationSystem.length > 0) evGeneral.llOtherRecSys = new LinkedList<ExecutionVariablesOtherRecommendationSystem>();
					
					for(FileStatus fstOtherRecSys : fsArrayOtherRecommendationSystem)
					{
						
						String otherRecSysStr = fstOtherRecSys.getPath().toString() + File.separator + "Statistics.bin";
						
						ExecutionVariablesOtherRecommendationSystem evOtherRecSys = ExecutionVariablesCurrentGroup.getStatistics_OtherRecommendationSystem(new Path(otherRecSysStr));
					
						FileStatus[] fsArrayTraining = fs.listStatus(fstOtherRecSys.getPath(), varPathFilterTraining);
						
						if(fsArrayTraining.length > 0) evOtherRecSys.llTraining = new LinkedList<ExecutionVariablesTraining>();
						
						for(FileStatus fstTraining : fsArrayTraining)
						{
							String trainingStr = fstTraining.getPath().toString() + File.separator + "Statistics.bin";
							
							ExecutionVariablesTraining evTraining = ExecutionVariablesCurrentGroup.getStatistics_Training(new Path(trainingStr));
							
							FileStatus[] fsArrayTesting = fs.listStatus(fstTraining.getPath(), varPathFilterTesting);
							
							if(fsArrayTesting.length > 0) evTraining.llTesting = new LinkedList<ExecutionVariablesTesting>();
							
							for(FileStatus fstTesting : fsArrayTesting)
							{
								String testingStr = fstTesting.getPath().toString() + File.separator + "Statistics.bin";
								
								ExecutionVariablesTesting evTesting = ExecutionVariablesCurrentGroup.getStatistics_Testing(new Path(testingStr));
								
								FileStatus[] fsArraySimilarityEstimation = fs.listStatus(fstTesting.getPath(), varPathFilterSimilarityEstimation);
								
								if(fsArraySimilarityEstimation.length > 0) evTesting.llSimEstim = new LinkedList<ExecutionVariablesSimilarityEstimation>();
								
								for(FileStatus fstSimilarityEstimation : fsArraySimilarityEstimation)
								{
									String similarityEstimationStr = fstSimilarityEstimation.getPath().toString() + File.separator + "Statistics.bin";
									
									ExecutionVariablesSimilarityEstimation evSimEstim = ExecutionVariablesCurrentGroup.getStatistics_SimilarityEstimation(new Path(similarityEstimationStr));
									
									evTesting.llSimEstim.add(evSimEstim);
								}
									
								evTraining.llTesting.add(evTesting);
							}
							evOtherRecSys.llTraining.add(evTraining);
						}
						evGeneral.llOtherRecSys.add(evOtherRecSys);
					}
					
					// Everything is loaded
					
					total_Size_OfAllDatasetsStr += evGeneral.toStringTotal_Size(true);
				}
			}
			
			exportStatistics(outputPathStr, "total_Size_OfAllDatasets.txt",total_Size_OfAllDatasetsStr);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void refreshAll_Statistics(String outputPathStr)
	{
		Configuration conf = new Configuration();
		try {
			FileSystem fs = FileSystem.get(conf);
			
			Path outputPath = new Path(outputPathStr);
			
			FileStatus[] fsArrayDatasetUsersTrainingTesting = fs.listStatus(outputPath);
			
			String total_Statistics_OfAllDatasetsStr = "";
			
			for(FileStatus fstDatasetUsersTrainingTesting : fsArrayDatasetUsersTrainingTesting)
			{
				FileStatus[] fsArrayGeneral = fs.listStatus(fstDatasetUsersTrainingTesting.getPath(),varPathFilterGeneral);
				
				total_Statistics_OfAllDatasetsStr += fstDatasetUsersTrainingTesting.getPath().getName() + "\r\n\r\n";
				
				for(FileStatus fstGeneral : fsArrayGeneral)
				{
					
					String generalStr = fstGeneral.getPath().toString() + File.separator + "Statistics.bin";
					
					ExecutionVariablesGeneral evGeneral = ExecutionVariablesCurrentGroup.getStatistics_General(new Path(generalStr));
										
					FileStatus[] fsArrayOtherRecommendationSystem = fs.listStatus(fstGeneral.getPath(), varPathFilterOtherRecommendationSystem);
					
					if(fsArrayOtherRecommendationSystem.length > 0) evGeneral.llOtherRecSys = new LinkedList<ExecutionVariablesOtherRecommendationSystem>();
					
					for(FileStatus fstOtherRecSys : fsArrayOtherRecommendationSystem)
					{
						
						String otherRecSysStr = fstOtherRecSys.getPath().toString() + File.separator + "Statistics.bin";
						
						ExecutionVariablesOtherRecommendationSystem evOtherRecSys = ExecutionVariablesCurrentGroup.getStatistics_OtherRecommendationSystem(new Path(otherRecSysStr));
					
						FileStatus[] fsArrayTraining = fs.listStatus(fstOtherRecSys.getPath(), varPathFilterTraining);
						
						if(fsArrayTraining.length > 0) evOtherRecSys.llTraining = new LinkedList<ExecutionVariablesTraining>();
						
						for(FileStatus fstTraining : fsArrayTraining)
						{
							String trainingStr = fstTraining.getPath().toString() + File.separator + "Statistics.bin";
							
							ExecutionVariablesTraining evTraining = ExecutionVariablesCurrentGroup.getStatistics_Training(new Path(trainingStr));
							
							FileStatus[] fsArrayTesting = fs.listStatus(fstTraining.getPath(), varPathFilterTesting);
							
							if(fsArrayTesting.length > 0) evTraining.llTesting = new LinkedList<ExecutionVariablesTesting>();
							
							for(FileStatus fstTesting : fsArrayTesting)
							{
								String testingStr = fstTesting.getPath().toString() + File.separator + "Statistics.bin";
								
								ExecutionVariablesTesting evTesting = ExecutionVariablesCurrentGroup.getStatistics_Testing(new Path(testingStr));
								
								FileStatus[] fsArraySimilarityEstimation = fs.listStatus(fstTesting.getPath(), varPathFilterSimilarityEstimation);
								
								if(fsArraySimilarityEstimation.length > 0) evTesting.llSimEstim = new LinkedList<ExecutionVariablesSimilarityEstimation>();
								
								for(FileStatus fstSimilarityEstimation : fsArraySimilarityEstimation)
								{
									String similarityEstimationStr = fstSimilarityEstimation.getPath().toString() + File.separator + "Statistics.bin";
									
									ExecutionVariablesSimilarityEstimation evSimEstim = ExecutionVariablesCurrentGroup.getStatistics_SimilarityEstimation(new Path(similarityEstimationStr));
									
									evTesting.llSimEstim.add(evSimEstim);
								}
									
								evTraining.llTesting.add(evTesting);
							}
							evOtherRecSys.llTraining.add(evTraining);
						}
						evGeneral.llOtherRecSys.add(evOtherRecSys);
					}
					
					// Everything is loaded
					
					total_Statistics_OfAllDatasetsStr += evGeneral.toStringTotal_Statistics(true);
				}
			}
			exportStatistics(outputPathStr, "total_Statistics_OfAllDatasets.txt", total_Statistics_OfAllDatasetsStr);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private String getTrainingTestingPercentage(String folderName)
	{
		String[] strTokens1 = folderName.split("_");
		
		String[] strTokens11 = strTokens1[2].split("-");
		String[] strTokens12 = strTokens1[3].split("-");
		
		String str = strTokens11[1] + "-" + strTokens12[1];
		
		return str;
	}
	
	public void refreshAll_RMSE_DCG(String outputPathStr){
		Configuration conf = new Configuration();
		try {
			FileSystem fs = FileSystem.get(conf);
			
			Path outputPath = new Path(outputPathStr);
			
			FileStatus[] fsArrayDatasetUsersTrainingTesting = fs.listStatus(outputPath);
			
			Double[] RMSE_Sums = new Double[2];
			RMSE_Sums[0] = 0.0;
			RMSE_Sums[1] = 0.0;
			Integer[] RMSE_Counters = new Integer[2];
			RMSE_Counters[0] = 0;
			RMSE_Counters[1] = 0;
			String total_RMSE_DCG_OfAllDatasetsStr = "";
			String totalCSV_RMSE_DCG_OfAllDatasetsStr = "";
			String totalCSV_Sorted_FullInfo_RMSE_DCG_OfAllDatasetsStr = "";
			String totalCSV_Sorted_ShortInfo_RMSE_DCG_OfAllDatasetsStr = "";
			String totalCSV_Max_ShortInfo_RMSE_DCG_OfAllDatasetsStr = "";
			
			for(FileStatus fstDatasetUsersTrainingTesting : fsArrayDatasetUsersTrainingTesting)
			{
				FileStatus[] fsArrayGeneral = fs.listStatus(fstDatasetUsersTrainingTesting.getPath(),varPathFilterGeneral);
				
				total_RMSE_DCG_OfAllDatasetsStr += fstDatasetUsersTrainingTesting.getPath().getName() + "\r\n\r\n";
				totalCSV_RMSE_DCG_OfAllDatasetsStr += fstDatasetUsersTrainingTesting.getPath().getName() + "\r\n\r\n";
				totalCSV_Sorted_FullInfo_RMSE_DCG_OfAllDatasetsStr +=  fstDatasetUsersTrainingTesting.getPath().getName() + "\r\n\r\n";
				totalCSV_Sorted_ShortInfo_RMSE_DCG_OfAllDatasetsStr +=  fstDatasetUsersTrainingTesting.getPath().getName() + "\r\n\r\n";
				totalCSV_Max_ShortInfo_RMSE_DCG_OfAllDatasetsStr += "<split-"+ getTrainingTestingPercentage(fstDatasetUsersTrainingTesting.getPath().getName()) + ">" + "\r\n";
				
				for(FileStatus fstGeneral : fsArrayGeneral)
				{
					
					String generalStr = fstGeneral.getPath().toString() + File.separator + "Statistics.bin";
					
					ExecutionVariablesGeneral evGeneral = ExecutionVariablesCurrentGroup.getStatistics_General(new Path(generalStr));
					
					FileStatus[] fsArrayOtherRecommendationSystem = fs.listStatus(fstGeneral.getPath(), varPathFilterOtherRecommendationSystem);
					
					if(fsArrayOtherRecommendationSystem.length > 0) evGeneral.llOtherRecSys = new LinkedList<ExecutionVariablesOtherRecommendationSystem>();
					
					for(FileStatus fstOtherRecSys : fsArrayOtherRecommendationSystem)
					{
						
						String otherRecSysStr = fstOtherRecSys.getPath().toString() + File.separator + "Statistics.bin";
						
						ExecutionVariablesOtherRecommendationSystem evOtherRecSys = ExecutionVariablesCurrentGroup.getStatistics_OtherRecommendationSystem(new Path(otherRecSysStr));
					
						FileStatus[] fsArrayTraining = fs.listStatus(fstOtherRecSys.getPath(), varPathFilterTraining);
						
						if(fsArrayTraining.length > 0) evOtherRecSys.llTraining = new LinkedList<ExecutionVariablesTraining>();
						
						for(FileStatus fstTraining : fsArrayTraining)
						{
							String trainingStr = fstTraining.getPath().toString() + File.separator + "Statistics.bin";
							
							ExecutionVariablesTraining evTraining = ExecutionVariablesCurrentGroup.getStatistics_Training(new Path(trainingStr));
							
							FileStatus[] fsArrayTesting = fs.listStatus(fstTraining.getPath(), varPathFilterTesting);
							
							if(fsArrayTesting.length > 0) evTraining.llTesting = new LinkedList<ExecutionVariablesTesting>();
							
							for(FileStatus fstTesting : fsArrayTesting)
							{
								String testingStr = fstTesting.getPath().toString() + File.separator + "Statistics.bin";
								
								ExecutionVariablesTesting evTesting = ExecutionVariablesCurrentGroup.getStatistics_Testing(new Path(testingStr));
								
								FileStatus[] fsArraySimilarityEstimation = fs.listStatus(fstTesting.getPath(), varPathFilterSimilarityEstimation);
								
								if(fsArraySimilarityEstimation.length > 0) evTesting.llSimEstim = new LinkedList<ExecutionVariablesSimilarityEstimation>();
								
								for(FileStatus fstSimilarityEstimation : fsArraySimilarityEstimation)
								{
									String similarityEstimationStr = fstSimilarityEstimation.getPath().toString() + File.separator + "Statistics.bin";
									
									ExecutionVariablesSimilarityEstimation evSimEstim = ExecutionVariablesCurrentGroup.getStatistics_SimilarityEstimation(new Path(similarityEstimationStr));
									
									evTesting.llSimEstim.add(evSimEstim);
								}
									
								evTraining.llTesting.add(evTesting);
							}
							evOtherRecSys.llTraining.add(evTraining);
						}
						evGeneral.llOtherRecSys.add(evOtherRecSys);
					}
					
					// Everything is loaded
					
					ExecutionVariablesCurrentGroup evcg = new ExecutionVariablesCurrentGroup();
					
					evcg.evGeneral = evGeneral;
					
					for(ExecutionVariablesOtherRecommendationSystem evOtherRecSys : evcg.evGeneral.llOtherRecSys)
					{
						evcg.evOtherRecSys = evOtherRecSys;
						
						for(ExecutionVariablesTraining evTraining : evcg.evOtherRecSys.llTraining)
						{
							evcg.evTraining = evTraining;
							
							for(ExecutionVariablesTesting evTesting : evcg.evTraining.llTesting)
							{
								evcg.evTesting = evTesting;
								
								for(ExecutionVariablesSimilarityEstimation evSimEstim : evcg.evTesting.llSimEstim)
								{
									evcg.evSimEstim = evSimEstim;
									
									evcg.exportStatistics_SimilarityEstimation();
								}
								
								evcg.exportStatistics_Testing();
								evcg.saveStatistics_Testing();
							}
							
							evcg.exportStatistics_Training();
							evcg.saveStatistics_Training();
						}
						
						evcg.exportStatistics_OtherRecommendationSystem();
						evcg.saveStatistics_OtherRecommendationSystem();
					}
					
					evcg.exportStatistics_General();
					evcg.saveStatistics_General();
					
					evcg.exportStatistics_Total_RMSE_DCG();
					
					total_RMSE_DCG_OfAllDatasetsStr += evcg.evGeneral.toStringTotal_RMSE_DCG(true);//,RMSE_Sums,RMSE_Counters) + "\r\n\r\n";
					totalCSV_RMSE_DCG_OfAllDatasetsStr += evcg.evGeneral.toStringTotalCSV_RMSE_DCG();
					totalCSV_Sorted_FullInfo_RMSE_DCG_OfAllDatasetsStr += evcg.evGeneral.toStringTotalCSV_Sorted_RMSE_DCG(true);
					totalCSV_Sorted_ShortInfo_RMSE_DCG_OfAllDatasetsStr += evcg.evGeneral.toStringTotalCSV_Sorted_RMSE_DCG(false);
					totalCSV_Max_ShortInfo_RMSE_DCG_OfAllDatasetsStr += evcg.evGeneral.toStringTotalCSV_Max_RMSE_DCG(false);
				}
				totalCSV_Max_ShortInfo_RMSE_DCG_OfAllDatasetsStr += "\r\n" + "</split-" + getTrainingTestingPercentage(fstDatasetUsersTrainingTesting.getPath().getName()) + ">" + "\r\n";;
			}
			exportStatistics(outputPathStr, "total_RMSE_nDCG_OfAllDatasets.txt",total_RMSE_DCG_OfAllDatasetsStr);
			exportStatistics(outputPathStr, "totalCSV_RMSE_DCG_OfAllDatasets.txt",totalCSV_RMSE_DCG_OfAllDatasetsStr);
			exportStatistics(outputPathStr, "totalCSV_Sorted_FullInfo_RMSE_DCG_OfAllDatasets.txt",totalCSV_Sorted_FullInfo_RMSE_DCG_OfAllDatasetsStr);
			exportStatistics(outputPathStr, "totalCSV_Sorted_ShortInfo_RMSE_DCG_OfAllDatasets.txt",totalCSV_Sorted_ShortInfo_RMSE_DCG_OfAllDatasetsStr);
			exportStatistics(outputPathStr, "totalCSV_Max_ShortInfo_RMSE_DCG_OfAllDatasets.txt",totalCSV_Max_ShortInfo_RMSE_DCG_OfAllDatasetsStr);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void exportStatistics(String outputPathStr, String strFileName, String strToWrite)
	{
		try {
			File tempFile = new File("./"+strFileName+"");
			if(tempFile.exists())
				tempFile.delete();
			File tempFileCrc = new File("./."+strFileName+".crc");
			if(tempFileCrc.exists())
				tempFileCrc.delete();
			
			FileWriter fstream = new FileWriter("./"+strFileName+".txt");
			BufferedWriter out = new BufferedWriter(fstream);
			
			out.write(strToWrite);
			
			out.close();
			
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);

			Path fromLocalFilePath = new Path("./"+strFileName+".txt");
			String outputPath = outputPathStr + File.separator + strFileName;
			Path toFSFilePath = new Path(outputPath);
			
			fs.delete(toFSFilePath, true);
			Path tsFSFilePathCrc = new Path("." + outputPath + ".crc");
			fs.delete(tsFSFilePathCrc, true);
			
			fs.copyFromLocalFile(true, true, fromLocalFilePath, toFSFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run(String outputPathStr){
		refreshAll_RMSE_DCG(outputPathStr);
		refreshAll_Statistics(outputPathStr);
		refreshAll_Size(outputPathStr);
	}
	
}
