package other.executionEnvironment;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.Map.Entry;

/**
 * 
 * ExecutionVariablesTesting.java
 * 
 * This class provides an easy and structured way of giving access
 * to parameters that are provided in "ExecutionParameters.txt".
 * 
 * We also maintain other fields that help other Classes during the execution,
 * and there are also fields for measuring execution time and specific file sizes
 * (files that must be loaded in the RAM, such as MapFile's index).
 * 
 * Objects from this class and the other ExecutionVariablesX classes, are saved
 * and can be loaded again for future statistics and refreshing of statistics
 * by our program.
 * 
 * @author John Koumarelas
 * @see ExecutionVariablesGeneral, ExecutionVariablesOtherRecommendationSystem, 
 * 		ExecutionVariablesTraining, ExecutionVariablesSimilarityEstimation
 */
public class ExecutionVariablesTesting implements Serializable{

	private static final long serialVersionUID = -6425801733313757784L;

	// Execution Parameters
	public int k_of_kNN_TestingModel = -1;
	
	// Execution Variables - Testing
	public LinkedList<ExecutionVariablesSimilarityEstimation> llSimEstim = new LinkedList<ExecutionVariablesSimilarityEstimation>();
	
	// Execution Times
	public long time_TestingModelPhase1Driver = -1L;
	public long time_TestingModelPhase2Driver = -1L;
	public long time_ExportTestingModel = -1L;
	
	// File Sizes
	public TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TestingModel1_keys = null;
	public TreeMap<Integer,TreeMap<Integer,Long>> file_MapFile_TestingModel1_index_sizeInBytes = null;
	
	public long file_MapFile_TestingModel2_keys = -1L;
	public long file_MapFile_TestingModel2_index_sizeInBytes = -1L;
	
	public String toStringStatistics(int numTabs){
		String str = "";
		
		String tabsStr = ExecutionVariablesGeneral.retNumTabs(numTabs);
		
		str += tabsStr + "Execution Parameters" + "\r\n\r\n";
		
		str += tabsStr + "k_of_kNN_TestingModel: " + k_of_kNN_TestingModel + "\r\n";
		
		str += tabsStr + "Execution Values" + "\r\n";
		
		str += tabsStr + "Execution Times: ns: nanoseconds, s: seconds" + "\r\n\r\n";
		
		str += tabsStr + "TestingModelPhase1Driver: " + time_TestingModelPhase1Driver +"ns\t" + time_TestingModelPhase1Driver / 1000000000.0 + "s" + "\r\n";
		str += tabsStr + "TestingModelPhase2Driver: " + time_TestingModelPhase2Driver +"ns\t" + time_TestingModelPhase2Driver / 1000000000.0 + "s" + "\r\n";
		str += tabsStr + "ExportTestingModel: " + time_ExportTestingModel +"ns\t" + time_ExportTestingModel / 1000000000.0 + "s"  + "\r\n";
		
		str += tabsStr + "File Size info (size in Bytes)" + "\r\n";
		
		if(file_MapFile_TestingModel1_keys != null && file_MapFile_TestingModel1_index_sizeInBytes != null)
		{
			Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatKeys = file_MapFile_TestingModel1_keys.entrySet().iterator();
			Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatSize = file_MapFile_TestingModel1_index_sizeInBytes.entrySet().iterator();
			while(itTmCatKeys.hasNext() && itTmCatSize.hasNext())
			{
				Entry<Integer, TreeMap<Integer, Long>> entryTmCatKeys = itTmCatKeys.next();
				Entry<Integer, TreeMap<Integer, Long>> entryTmCatSize = itTmCatSize.next();
				
				Integer categoryID = entryTmCatKeys.getKey();
				
				Iterator<Entry<Integer, Long>> itTmCatFilePartKeys = entryTmCatKeys.getValue().entrySet().iterator();
				Iterator<Entry<Integer, Long>> itTmCatFilePartSize = entryTmCatSize.getValue().entrySet().iterator();
				while(itTmCatFilePartKeys.hasNext() && itTmCatFilePartSize.hasNext())
				{
					Entry<Integer, Long> entryTmCatFilePartKeys = itTmCatFilePartKeys.next();
					Entry<Integer, Long> entryTmCatFilePartSize = itTmCatFilePartSize.next();
					
					Integer filePart = entryTmCatFilePartKeys.getKey();
					
					Long keys = entryTmCatFilePartKeys.getValue();
					Long size = entryTmCatFilePartSize.getValue();
					
					str += tabsStr + "category: " + categoryID + " filePart: " + filePart + " keys: " + keys + " fileSize: " + size + "\r\n";
				}
			}
		}
		
		
		str += tabsStr + "file_MapFile_TestingModel2_keys: " + file_MapFile_TestingModel2_keys + "\r\n";
		str += tabsStr + "file_MapFile_TestingModel2_index_sizeInBytes: " + file_MapFile_TestingModel2_index_sizeInBytes + "\r\n";
		
		return str;
	}
	
	public String toStringSize(int numTabs)
	{
		String str = "";
		
		String tabsStr = ExecutionVariablesGeneral.retNumTabs(numTabs);
		
		str += tabsStr + "test phase 1" + "\r\n";
		
		if(file_MapFile_TestingModel1_keys != null && file_MapFile_TestingModel1_index_sizeInBytes != null)
		{
			Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatKeys = file_MapFile_TestingModel1_keys.entrySet().iterator();
			Iterator<Entry<Integer,TreeMap<Integer, Long>>> itTmCatSize = file_MapFile_TestingModel1_index_sizeInBytes.entrySet().iterator();
			while(itTmCatKeys.hasNext() && itTmCatSize.hasNext())
			{
				Entry<Integer, TreeMap<Integer, Long>> entryTmCatKeys = itTmCatKeys.next();
				Entry<Integer, TreeMap<Integer, Long>> entryTmCatSize = itTmCatSize.next();
				
				Integer categoryID = entryTmCatKeys.getKey();
				
				Iterator<Entry<Integer, Long>> itTmCatFilePartKeys = entryTmCatKeys.getValue().entrySet().iterator();
				Iterator<Entry<Integer, Long>> itTmCatFilePartSize = entryTmCatSize.getValue().entrySet().iterator();
				while(itTmCatFilePartKeys.hasNext() && itTmCatFilePartSize.hasNext())
				{
					Entry<Integer, Long> entryTmCatFilePartKeys = itTmCatFilePartKeys.next();
					Entry<Integer, Long> entryTmCatFilePartSize = itTmCatFilePartSize.next();
					
					Integer filePart = entryTmCatFilePartKeys.getKey();
					
					Long keys = entryTmCatFilePartKeys.getValue();
					Long size = entryTmCatFilePartSize.getValue();
					
					str += tabsStr + "category: " + categoryID + " filePart: " + filePart + " keys: " + keys + " fileSize: " + size + "\r\n";
				}
			}
		}
		
		str += tabsStr + "test phase 2" + "\r\n";
		
		str += tabsStr + "file_MapFile_TestingModel2_keys: " + file_MapFile_TestingModel2_keys + "\r\n";
		str += tabsStr + "file_MapFile_TestingModel2_index_sizeInBytes: " + file_MapFile_TestingModel2_index_sizeInBytes + "\r\n";
		
		return str;
	}
	
	public void copyValues(TreeMap<Integer,TreeMap<Integer,Long>> tempFile_MapFile_TestingModel1_keys, TreeMap<Integer,TreeMap<Integer,Long>> tempFile_MapFile_TestingModel1_index_sizeInBytes)
	{
		file_MapFile_TestingModel1_keys = new TreeMap<Integer,TreeMap<Integer,Long>>();
		file_MapFile_TestingModel1_index_sizeInBytes = new TreeMap<Integer,TreeMap<Integer,Long>>();
		
		Iterator<Entry<Integer, TreeMap<Integer, Long>>> itTmCatFileKeys = tempFile_MapFile_TestingModel1_keys.entrySet().iterator();
		Iterator<Entry<Integer, TreeMap<Integer, Long>>> itTmCatFileSize = tempFile_MapFile_TestingModel1_index_sizeInBytes.entrySet().iterator();
		while(itTmCatFileKeys.hasNext() && itTmCatFileSize.hasNext())
		{
			Entry<Integer, TreeMap<Integer, Long>> entryTmCatFileKeys = itTmCatFileKeys.next();
			Entry<Integer, TreeMap<Integer, Long>> entryTmCatFileSize = itTmCatFileSize.next();
			
			Integer categoryID = entryTmCatFileKeys.getKey();
			
			if(entryTmCatFileSize.getKey() != categoryID)
				System.out.println("PROBLEM 3");
			
			TreeMap<Integer, Long> tsCatFilePartsKeysCopy = new TreeMap<Integer, Long>();
			TreeMap<Integer, Long> tsCatFilePartsSizeCopy = new TreeMap<Integer, Long>();
			
			Iterator<Entry<Integer, Long>> itTmCatFilePartsKeys =  entryTmCatFileKeys.getValue().entrySet().iterator();
			Iterator<Entry<Integer, Long>> itTmCatFilePartsSize =  entryTmCatFileSize.getValue().entrySet().iterator();
			while(itTmCatFilePartsKeys.hasNext() && itTmCatFilePartsSize.hasNext())
			{
				Entry<Integer, Long> entryTmCatFilePartKeys = itTmCatFilePartsKeys.next();
				Entry<Integer, Long> entryTmCatFilePartSize = itTmCatFilePartsSize.next();
				
				Integer filePart = entryTmCatFilePartKeys.getKey();
				
				if(entryTmCatFilePartSize.getKey() != filePart)
					System.out.println("PROBLEM 4");
				
				Long keys = entryTmCatFilePartKeys.getValue();
				Long size = entryTmCatFilePartSize.getValue();
				
				tsCatFilePartsKeysCopy.put(filePart, keys);
				tsCatFilePartsSizeCopy.put(filePart, size);
			}
			
			file_MapFile_TestingModel1_keys.put(categoryID, tsCatFilePartsKeysCopy);
			file_MapFile_TestingModel1_index_sizeInBytes.put(categoryID, tsCatFilePartsSizeCopy);
		}
	}
}
