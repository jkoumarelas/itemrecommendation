package other.naiveBayes;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author Karagiannidis Savvas
 */
public class NaiveBayesFilter
{
    private User currentUser;
    private int positiveTuples;
    private int negativeTuples;
    private int numberOfTuples;
    private double[] positiveProbabilities;
    private double[] negativeProbabilities;

    public NaiveBayesFilter()
    {

    }

    public void SetNewUser(User newUser)
    {
        this.currentUser = newUser;
    }

    public void ProduceUsersModel(double positiveTupleThreshold){
        //Calculate the user's number of positive and negative Tuples.
    	Map<Long, Tuple> historicalData = this.currentUser.GetHistoricalData();
    	Set<Long> keys = historicalData.keySet();
        this.negativeTuples = 0;
        this.positiveTuples = 0;
        this.numberOfTuples = 0;
        int[] positiveTuplesC = null;
        int[] numberOfTuplesC = null;
        for(Long key : keys){
        	this.negativeProbabilities = new double[historicalData.get(key).GetDataVector().size()];
            this.positiveProbabilities = new double[historicalData.get(key).GetDataVector().size()];
            positiveTuplesC = new int[historicalData.get(key).GetDataVector().size()];
            numberOfTuplesC = new int[historicalData.get(key).GetDataVector().size()];
            break;
        }
        
        for(Long key : keys){
        	if(historicalData.get(key).GetRating() >= positiveTupleThreshold){
        		this.positiveTuples++;
        	}
        	this.numberOfTuples++;
        }
        this.negativeTuples = this.numberOfTuples - this.positiveTuples;

        //Calculate each feature's positive and negative probability.
        

        for(Long key : keys){
        	Tuple buffer = historicalData.get(key);
        	for(int j = 0 ; j < buffer.GetDataVector().size() ; j++){
                if(buffer.GetDataVectorCell(j) == true){
                    if(buffer.GetRating() >= positiveTupleThreshold){
                        positiveTuplesC[j]++;
                    }
                    numberOfTuplesC[j]++;
                }
            }
        }

        for(int i = 0 ; i < positiveTuplesC.length ; i++)
        {
        	if(numberOfTuplesC[i] == 0){
        		this.positiveProbabilities[i] = 0;
        		this.negativeProbabilities[i] = 0;
        	}else{
        		if(numberOfTuplesC[i] > 0){
        			this.positiveProbabilities[i] = (double)positiveTuplesC[i] / (double)numberOfTuplesC[i];
        			this.negativeProbabilities[i] = (double)(numberOfTuplesC[i] - positiveTuplesC[i]) / (double)numberOfTuplesC[i];
                }
        		else{
        			this.positiveProbabilities[i] = -1.0;
        			this.negativeProbabilities[i] = -1.0;
        		}
        	}
        }
    }

    public double CalculateScoreForTuple(Tuple newTuple)
    {
        double nominator = (double)this.positiveTuples / (double)this.numberOfTuples;
        double denominator = (double)this.negativeTuples / (double)this.numberOfTuples;

        for(int i = 0 ; i < newTuple.GetDataVector().size() ;  i++)
        {
            if(newTuple.GetDataVectorCell(i) == true)
            {
            	if(this.positiveProbabilities[i] >= 0.0)
            	{
                nominator = nominator * this.positiveProbabilities[i];
                denominator = denominator * this.negativeProbabilities[i];
                }
            }
        }

        if(nominator == 0.0 && denominator == 0.0){
        	return 0.0;
        }
        return nominator / (nominator + denominator);

    }
}
