package other.naiveBayes;

import java.util.Map.Entry;
import java.util.TreeMap;

public class NaiveBayesEvaluator {
	
	public static TreeMap<Long, Double> runNaiveBayes(User userProfileTraining, 
			User userProfileTesting){
		
			NaiveBayesFilter nbModel = getNaiveBayesModel(userProfileTraining);
			
			TreeMap<Long, Double> tmTestPredictions = new TreeMap<Long, Double>();
			
			for(Entry<Long, Tuple> tuples : 
					userProfileTesting.GetHistoricalData().entrySet()){
				
				double score = nbModel.CalculateScoreForTuple(tuples.getValue());	// score in [0,1]
				tmTestPredictions.put(tuples.getKey(), score*5);
			}
		
			return tmTestPredictions;
	}

	private static NaiveBayesFilter getNaiveBayesModel(User user) {
		NaiveBayesFilter nb = new NaiveBayesFilter();
		nb.SetNewUser(user);
		nb.ProduceUsersModel(user.GetAverageRating());
		return nb;
	}

}
