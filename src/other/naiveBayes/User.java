package other.naiveBayes;

import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Karagiannidis Savvas
 */
public class User
{
    private long userID;
    private Map<Long, Tuple> historicalData;//itemID -> tuple
    private double averageRating;

    public User(long userID, Map<Long, Tuple> historicalData)
    {
        this.userID = userID;
        this.historicalData = historicalData;
        this.averageRating = 0.0;

        for(Entry<Long, Tuple> entry : historicalData.entrySet())
        {
            this.averageRating += entry.getValue().GetRating();
        }
        this.averageRating = this.averageRating / (double) this.historicalData.size();
    }

    public void AddNewTuple(Tuple newTuple)
    {
		this.averageRating = this.averageRating * this.historicalData.size();

        this.historicalData.put(newTuple.getItemID(), newTuple);

        this.averageRating += newTuple.GetRating();

        this.averageRating = this.averageRating / (double) this.historicalData.size();
    }

    public Map<Long, Tuple> GetHistoricalData()
    {
        return this.historicalData;
    }

    public double GetAverageRating()
    {
        return this.averageRating;
    }

    public int GetNumberOfTuples()
    {
        return this.historicalData.size();
    }

	public long GetUserID() {
		return userID;
	}

}
