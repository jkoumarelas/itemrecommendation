package other.naiveBayes;

import java.util.Vector;

/**
 *
 * @author Karagiannidis Savvas
 */
public class Tuple
{
    private Vector<Boolean> dataVector;
    private double rating;
    private long userID;
    private long itemID;

    public Tuple(Vector<Boolean> dataVector, double rating)
    {
        this.dataVector = dataVector;
        this.rating = rating;
    }
    
    public Tuple(Vector<Boolean> dataVector, double rating, long userID, long itemID)
    {
        this.dataVector = dataVector;
        this.rating = rating;
        this.userID = userID;
        this.itemID = itemID;
    }
    
    public Tuple(Tuple tuple){
    	this.dataVector = tuple.GetDataVector();
    	this.rating = tuple.GetRating();
    	this.userID = tuple.getUserID();
    	this.itemID = tuple.getItemID();
    }

    public Vector<Boolean> GetDataVector()
    {
        return this.dataVector;
    }

    public double GetRating()
    {
        return this.rating;
    }

    public boolean GetDataVectorCell(int vectorIntex)
    {
        return this.dataVector.get(vectorIntex).booleanValue();
    }
	
	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public void setDataVector(Vector<Boolean> dataVector) {
		this.dataVector = dataVector;
	}

	public long getItemID() {
		return itemID;
	}

	public void setItemID(long itemID) {
		this.itemID = itemID;
	}
    
}
