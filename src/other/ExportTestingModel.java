package other;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.MapFile;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;
import rate.TestingObjectPhase2Output;

import commons.LongPairWritable;

/**
 * 
 * ExportTestingModel.java
 * 
 * This class will export the Testing estimations 
 * for visual verification purposes.
 * 
 * @author John Koumarelas
 *
 */
public class ExportTestingModel {
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	public static void run(ExecutionVariablesCurrentGroup evcg) throws IOException{
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Export Testing Model - Start");
		
		long start = System.nanoTime();
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		String outputPathStr = evcg.getOutputPathTesting() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_OurSystemOutput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Export Testing Model*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true); // true: recursively
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
				fs.mkdirs(new Path(outputPathStr));
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
			else
				fs.mkdirs(new Path(outputPathStr));
		}
		
		Path pathQualifiedOutputTesting = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
		FSDataOutputStream	fsOutStreamTesting = fs.create(pathQualifiedOutputTesting);
		BufferedWriter bwOut = new BufferedWriter(new OutputStreamWriter(fsOutStreamTesting));
		
		
		String pathInputStr = evcg.getOutputPathTesting() + File.separator +"RatePrediction" + File.separator + "TestingModelPhase2" + "_Final";
		Path pathInput = new Path(pathInputStr);
    	Path qualifiedDirNameInput = fs.makeQualified(pathInput);
		MapFile.Reader reader = new MapFile.Reader(fs, qualifiedDirNameInput.toString(),conf);
		
    	TestingObjectPhase2Output tObjPh2Out = new TestingObjectPhase2Output();
		LongPairWritable key = new LongPairWritable();
		key.set(Long.MIN_VALUE, Long.MIN_VALUE);
    	
		key = (LongPairWritable) reader.getClosest(key, tObjPh2Out);
		
		// First User, first Item
		bwOut.write(key.getFirst() + "," + key.getSecond() + "," + tObjPh2Out.normalizedRating + "," + tObjPh2Out.otherRecSysEstimationRating + "," + tObjPh2Out.ourEstimationRating);

		while(reader.next(key, tObjPh2Out))
			bwOut.write("\r\n" + key.getFirst() + "," + key.getSecond() + "," + tObjPh2Out.normalizedRating + "," + tObjPh2Out.otherRecSysEstimationRating + "," + tObjPh2Out.ourEstimationRating);
		
		bwOut.close();
		reader.close();
		
		long stop = System.nanoTime();
		evcg.evTesting.time_ExportTestingModel = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Export Testing Model - Stop");
		evcg.hasChangedTesting = true;
	}
}
