package other;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

/**
 * 
 * This class returns the categories in a TreeSet to be used from other classes
 * of our system.
 * 
 * @author John Koumarelas
 * 
 */
public class GetCategoriesTreeSet {

	public static TreeSet<Integer> run(ExecutionVariablesCurrentGroup evcg) {

		String pathStr = evcg.evGeneral.inputPath + File.separator
				+ "filesNeeded" + File.separator + "categories"
				+ evcg.evGeneral.datasetName;

		Configuration conf = new Configuration();
		FileSystem fs = null;
		try {
			fs = FileSystem.get(conf);
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		FSDataInputStream fsdis = null;
		try {
			fsdis = fs.open(new Path(pathStr));
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fsdis));

		String strLine = null;

		TreeSet<Integer> ts = new TreeSet<Integer>();

		try {
			while ((strLine = br.readLine()) != null) {
				ts.add(new Integer(Integer.parseInt(strLine)));
			}
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ts;
	}

}
