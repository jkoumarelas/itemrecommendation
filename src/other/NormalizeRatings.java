package other;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;
import other.executionEnvironment.ExecutionVariablesOtherRecommendationSystem;

/**
 * 
 * NormalizeRatings.java
 * 
 * This class normalizes the ratings (user rating * user disposition) for the
 * OtherRecommendationSystem phase, as well as for our Training and Testing
 * Map / Reduce phases.
 * 
 * @author John Koumarelas
 *
 */
public class NormalizeRatings {
	private static Configuration conf = null;
	private static FileSystem fs = null;
	private static LocalFileSystem lfs = null;
	
	public static boolean normalizeTrainingOurSystem(ExecutionVariablesCurrentGroup evcg) throws IOException{
		
		String outputPathStr = evcg.getOutputPathGeneral() + File.separator + "newUserPreferences" + File.separator + "userPreferences_training_OurSystemInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Training Our System*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		
		if(!lfs.exists(new Path("localMapFiles" + File.separator + "UserDisposition_Final")))
		{
			Path pathHDFS = new Path(evcg.getOutputPathGeneral() + File.separator +"UserPipeline" + File.separator + "UserDisposition_Final");
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "UserDisposition_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
		}
		
		Path pathInput = new Path("localMapFiles" + File.separator + "UserDisposition_Final");
    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		
		DoubleWritable dwValue = new DoubleWritable();
    	LongWritable lwUserID = new LongWritable(-1);
    	
    	Path qualifiedOutputPath = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
    	
		FSDataOutputStream	fsOutStream = fs.create(qualifiedOutputPath);
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutStream));
    	
		DataInputStream in = new DataInputStream(fs.open(new Path(evcg.getInputPathDatasetAndPercentages() + File.separator + "Training" + File.separator + "data")));

		BufferedReader brIn = new BufferedReader(new InputStreamReader(in));
		String strLine;
				
		boolean firstTime = true;
		
		while ((strLine = brIn.readLine()) != null)   {
			
			if(firstTime){
				firstTime = false;
			}
			else{
				bw.write("\r\n");
			}
			
			String[] strUserPreferences = strLine.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			lwUserID.set(Long.parseLong(strUserID));
			reader.get(lwUserID, dwValue);
			
			double userDisposition = dwValue.get();
			
			bw.write(strUserID);
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
								
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");

				double rating = Double.parseDouble(strItemInfo[2]);
				
				rating = rating * userDisposition;
				if(rating > 5.0)
					rating = 5.0;
				
				bw.write("\t" + strItemInfo[0] + "-" + strItemInfo[1] + "-" + String.valueOf(rating));
			}
		}

		brIn.close();
		bw.close();
		reader.close();
		return true;
	}
	
	public static boolean normalizeTrainingRecSys_NaiveBayes(ExecutionVariablesCurrentGroup evcg) throws IOException{

		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_training_RecSysInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Training Recommendation System - NaiveBayes*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}

		if(!lfs.exists(new Path("localMapFiles" + File.separator + "UserDisposition_Final")))
		{
			Path pathHDFS = new Path(evcg.getOutputPathGeneral() + File.separator +"UserPipeline" + File.separator + "UserDisposition_Final");
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "UserDisposition_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
		}
		
		Path pathInput = new Path("localMapFiles" + File.separator + "UserDisposition_Final");
    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		
		DoubleWritable dwValue = new DoubleWritable();
    	LongWritable lwUserID = new LongWritable(-1);
    	
    	Path qualifiedOutputPath = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
		FSDataOutputStream	fsOutStream = fs.create(qualifiedOutputPath);
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutStream));
    	
		DataInputStream in = new DataInputStream(fs.open(new Path(evcg.getInputPathDatasetAndPercentages() + File.separator + "Training" + File.separator + "data")));
		BufferedReader brIn = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		boolean firstTime = true;
		
		while ((strLine = brIn.readLine()) != null)   {

			String[] strUserPreferences = strLine.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			lwUserID.set(Long.parseLong(strUserID));
			reader.get(lwUserID, dwValue);
			
			double userDisposition = dwValue.get();
			
			if(firstTime){
				firstTime = false;
			}
			else{
				bw.write("\r\n");
			}
			
			bw.write(strUserID);
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
				
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
				double rating = Double.parseDouble(strItemInfo[2]);
				
				rating = rating * userDisposition;
				if(rating > 5.0)
					rating = 5.0;
				
				bw.write("\t" + strItemInfo[0] + "-" + strItemInfo[1] + "-" + String.valueOf(rating));
			}
		}

		brIn.close();
		bw.close();
		reader.close();
		return true;
	}
	
	public static boolean normalizeTestingRecSys_NaiveBayes(ExecutionVariablesCurrentGroup evcg) throws IOException{

		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Testing Recommendation System - NaiveBayes*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		
		if(!lfs.exists(new Path("localMapFiles" + File.separator + "UserDisposition_Final")))
		{
			Path pathHDFS = new Path(evcg.getOutputPathGeneral() + File.separator +"UserPipeline" + File.separator + "UserDisposition_Final");
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "UserDisposition_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
		}
		
		Path pathInput = new Path("localMapFiles" + File.separator + "UserDisposition_Final");
    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		
		DoubleWritable dwValue = new DoubleWritable();
    	LongWritable lwUserID = new LongWritable(-1);
    	
    	Path qualifiedOutputPath = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
		FSDataOutputStream	fsOutStream = fs.create(qualifiedOutputPath);
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutStream));
    	
		DataInputStream in = new DataInputStream(fs.open(new Path(evcg.getInputPathDatasetAndPercentages() + File.separator + "Testing" + File.separator + "data")));
		BufferedReader brIn = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		boolean firstTime = true;
		
		while ((strLine = brIn.readLine()) != null)   {

			String[] strUserPreferences = strLine.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			lwUserID.set(Long.parseLong(strUserID));
			reader.get(lwUserID, dwValue);
			
			double userDisposition = dwValue.get();
			
			if(firstTime){
				firstTime = false;
			}
			else{
				bw.write("\r\n");
			}
			
			bw.write(strUserID);
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
				
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
//				long itemID = Long.parseLong(strItemInfo[0]);
				double rating = Double.parseDouble(strItemInfo[2]);
				
				rating = rating * userDisposition;
				if(rating > 5.0)
					rating = 5.0;
				
				bw.write("\t" + strItemInfo[0] + "-" + strItemInfo[1] + "-" + String.valueOf(rating));
			}
		}

		brIn.close();
		bw.close();
		reader.close();
		return true;
	}
	
	public static boolean normalizeTrainingRecSys_SlopeOne(ExecutionVariablesCurrentGroup evcg) throws IOException{

		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_training_RecSysInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Training Recommendation System - SlopeOne*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		
		if(!lfs.exists(new Path("localMapFiles" + File.separator + "UserDisposition_Final")))
		{
			Path pathHDFS = new Path(evcg.getOutputPathGeneral() + File.separator +"UserPipeline" + File.separator + "UserDisposition_Final");
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "UserDisposition_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
		}
		
		Path pathInput = new Path("localMapFiles" + File.separator + "UserDisposition_Final");
    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		
		DoubleWritable dwValue = new DoubleWritable();
    	LongWritable lwUserID = new LongWritable(-1);
    	
    	Path qualifiedOutputPath = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
		FSDataOutputStream	fsOutStream = fs.create(qualifiedOutputPath);
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutStream));
    	
		DataInputStream in = new DataInputStream(fs.open(new Path(evcg.getInputPathDatasetAndPercentages() + File.separator + "Training" + File.separator + "data")));

		BufferedReader brIn = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		boolean firstTime = true;
		
		while ((strLine = brIn.readLine()) != null)   {

			String[] strUserPreferences = strLine.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			lwUserID.set(Long.parseLong(strUserID));
			reader.get(lwUserID, dwValue);
			
			double userDisposition = dwValue.get();
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
				
//				LongWritable userID = new LongWritable( Long.parseLong(strUserID));
				
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
//				long itemID = Long.parseLong(strItemInfo[0]);
				double rating = Double.parseDouble(strItemInfo[2]);
				
				rating = rating * userDisposition;
				if(rating > 5.0)
					rating = 5.0;
				
				if(firstTime){
					firstTime = false;
				}
				else{
					bw.write("\r\n");
				}
				
				bw.write(strUserID + "," + strItemInfo[0] + "," + String.valueOf(rating));
			}
		}

		brIn.close();
		bw.close();
		reader.close();
		return true;
	}
	
	public static boolean normalizeTestingRecSys_SlopeOne(ExecutionVariablesCurrentGroup evcg) throws IOException{

		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Testing Recommendation System - SlopeOne*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}

		if(!lfs.exists(new Path("localMapFiles" + File.separator + "UserDisposition_Final")))
		{
			Path pathHDFS = new Path(evcg.getOutputPathGeneral() + File.separator +"UserPipeline" + File.separator + "UserDisposition_Final");
	    	Path pathLocal = new Path( "localMapFiles" + File.separator + "UserDisposition_Final");
	    	fs.copyToLocalFile(pathHDFS, pathLocal);
		}
		
		Path pathInput = new Path("localMapFiles" + File.separator + "UserDisposition_Final");
    	Path qualifiedDirNameInput = lfs.makeQualified(pathInput);
		MapFile.Reader reader = new MapFile.Reader(lfs, qualifiedDirNameInput.toString(),conf);
		
		DoubleWritable dwValue = new DoubleWritable();
    	LongWritable lwUserID = new LongWritable(-1);

    	Path qualifiedOutputPath = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
		FSDataOutputStream	fsOutStream = fs.create(qualifiedOutputPath);
			
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutStream));
    	
		DataInputStream in = new DataInputStream(fs.open(new Path(evcg.getInputPathDatasetAndPercentages() + File.separator + "Testing" + File.separator + "data")));

		BufferedReader brIn = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		boolean firstTime = true;
		
		while ((strLine = brIn.readLine()) != null)   {

			String[] strUserPreferences = strLine.split("\\t");
		
			String strUserID = strUserPreferences[0];
			
			lwUserID.set(Long.parseLong(strUserID));
			reader.get(lwUserID, dwValue);
			
			double userDisposition = dwValue.get();
			
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
								
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
				double rating = Double.parseDouble(strItemInfo[2]);
				
				rating = rating * userDisposition;
				if(rating > 5.0)
					rating = 5.0;
				
				if(firstTime){
					firstTime = false;
				}
				else{
					bw.write("\r\n");
				}
				
				bw.write(strUserID + "," + strItemInfo[0] + "," + String.valueOf(rating));
			}
		}

		brIn.close();
		bw.close();
		reader.close();
		return true;
	}
	
	public static void run(ExecutionVariablesCurrentGroup evcg) throws IOException{
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Normalize Ratings - Start");
		
		long start = 0L;
		
		try {
			conf = new Configuration();
			fs = FileSystem.get(conf);
			lfs = LocalFileSystem.getLocal(conf);
			start = System.nanoTime();
			lfs.delete(new Path("localMapFiles"), true);
		    lfs.mkdirs(new Path("localMapFiles"));
			
			if(!fs.exists(new Path(evcg.getOutputPathGeneral() + File.separator + "newUserPreferences")))
			{
				fs.mkdirs(new Path(evcg.getOutputPathGeneral() + File.separator + "newUserPreferences"));
				normalizeTrainingOurSystem(evcg);
				if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.SLOPE_ONE)
				{
					normalizeTrainingRecSys_SlopeOne(evcg);
					normalizeTestingRecSys_SlopeOne(evcg);
				}
				else if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.NAIVE_BAYES)
				{
					normalizeTrainingRecSys_NaiveBayes(evcg);
					normalizeTestingRecSys_NaiveBayes(evcg);
				}
				
			}
			else
			{
				boolean somethingHasRun = false;
				
				somethingHasRun = (normalizeTrainingOurSystem(evcg) || somethingHasRun);
				if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.SLOPE_ONE)
				{
					somethingHasRun = (normalizeTrainingRecSys_SlopeOne(evcg) || somethingHasRun);
					somethingHasRun = (normalizeTestingRecSys_SlopeOne(evcg) || somethingHasRun);
				}
				else if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.NAIVE_BAYES)
				{
					somethingHasRun = (normalizeTrainingRecSys_NaiveBayes(evcg) || somethingHasRun);
					somethingHasRun = (normalizeTestingRecSys_NaiveBayes(evcg) || somethingHasRun);
				}
				
				if(somethingHasRun == false)
					return;
			}
			
			lfs.delete(new Path("localMapFiles"), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		long stop = System.nanoTime();
		evcg.evOtherRecSys.time_NormalizeRatings = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Normalize Ratings - Stop");
		evcg.hasChangedOtherRecommendationSystem = true;
		
		System.runFinalization();
		System.gc();
	}
}
