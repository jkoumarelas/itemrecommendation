package other;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

/**
 * 
 * This class helps to split the dataset in percentages as defined in the
 * "ExecutionParameters.txt". The splitted dataset is saved so when it's needed
 * again it will not be splitted again.
 * 
 * @author John Koumarelas
 *
 */
public class SplitDataset {

	public static void split(String inputFilePath, String outputFilePath, double trainingPercentage, double testingPercentage,double usersPercentage, long usersNumber ) throws IOException{
	    
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path inFilePath = new Path(inputFilePath);

		BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(fs.open(inFilePath))));
		
		fs.mkdirs(new Path(outputFilePath));
		fs.mkdirs(new Path(outputFilePath + File.separator + "Training"));
		fs.mkdirs(new Path(outputFilePath + File.separator + "Testing"));

		Path pathQualifiedOutputTraining = fs.makeQualified(new Path(outputFilePath + File.separator + "Training" + File.separator + "data"));
		FSDataOutputStream	fsOutStreamTraining = fs.create(pathQualifiedOutputTraining);
		BufferedWriter bwOutTraining = new BufferedWriter(new OutputStreamWriter(fsOutStreamTraining));

		Path pathQualifiedOutputTesting = fs.makeQualified(new Path(outputFilePath + File.separator + "Testing" + File.separator + "data"));
		FSDataOutputStream	fsOutStreamTesting = fs.create(pathQualifiedOutputTesting);
		BufferedWriter bwOutTesting = new BufferedWriter(new OutputStreamWriter(fsOutStreamTesting));
		
		boolean firstTime = true;
		
		String strLine;
		
		long counterTotalNumberOfUsers = -1L;
		
		if(usersNumber > -1L)
			counterTotalNumberOfUsers = usersNumber;
		else
		{
			long totalNumberOfUsers = 0L;
			// We've got percentage, so first we've got to find the total number of users.
			while ((strLine = br.readLine()) != null)
			{
				totalNumberOfUsers++;
			}
			counterTotalNumberOfUsers = (long)((double)totalNumberOfUsers * usersPercentage);
			br.close();
			br = new BufferedReader(new InputStreamReader(new DataInputStream(fs.open(inFilePath))));
		}
		
		long counterNumberOfUsers = 0L;
		
		boolean trainingPreviousUserWrote = true;
		boolean testingPreviousUserWrote = true;
		
		while ((strLine = br.readLine()) != null)
		{
			try
			{
				if((counterNumberOfUsers++) == counterTotalNumberOfUsers) break;
				
				  String[] strUserPreferences = strLine.split("\\t");
				  String strUserID = strUserPreferences[0];
				  
				  int howManyForTraining = (int)Math.floor((strUserPreferences.length - 1) * trainingPercentage);
				  
				  if(howManyForTraining == 0)
					  howManyForTraining = 1;	// At least one.
				  
				  int howManyForTesting = -1;
				  if(testingPercentage > 0 && ((testingPercentage + trainingPercentage) < 1.0))
					  howManyForTesting = (int)Math.floor((strUserPreferences.length - 1) * testingPercentage);
				  else
					  howManyForTesting = (strUserPreferences.length - 1) - howManyForTraining;
				  
				  int countTraining = 0;
				  int countTesting = 0;

				  if(firstTime)
					  firstTime = false;
				  else
				  {
					  if(trainingPreviousUserWrote)
						  bwOutTraining.write("\r\n");
					  
					  if(testingPreviousUserWrote)
						  bwOutTesting.write("\r\n");
				  }
				  
				  if(howManyForTraining > 0)
					  trainingPreviousUserWrote = true;
				  else if (howManyForTraining == 0)
					  trainingPreviousUserWrote = false;
				  
				  if(howManyForTesting > 0)
					  testingPreviousUserWrote = true;
				  else if (howManyForTesting == 0)
					  testingPreviousUserWrote = false;
				  
				  if(howManyForTraining > 0)
					  bwOutTraining.write(strUserID);
				  
				  if(howManyForTesting > 0)
					  bwOutTesting.write(strUserID);
				  
				  for(int i = 1 ; i < strUserPreferences.length ; i++){
						if((countTraining++) < howManyForTraining && howManyForTraining > 0)
							bwOutTraining.write("\t" + strUserPreferences[i]);
						else if((countTesting++) < howManyForTesting && howManyForTesting > 0)
							bwOutTesting.write("\t" + strUserPreferences[i]);
				  }
			}
			catch (Exception e)
			{
				  System.err.println("Error: " + e.getMessage());
			}
		}
		
		//Close the streams
		bwOutTraining.close();
		bwOutTesting.close();
		br.close();
	}
	
	public static boolean checkFolderExists(ExecutionVariablesCurrentGroup evcg)
	{
		try {
			Configuration conf = new Configuration();
			FileSystem fs = FileSystem.get(conf);
			String pathStrToCreate = evcg.getInputPathDatasetAndPercentages();
			
			org.apache.hadoop.fs.Path pathToCreate = new org.apache.hadoop.fs.Path(pathStrToCreate);
			
			return fs.exists(pathToCreate);
				
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}
	
	public static void run(ExecutionVariablesCurrentGroup evcg){
		
		if(checkFolderExists(evcg))
			return;
		
		String pathStrInputFile = evcg.evGeneral.inputPath + File.separator + evcg.evGeneral.datasetName + File.separator + "data";
		String pathStrOutputFile = evcg.getInputPathDatasetAndPercentages();
		try {
			split(pathStrInputFile, pathStrOutputFile, evcg.evGeneral.trainingPercentage, evcg.evGeneral.testingPercentage,evcg.evGeneral.usersPercentage, evcg.evGeneral.usersNumber);
		} catch (IOException e) {
			e.printStackTrace();
		}
		evcg.hasChangedGeneral = true;
	}
	
}
