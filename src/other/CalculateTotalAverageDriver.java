package other;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.LongPairWritable;

/**
 * 
 * This class will calculate the total average of all the ratings, which
 * is needed in other drivers. (UserPipelineDriver.java)
 * 
 * @author John Koumarelas
 *
 */
public class CalculateTotalAverageDriver {
	private static String pathStrOutput = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	public static class Map extends Mapper<LongWritable, Text, LongWritable, LongPairWritable> {
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			String strValue = value.toString();
			String[] strUserPreferences = strValue.split("\\t");
					
			// We begin from 1, because in 0 index userID is stored.
			for(int i = 1 ; i < strUserPreferences.length ; i++){
				// strUP:	itemID-categoryID1_categoryID2_..._categoryIDN-rating
				String strUP = strUserPreferences[i];
								
				//	strItemInfo[0]: 	itemID
				//	strItemInfo[1]:		categoryID1_categoryID2_..._categoryIDN
				//	strItemInfo[2]:		rating
				String[] strItemInfo = strUP.split("-");
				
				int rating = Integer.parseInt(strItemInfo[2]);
				
				if(rating > 5)
					System.err.println("Error at \"CalculateTotalAverageDriver\"! Number greater than 5 (>5).");
				
				LongPairWritable lpw = new LongPairWritable();
				lpw.set(rating, 1L);

				LongWritable keyForCombiner = new LongWritable();
				keyForCombiner.set(1L);
				
				context.write(keyForCombiner, lpw);
			}
			
		}
	}
	
	public static class Combine extends Reducer<LongWritable, LongPairWritable, LongWritable, LongPairWritable> {
		@Override
		public void reduce(LongWritable key, Iterable<LongPairWritable> values, Context context) {
			
			long sum = 0;
			long counter = 0;
			for(LongPairWritable lpw : values){
				sum += lpw.getFirst();
				counter += lpw.getSecond();
			}
		    
			LongPairWritable lpw = new LongPairWritable();
			lpw.set(sum, counter);
			
			LongWritable keyForReduce = new LongWritable();
			keyForReduce.set(1L);
			
			try {
				context.write(keyForReduce, lpw);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	}
	
	public static class Reduce extends Reducer<LongWritable, LongPairWritable, Text, Text> {
		@Override
		public void reduce(LongWritable key, Iterable<LongPairWritable> values, Context context) {
			try {
				Iterator<LongPairWritable> it = values.iterator();
					
				long sum = 0;
				long counter = 0;
				while(it.hasNext())
				{
					LongPairWritable lpw = it.next();
					sum += lpw.getFirst();
					counter += lpw.getSecond();
				}
				
				double totalAverage = sum / (double) counter;
			    
				context.write(new Text(String.valueOf(1)), new Text(String.valueOf(totalAverage)));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	}
	
	public static void getAndStoreTotalAverage(){
		try {
			double totalAverage = 0.0;
			
			// If many reducers have initialized files, we open all of them until we found the one (file) that has the value.
			FileStatus[] fstArray = fs.listStatus(new Path(pathStrOutput + File.separator + "General" + File.separator + "TotalAverage"));
			for(FileStatus fst : fstArray){
				if(fst.getPath().getName().startsWith("part-r-"))	//	We are in a "part-r-" file.
				{
					DataInputStream dis = new DataInputStream(fs.open(fst.getPath()));
					BufferedReader br = new BufferedReader(new InputStreamReader(dis));
					
					String strLine;
					
					while ((strLine = br.readLine()) != null)   {
						String[] tokens = strLine.split("\\t");
						totalAverage = totalAverage + Double.valueOf(tokens[1]);
					}
					br.close();
				}
			}

			//Writer
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fs.create(new Path(pathStrOutput, "TotalAverage_Final"))));
			bw.write(String.valueOf(totalAverage));
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Calculate Total Average Driver - Start");

		conf = new Configuration();
		fs = FileSystem.get(conf);
	
		pathStrOutput = evcg.getOutputPathGeneral();
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TotalAverage_Final")))
			{
				System.out.println("*Calculate Total Average Driver*");
				System.out.println("Folder: " + pathStrOutput + File.separator + "TotalAverage_Final");
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput + File.separator + "TotalAverage_Final")))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) return;
			}
		}
		
		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput + File.separator + "General"), true); // true: recursively
		fs.delete(new Path(pathStrOutput + File.separator + "TotalAverage_Final"), false);

		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.getInputPathDatasetAndPercentages() + File.separator + "Training";
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "General" + File.separator + "TotalAverage";

		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		Job job = new Job(conf, "TotalAverage");

		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);
		
		job.setJarByClass(CalculateTotalAverageDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		job.setCombinerClass(Combine.class);
		
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(LongPairWritable.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Double.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		job.waitForCompletion(true);
		
		getAndStoreTotalAverage();
	
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
		{
			fs.deleteOnExit(new Path(pathStrOutput + File.separator + "General"));
		}
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_CalculateTotalAverageDriver = stop - start;
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Calculate Total Average Driver - Stop");
		evcg.hasChangedGeneral = true;
	}
}
