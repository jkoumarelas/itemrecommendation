package other;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;
import other.executionEnvironment.ExecutionVariablesOtherRecommendationSystem;

/**
 * 
 * MergeOtherRecSysWithOur.java
 * 
 * This class merges the estimation ratings that the other recommendation system
 * outputted in the previous step. This is needed normally when the other recommendation
 * system demanded the ratings in a way where useful information for our system were not included
 * like the categories of the movie's. This is done for the testing data.
 * 
 * @author John Koumarelas
 *
 */
public class MergeOtherRecSysWithOur {
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	public static boolean merge(ExecutionVariablesCurrentGroup evcg) throws IOException{

		String outputPathStr = evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_OurSystemInput";
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				System.out.println("*Normalize Ratings - Other Recommendation System*");
				System.out.println("Folder: " + outputPathStr);
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return false;
				}
				else
				{
					in.close();
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
		else
		{
			if(fs.exists(new Path(outputPathStr)))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders)
					return false;
				else
				{
					fs.delete(new Path(outputPathStr), true);
					fs.mkdirs(new Path(outputPathStr));
				}
			}
			else
			{
				fs.mkdirs(new Path(outputPathStr));
			}
		}
    	
		Path qualifiedOutputPath = fs.makeQualified(new Path(outputPathStr + File.separator + "data"));
		FSDataOutputStream	fsOutStream = fs.create(qualifiedOutputPath);
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fsOutStream));
		
		BufferedReader brOurRecSys = new BufferedReader(new InputStreamReader(new DataInputStream(fs.open(new Path(evcg.getInputPathDatasetAndPercentages() + File.separator + "Testing" + File.separator + "data")))));
		BufferedReader brOtherRecSys = new BufferedReader(new InputStreamReader(new DataInputStream(fs.open(new Path(evcg.getOutputPathOtherRecommendationSystem() + File.separator + "newUserPreferences" + File.separator + "userPreferences_testing_RecSysOutput" + File.separator + "data")))));

		
		String strLine = null;
		String strLineRecSys = null;
		
		String userID = null;
		
		boolean firstTime = true;
		
		while ((strLine = brOurRecSys.readLine()) != null)   {
			String[] tokens = strLine.split("\t");
			
			userID = tokens[0];
			
			if(firstTime){
				firstTime = false;
			}
			else{
				bw.write("\r\n");
			}
			
			bw.write(userID);
			
			// tokens.length - 1 == # of ratings by this user
			for(int i = 1 ; i < tokens.length ; i++){
				strLineRecSys = brOtherRecSys.readLine();
				
				
				//	tokensRecSys[0]: userID
				//	tokensRecSys[1]: itemID
				//	tokensRecSys[2]: normalizedRating
				//	tokensRecSys[3]: RecSys Rating
				String[] tokensRecSys = strLineRecSys.split(",");
				
				String categories = (tokens[i].split("-"))[1];
				
				if(Double.parseDouble(tokensRecSys[3]) >= 0)
					bw.write("\t" + tokensRecSys[1] + "-" + categories + "-" + tokensRecSys[2] + "-" + tokensRecSys[3]);
				else{
					// Negative or NaN --->  nothing...
				}
			}
			
		}

		brOurRecSys.close();
		brOtherRecSys.close();
		bw.close();
		return true;
	}
	
	public static void run(ExecutionVariablesCurrentGroup evcg){
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Merge Other Recommendation System With Our Recommendation System - Start");
		
		if(evcg.evOtherRecSys.otherRecSys == ExecutionVariablesOtherRecommendationSystem.OtherRecommendationSystem.NAIVE_BAYES)
		{
			if(evcg.evGeneral.showStartStopPrints) System.out.println("Merge Other Recommendation System With Our Recommendation System - Stop");
			evcg.hasChangedOtherRecommendationSystem = true;
			return;
		}
		
		long start = System.nanoTime();

		try {
			conf = new Configuration();
			fs = FileSystem.get(conf);
			
			if(merge(evcg) == false)
				return;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		long stop = System.nanoTime();
		evcg.evOtherRecSys.time_MergeOtherRecSysWithOur = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("Merge Other Recommendation System With Our Recommendation System - Stop");
		evcg.hasChangedOtherRecommendationSystem = true;
		
		System.runFinalization();
		System.gc();
	}
}
