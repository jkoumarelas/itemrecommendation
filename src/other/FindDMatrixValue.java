package other;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.MapFile;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.IntPairWritable;

/**
 * 
 * This class provides standard functions over the D Matrix.
 * 
 * @author John Koumarelas
 *
 */
public class FindDMatrixValue {

	public static double run_max(ExecutionVariablesCurrentGroup evcg) throws IOException{
		
		Configuration conf = new Configuration();
		FileSystem fs = null;
		
		fs = FileSystem.get(conf);

		IntPairWritable ipwKey = new IntPairWritable();
		ipwKey.set(-2, -1);

    	Path path = new Path(evcg.getOutputPathGeneral() + File.separator + "DMatrix" + "_Final");

    	Path pathQualified = fs.makeQualified(path);
    	MapFile.Reader reader = new MapFile.Reader(fs, pathQualified.toString(),conf);
    	
    	DoubleWritable dwValue = new DoubleWritable();
    	
    	TreeSet<Double> ts = new TreeSet<Double>();
		
		ipwKey = (IntPairWritable) reader.getClosest(ipwKey, dwValue);	//	key: userID

		// First user
		ts.add(new Double(dwValue.get()));
		
		while(reader.next(ipwKey, dwValue)){
			ts.add(new Double(dwValue.get()));
		}
		
		double max = -1.0;
		
		max = ts.descendingIterator().next();
		
		reader.close();
		
		return max;
	}
	
	public static double run_median(ExecutionVariablesCurrentGroup evcg) throws IOException{
		Configuration conf = new Configuration();
		FileSystem fs = null;
		
		fs = FileSystem.get(conf);

		IntPairWritable ipwKey = new IntPairWritable();
		ipwKey.set(-2, -1);

    	Path path = new Path(evcg.getOutputPathGeneral() + File.separator + "DMatrix" + "_Final");

    	Path pathQualified = fs.makeQualified(path);
    	MapFile.Reader reader = new MapFile.Reader(fs, pathQualified.toString(),conf);
    	
    	DoubleWritable dwValue = new DoubleWritable();
    	
    	TreeSet<Double> ts = new TreeSet<Double>();
		
		ipwKey = (IntPairWritable) reader.getClosest(ipwKey, dwValue);	//	key: userID

		// First user
		ts.add(new Double(dwValue.get()));
		
		while(reader.next(ipwKey, dwValue)){
			ts.add(new Double(dwValue.get()));
		}
		
		int medianPosition = ts.size() / 2;
		double median = -1.0;
		
		Iterator<Double> it = ts.iterator();
		int counter = 0;
		while(it.hasNext()){
			counter++; 
			if(counter == medianPosition)
			{
				median = it.next();
				break;
			}
			it.next();
		}
		
		reader.close();
		return median;
	}
	
	public static double run_mean(ExecutionVariablesCurrentGroup evcg) throws IOException{
		Configuration conf = new Configuration();
		FileSystem fs = null;
		
		fs = FileSystem.get(conf);

		IntPairWritable ipwKey = new IntPairWritable();
		ipwKey.set(-2, -1);

    	Path path = new Path(evcg.getOutputPathGeneral() + File.separator + "DMatrix" + "_Final");

    	Path pathQualified = fs.makeQualified(path);
    	MapFile.Reader reader = new MapFile.Reader(fs, pathQualified.toString(),conf);
    	
    	DoubleWritable dwValue = new DoubleWritable();
		
		ipwKey = (IntPairWritable) reader.getClosest(ipwKey, dwValue);	//	key: userID

		double sumMean = 0;
		int counterMean = 0;
		
		// First user
		sumMean += dwValue.get();
		counterMean++;
		
		while(reader.next(ipwKey, dwValue)){
			sumMean += dwValue.get();
			counterMean++;
		}
		
		double mean = sumMean / counterMean;
		
		reader.close();
		return mean;
	}
	
	public static double run_std(ExecutionVariablesCurrentGroup evcg) throws IOException{
		double mean = run_mean(evcg);
		
		Configuration conf = new Configuration();
		FileSystem fs = null;
		
		fs = FileSystem.get(conf);

		IntPairWritable ipwKey = new IntPairWritable();
		ipwKey.set(-2, -1);

    	Path path = new Path(evcg.getOutputPathGeneral() + File.separator + "DMatrix" + "_Final");

    	Path pathQualified = fs.makeQualified(path);
    	MapFile.Reader reader = new MapFile.Reader(fs, pathQualified.toString(),conf);
    	
    	DoubleWritable dwValue = new DoubleWritable();
		
		ipwKey = (IntPairWritable) reader.getClosest(ipwKey, dwValue);	//	key: userID

		double sumDiffFromMean = 0;
		int counterDiffFromMean = 0;
		
		// First user
		sumDiffFromMean += Math.pow(dwValue.get() - mean, 2.0);
		counterDiffFromMean++;
		
		while(reader.next(ipwKey, dwValue)){
			sumDiffFromMean += Math.pow(dwValue.get() - mean, 2.0);
			counterDiffFromMean++;
		}
		
		double std = Math.sqrt(sumDiffFromMean / counterDiffFromMean);
		
		reader.close();
		return std;
	}
}
