package dmatrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapFile;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import other.executionEnvironment.ExecutionVariablesCurrentGroup;

import commons.IntPairWritable;
import commons.LongArrayWritable;

/**
 * 
 * DMatrixDriver.java
 * 
 * This class will output MapFiles for the structure:
 * 		-	D Matrix
 * 
 * @author John Koumarelas
 *
 */
public class DMatrixDriver {
	private static String pathStrOutput = null;
	private static String pathStrOutputOriginal = null;
	
	private static Configuration conf = null;
	private static FileSystem fs = null;
	
	private static long keys_MapFile_DMatrix = 0L;
	private static long index_sizeInBytes_MapFile_DMatrix = 0L;
	
	public static void clearFields()
	{
		keys_MapFile_DMatrix = 0L;
		index_sizeInBytes_MapFile_DMatrix = 0L;
		
	}
	
	// Input: users... At the reducers the average will be computed.
	public static class Map extends Mapper<LongWritable, Text, IntPairWritable, LongArrayWritable> 
	{
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutputOriginal = null;
		
		private MapFile.Reader reader = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,IntPairWritable,LongArrayWritable>.Context context) throws IOException ,InterruptedException 
		{
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			{
				Path path = new Path(pathStrOutputOriginal + File.separator + "CategoriesCommonUsers" + File.separator 
						+ "SetUsersPairCategories" + "_Final");
				Path pathQualified = fs.makeQualified(path);
				reader = new MapFile.Reader(fs, pathQualified.toString(),conf);
			}
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,IntPairWritable,LongArrayWritable>.Context context) throws IOException ,InterruptedException 
		{
			reader.close();
			super.cleanup(context);
		};
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException 
		{
			
			//	strArrayCategoriesCombination[0]: categoryID1
			//	strArrayCategoriesCombination[1]: categoryID2
			String[] strArrayCategoriesCombination = value.toString().split(",");	//	Categories were separated in file with commas. e.g. 1,2
			
			// We get the common users for these categories.
	    	IntWritable iwKeyCategory1 = new IntWritable(Integer.parseInt(strArrayCategoriesCombination[0]));
	    	IntWritable iwKeyCategory2 = new IntWritable(Integer.parseInt(strArrayCategoriesCombination[1]));
	    	
	    	IntPairWritable ipwCategoriesCombination = new IntPairWritable();
	    	ipwCategoriesCombination.set(iwKeyCategory1.get(), iwKeyCategory2.get());
	    	
	    	LongArrayWritable lawSetCommonUsers = new LongArrayWritable();
	    	
	    	reader.get(ipwCategoriesCombination, lawSetCommonUsers);
	    	
	    	if(lawSetCommonUsers.get() != null)
	    		context.write(ipwCategoriesCombination, lawSetCommonUsers);
		}
	}
	
	public static class Reduce extends Reducer<IntPairWritable, LongArrayWritable, Text, Text> 
	{
		private Configuration conf = null;
		private FileSystem fs = null;
		
		private String pathStrOutput = null;
		private String pathStrOutputOriginal = null;
		
		private MapFile.Reader reader = null;
		
		protected void setup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,LongArrayWritable,Text,Text>.Context context) throws IOException ,InterruptedException 
		{
			conf = context.getConfiguration();
			fs = FileSystem.get(conf);
			
			pathStrOutput = conf.get("pathStrOutput");
			pathStrOutputOriginal = conf.get("pathStrOutputOriginal");
			
			// User's Interest On A category
			{
				Path path = new Path(pathStrOutputOriginal + File.separator + "UserPipeline" + File.separator + "UserInterestOnCategory_Final");
	 	    	Path pathQualified = fs.makeQualified(path);
	 	    	reader = new MapFile.Reader(fs, pathQualified.toString(),conf);
			}
			super.setup(context);
		};
		
		protected void cleanup(org.apache.hadoop.mapreduce.Reducer<IntPairWritable,LongArrayWritable,Text,Text>.Context context) throws IOException ,InterruptedException 
		{
			reader.close();
			super.cleanup(context);
		};
	

		@Override
		public void reduce(IntPairWritable key, Iterable<LongArrayWritable> values, Context context) {
			
	    	int ci = key.getFirst();
			int cj = key.getSecond();
			
			Iterator<LongArrayWritable> it = values.iterator();
			LongArrayWritable iawUsers = it.next();
		    if (it.hasNext())
		    {
		    	System.err.println("Something is wrong. One IntPairWritable should only be passed.");
		    	System.exit(-1);
		    }
		    
		    Writable[] wArrayUsers = iawUsers.get();

			try
			{
	 	    	MapWritable mw = new MapWritable();
	 	    	
	 	    	int counter_ci_cj = 0;	//	When ci > cj
	 	    	int counter_cj_ci = 0;	//	When ci < cj
	 	    	
	 	    	double sum_ci_cj = 0.0;	//	When ci > cj
	 	    	double sum_cj_ci = 0.0;	//	When ci < cj
	 	    	
	 	    	if(wArrayUsers != null)
	 	    	for(Writable wUserID : wArrayUsers)
	 	    	{
	 	    		reader.get((LongWritable)wUserID, mw);
	 	    		
	 	    		DoubleWritable dwUserInterest_ci= (DoubleWritable) mw.get(new IntWritable(ci));
	 	    		DoubleWritable dwUserInterest_cj= (DoubleWritable) mw.get(new IntWritable(cj));
		 	    	
		 	    	if(dwUserInterest_ci != null && dwUserInterest_cj != null && dwUserInterest_ci.get() > dwUserInterest_cj.get())
		 	    	{
		 	    		counter_ci_cj++;
		 	    		sum_ci_cj += dwUserInterest_ci.get() - dwUserInterest_cj.get();
		 	    		
		 	    	} 
		 	    	else if(dwUserInterest_ci != null && dwUserInterest_cj != null && dwUserInterest_ci.get() < dwUserInterest_cj.get())
		 	    	{
		 	    		counter_cj_ci++;
		 	    		sum_cj_ci += dwUserInterest_cj.get() - dwUserInterest_ci.get();
		 	    	}
	 	    	}
	 	    	
		    	if(counter_ci_cj == 0)
		    		counter_ci_cj = 1;
		    	
		    	if(counter_cj_ci == 0)
		    		counter_cj_ci = 1;
	 	    	
	 	    	double dmatrixValue_ci_cj = sum_ci_cj / counter_ci_cj;
	 	    	double dmatrixValue_cj_ci = sum_cj_ci / counter_cj_ci;
	 	    	
	 	    	/* D(ci,cj) */
	 	    	IntPairWritable ci_cj = new IntPairWritable();
		    	ci_cj.set(ci, cj);
	 	    	
	 	    	String pathStr_ci_cj = pathStrOutput + File.separator +  ci + "_" + cj;
				Path path_ci_cj = new Path(pathStr_ci_cj);
	 	    	Path pathQualified_ci_cj = fs.makeQualified(path_ci_cj);
			    MapFile.Writer writer_ci_cj = new MapFile.Writer(conf, fs, pathQualified_ci_cj.toString(), IntPairWritable.class, DoubleWritable.class);
			    
			    DoubleWritable dpw_ci_cj = new DoubleWritable();
			    dpw_ci_cj.set(dmatrixValue_ci_cj);
			    writer_ci_cj.append(ci_cj, dpw_ci_cj);	//	D(ci,cj)
			    writer_ci_cj.close();
			    
			    /* D(cj,ci) */
			    IntPairWritable cj_ci = new IntPairWritable();
			    cj_ci.set(cj, ci);
		    	
			    String pathStr_cj_ci = pathStrOutput + File.separator +  cj + "_" + ci;
				Path path_cj_ci = new Path(pathStr_cj_ci);
	 	    	Path pathQualified_cj_ci = fs.makeQualified(path_cj_ci);
			    MapFile.Writer writer_cj_ci = new MapFile.Writer(conf, fs, pathQualified_cj_ci.toString(), IntPairWritable.class, DoubleWritable.class);
			    
			    DoubleWritable dpw_cj_ci = new DoubleWritable();
			    dpw_cj_ci.set(dmatrixValue_cj_ci);
			    writer_cj_ci.append(cj_ci, dpw_cj_ci);	//	D(cj,ci)
			    writer_cj_ci.close();
	 	    	
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	}
	
	public static void mergeMapFilesGeneral(String pathStrInput,String pathStrCategoriesCombinationFile){
		try
		{
			String pathStr = pathStrInput + "_Final";
			Path pathOutput = new Path(pathStr);
			Path pathQualifiedOutput = fs.makeQualified(pathOutput);
		    MapFile.Writer writer = null;
		    
		    writer = new MapFile.Writer(conf, fs, pathQualifiedOutput.toString(), IntPairWritable.class, DoubleWritable.class);

		    FSDataInputStream fsdis_current = fs.open(new Path(pathStrCategoriesCombinationFile));
		    BufferedReader br_current = new BufferedReader(new InputStreamReader(fsdis_current));
		    
		    String line = null;
		    
		    // Full: not only x,y but y,x also. e.g. 1,3 and 3,1.
		    TreeSet<IntPairWritable> tsCategoriesCombinationFull = new TreeSet<IntPairWritable>();
		    
		    while((line = br_current.readLine()) != null) 
		    {
		    	String[] categoriesCombinationStr = line.split(",");
		    	
		    	IntPairWritable ipwCatComb1 = new IntPairWritable();
		    	ipwCatComb1.set(Integer.parseInt(categoriesCombinationStr[0]), Integer.parseInt(categoriesCombinationStr[1]));
		    	tsCategoriesCombinationFull.add(ipwCatComb1);
		    	IntPairWritable ipwCatComb2 = new IntPairWritable();
		    	ipwCatComb2.set(Integer.parseInt(categoriesCombinationStr[1]), Integer.parseInt(categoriesCombinationStr[0]));
		    	tsCategoriesCombinationFull.add(ipwCatComb2);
		    }
		    
		    Iterator<IntPairWritable> it = tsCategoriesCombinationFull.iterator();
		    while(it.hasNext()) 
		    {
		    	IntPairWritable ipwKey = it.next();
		    	
		    	Path pathInput = new Path(pathStrInput + File.separator + ipwKey.getFirst() + "_" + ipwKey.getSecond());
		    	if(!fs.exists(pathInput)) continue;
		    	
		    	keys_MapFile_DMatrix++;
		    	
		    	Path pathQualifiedInput = fs.makeQualified(pathInput);
		    	MapFile.Reader reader = new MapFile.Reader(fs, pathQualifiedInput.toString(),conf);
		    	
		    	DoubleWritable dwValue = new DoubleWritable();
	    		
	    		reader.get(ipwKey, dwValue);
		    	writer.append(ipwKey, dwValue);
		    	
		    	reader.close();
		    }
		    
		    writer.close();
		    
		} catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public static void mergeMapFiles(String categoriesCombinationFilePath){
		mergeMapFilesGeneral(pathStrOutput,categoriesCombinationFilePath);
	}
	
	private static void calculateFileSizes()
	{
		try
		{
			
			FileStatus indexFileStatus = fs.getFileStatus(new Path(pathStrOutput + "_Final" + File.separator + "index"));
			index_sizeInBytes_MapFile_DMatrix = indexFileStatus.getLen();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public static void updateInfo(ExecutionVariablesCurrentGroup evcg)
	{
		evcg.evGeneral.file_MapFile_DMatrix_keys = keys_MapFile_DMatrix;
		evcg.evGeneral.file_MapFile_DMatrix_index_sizeInBytes = index_sizeInBytes_MapFile_DMatrix;
	}
	
	public static void setParameters(Configuration conf)
	{
		conf.set("pathStrOutput", pathStrOutput);
		conf.set("pathStrOutputOriginal", pathStrOutputOriginal);
	}
	
	public static void run (ExecutionVariablesCurrentGroup evcg) throws Exception {
		if(evcg.evGeneral.showStartStopPrints) System.out.println("D Matrix Driver - Start");
		
		conf = new Configuration();
		fs = FileSystem.get(conf);
		
		pathStrOutput = evcg.getOutputPathGeneral() + File.separator + "DMatrix";
		pathStrOutputOriginal = evcg.getOutputPathGeneral();
		
		setParameters(conf);
		
		if(evcg.evGeneral.askBeforeOverwritingΕxistingFolders)
		{
			if(fs.exists(new Path(pathStrOutput + "_Final")))
			{
				System.out.println("*D Matrix Driver*");
				System.out.println("Folder: " + pathStrOutput + "_Final");
				System.out.println("Do you want to delete everything and execute again? (yes/y/no/n)");
				Scanner in = new Scanner(System.in);
				String decision = in.nextLine();
				if((decision.compareToIgnoreCase("no") == 0) || (decision.compareToIgnoreCase("n") == 0)) {
					in.close();
					return;
				}
				in.close();
			}
		}
		else
		{
			if(fs.exists(new Path(pathStrOutput + "_Final")))
			{
				if(evcg.evGeneral.onFalseAskBeforeOverwriting_KeepExistingFolders) 
					return;
			}
		}
		
		long start = System.nanoTime();
		
		fs.delete(new Path(pathStrOutput), true); // true: recursively
		fs.delete(new Path(pathStrOutput + "_Final"), true); // true: recursively
		
		String[] myArgs = new String[2];
		
		myArgs[0] = evcg.evGeneral.inputPath + File.separator + "filesNeeded" + File.separator + "categoriesCombination" + evcg.evGeneral.datasetName;
		Date dt = new Date(); 
		myArgs[1] = evcg.getOutputPathGeneral() + File.separator + "DummyDirectory" + File.separator + + dt.getTime();
		
		String[] otherArgs =  new GenericOptionsParser(conf, myArgs).getRemainingArgs();
				 
		MapFile.Writer.setIndexInterval(conf, 1);
		
		Job job = new Job(conf, "DMatrix");
		
		job.setNumReduceTasks(evcg.evGeneral.numberOfReduceTasks);

		job.setJarByClass(DMatrixDriver.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		    
		//	Mapper
		job.setInputFormatClass(TextInputFormat.class);
		//	Reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		
		//	Mapper
		job.setMapOutputKeyClass(IntPairWritable.class);
		job.setMapOutputValueClass(LongArrayWritable.class); 
		
		//	Reducer
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		
		clearFields();
		
		job.waitForCompletion(true);
		
		mergeMapFiles(evcg.evGeneral.inputPath + File.separator + "filesNeeded" + File.separator + "categoriesCombination" + evcg.evGeneral.datasetName); // categoriesCombination CORRECT
		calculateFileSizes();
		updateInfo(evcg);
		
		if(evcg.evGeneral.deleteTempFilesAfterExecution)
		{
			fs.deleteOnExit(new Path(pathStrOutput ));
		}
		
		long stop = System.nanoTime();
		evcg.evGeneral.time_DMatrixDriver = stop - start;
		
		if(evcg.evGeneral.showStartStopPrints) System.out.println("D Matrix Driver - Stop");
		evcg.hasChangedGeneral = true;
		
		System.runFinalization();
		System.gc();
	}
}
